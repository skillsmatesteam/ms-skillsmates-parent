package com.yamo.skillsmates.settings.enums;

import lombok.Getter;

@Getter
public enum TemplateKeysEnum {
    RECEIVER_USERNAME,
    RECEIVER_DISPLAY_LINK,
    EMAIL_TITLE,
    EMAIL_MESSAGE,
    CTA_END_TEXT
}
