## Server ports
### Server ports for integration
 - ms-skillsmates-accounts: 8001
 - ms-skillsmates-pages: 8002
 - ms-skillsmates-media: 8003
 - ms-skillsmates-notifications: 8004
 - ms-skillsmates-messages: 8005
 - ms-skillsmates-settings: 8006
 - ms-skillsmates-statistics: 8007
 - ms-skillsmates-group: 8008
 
### Server ports for production
 - ms-skillsmates-accounts: 9001
 - ms-skillsmates-pages: 9002
 - ms-skillsmates-media: 9003
 - ms-skillsmates-notifications: 9004
 - ms-skillsmates-messages: 9005
 - ms-skillsmates-settings: 9006
 - ms-skillsmates-statistics: 9007
 - ms-skillsmates-group: 9008

## How to run this app
nohup java -jar ms-skillsmates-accounts.jar > ms-skillsmates-accounts.log 2>&1 &
nohup java -jar ms-skillsmates-media.jar > ms-skillsmates-media.log 2>&1 &
nohup java -jar ms-skillsmates-messages.jar > ms-skillsmates-messages.log 2>&1 &
nohup java -jar ms-skillsmates-notifications.jar > ms-skillsmates-notifications.log 2>&1 &
nohup java -jar ms-skillsmates-pages.jar > ms-skillsmates-pages.log 2>&1 &
nohup java -jar ms-skillsmates-settings.jar > ms-skillsmates-settings.log 2>&1 &

or

./launchjar.sh 

## How to kill a process
ps aux | grep java
kill -9 <process id>

##  Check expiration date of certificate
openssl pkcs12 -in certificate.p12 -out certificate.pem -nodes
cat certificate.pem | openssl x509 -noout -enddate

##  Generate SSL certificate
1. Go to **https://manage.sslforfree.com/certificate/new**  
2. create an account
3. enter your domain name
4. choose the validity period
5. check auto-generate CSR
6. choose DNS validation and set the CNAME on the record on Route53 on AWS
7. download the zip file certificate
8. go to **https://www.sslshopper.com/ssl-converter.html** 
9. Certificate File to Convert --> select **certificate.crt**
10. Type of Current Certificate --> PEM
11. Type To Convert To --> PFX/PKCS#12
12. Private Key File --> select **private.key**
