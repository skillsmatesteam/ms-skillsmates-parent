package com.yamo.skillsmates.admin.services.impl;

import com.yamo.skillsmates.admin.services.PartnerService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.admin.Partner;
import com.yamo.skillsmates.repositories.account.ActivityAreaRepository;
import com.yamo.skillsmates.repositories.admin.PartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PartnerServiceImpl implements PartnerService {

    private PartnerRepository partnerRepository;
    private ActivityAreaRepository activityAreaRepository;

    @Autowired
    public PartnerServiceImpl(PartnerRepository partnerRepository, ActivityAreaRepository activityAreaRepository) {
        this.partnerRepository = partnerRepository;
        this.activityAreaRepository = activityAreaRepository;
    }

    @Override
    public List<Partner> findAllPartners() {
        return partnerRepository.findAllByActiveTrueAndDeletedFalse().orElse(new ArrayList<>());
    }

    @Override
    public Partner savePartner(Partner partner) throws GenericException {
        try {
            if (partnerRepository.findByDeletedFalseAndEmail(partner.getEmail()).isPresent()){
                throw new GenericException("Partner already exists");
            }
            partner.setIdServer(String.valueOf(System.nanoTime()));
            partner.setActive(true);

            if (partner.getActivityArea() != null){
                Optional<ActivityArea> activityArea= activityAreaRepository.findByIdServer(partner.getActivityArea().getIdServer());
                activityArea.ifPresent(partner::setActivityArea);
            }

            partner = partnerRepository.save(partner);

            return partner;
        } catch (Exception e) {
            throw new GenericException("Error occurred while creating account");
        }
    }
}
