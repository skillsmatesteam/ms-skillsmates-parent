package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.CertificationDto;
import com.yamo.skillsmates.dto.account.DegreeObtainedDto;
import com.yamo.skillsmates.dto.account.HigherEducationDto;
import com.yamo.skillsmates.dto.account.SecondaryEducationDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-accounts/accounts/academics", produces = MediaType.APPLICATION_JSON_VALUE)
public interface AcademicApi {
    @PostMapping(value = "/degrees")
    ResponseEntity<GenericResultDto> createDegreeObtained(String token, String id, DegreeObtainedDto degreeObtainedDto) throws GenericException;

    @PostMapping(value = "/certificates")
    ResponseEntity<GenericResultDto> createCertification(String token, String id, CertificationDto certificationDto) throws GenericException;

    @GetMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> findAcademicsById(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/offline/{accountId}")
    ResponseEntity<GenericResultDto> findAcademicsOfflineById(String accountId) throws GenericException;

    @DeleteMapping(value = "/degrees")
    ResponseEntity<GenericResultDto> deleteDegreeObtained(String token, String id, String degreeObtainedId) throws GenericException;

    @DeleteMapping(value = "/certificates")
    ResponseEntity<GenericResultDto> deleteCertification(String token, String id, String certificationId) throws GenericException;

    @PostMapping(value = "/secondary")
    ResponseEntity<GenericResultDto> createSecondaryEducation(String token, String id, SecondaryEducationDto secondaryEducationDto) throws GenericException;

    @DeleteMapping(value = "/secondary")
    ResponseEntity<GenericResultDto> deleteSecondaryEducation(String token, String id, String secondaryEducationId) throws GenericException;

    @PostMapping(value = "/higher")
    ResponseEntity<GenericResultDto> createHigherEducation(String token, String id, HigherEducationDto higherEducationDto) throws GenericException;

    @DeleteMapping(value = "/higher")
    ResponseEntity<GenericResultDto> deleteHigherEducation(String token, String id, String higherEducationId) throws GenericException;
}
