package com.yamo.skillsmates.media.controllers;

import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.media.api.MetadataApi;
import com.yamo.skillsmates.media.helpers.MetadataHelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class MetadataController extends AbstractController implements MetadataApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetadataController.class);
    @Override
    public ResponseEntity<GenericResultDto> getMetadata(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                        @RequestHeader(value = SkillsmatesHeader.ID) String id, @PathVariable String url) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(url)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            String urlDecoded  = MetadataHelper.decodeSafeUrl(url);
            if (!StringHelper.isUrlValid(urlDecoded)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            String html  = MetadataHelper.getUrlContents(urlDecoded);
            result.setResultObject(MetadataHelper.parseHtml(html, urlDecoded));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

//    private static String decodeSafeUrl(String theUrl) throws IOException{
//        byte[] decodedURLBytes = Base64.getDecoder().decode(theUrl);
//        return new String(decodedURLBytes);
//    }

//    private static MetadataDto parseHtml(String html, String urlDecoded) throws IOException, JSONException {
//        MetadataDto metadataDto = new MetadataDto();
//        Document doc = Jsoup.parse(html);
//
//        // title
//        String title;
//        Elements metaOgTitle = doc.select("meta[property=og:title]");
//        if(metaOgTitle != null && !metaOgTitle.isEmpty()){
//            title = metaOgTitle.first().attr("content");
//        } else{
//            title = doc.title();
//        }
//        metadataDto.setTitle(title);
//
//        // description
//        String description = "";
//        Elements metaOgDescription = doc.select("meta[property=og:description]");
//        if(metaOgDescription != null && !metaOgDescription.isEmpty()){
//            description = metaOgDescription.first().attr("content");
//        } else{
//            Elements elements = doc.select("meta[name=description]");
//            if(elements != null && !elements.isEmpty()) {
//                description = elements.get(0).attr("content");
//            }
//        }
//        metadataDto.setDescription(description);
//
//        // image
//        Elements metaOgImage  = doc.select("meta[property=og:image]");
//        if(metaOgImage != null && !metaOgImage.isEmpty()){
//            metadataDto.setImage(metaOgImage.first().attr("content"));
//        }
//
//        // url de l audio
//        Elements metaOgAudio  = doc.select("meta[property=og:audio]");
//        if(metaOgAudio != null && !metaOgAudio.isEmpty()){
//            metadataDto.setAudio(metaOgAudio.first().attr("content"));
//            //type de l audio
//            Elements metaOgAudioType  = doc.select("meta[property=og:audio:type]");
//            if(metaOgAudioType != null && !metaOgAudioType.isEmpty()) {
////                json.put("og:audio:type", metaOgAudioType.first().attr("content"));
//            }
//        }
//
//        // url de la video
//        Elements metaOgVideo  = doc.select("meta[property=og:video]");
//        if(metaOgVideo != null && !metaOgVideo.isEmpty()){
//            metadataDto.setVideo(metaOgVideo.first().attr("content"));
//            //type de la video
//            Elements metaOgVideoType  = doc.select("meta[property=og:video:type]");
//            if(metaOgVideoType != null && !metaOgVideoType.isEmpty()) {
////                json.put("og:video:type",metaOgVideoType.first().attr("content"));
//            }
//
//        }
//
//        metadataDto.setUrl(urlDecoded);
//        metadataDto.setCanonical(urlDecoded);
//        return metadataDto;
//    }

//    private static String getUrlContents(String theUrl) throws IOException {
//        StringBuilder content = new StringBuilder();
//        // create a url object
//        URL url = new URL(theUrl);
//
//        // create a urlconnection object
//        URLConnection urlConnection = url.openConnection();
//
//        // wrap the urlconnection in a bufferedreader
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//
//        String line;
//
//        // read from the urlconnection via the bufferedreader
//        while ((line = bufferedReader.readLine()) != null) {
//            content.append(line).append("\n");
//        }
//        bufferedReader.close();
//        return content.toString();
//    }
}
