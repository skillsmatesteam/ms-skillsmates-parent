package com.yamo.skillsmates.messages.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface MessageService {
    /**
     * save maessage
     * @param message
     * @return
     * @throws GenericException
     */
    Message saveMessage(Message message) throws GenericException;

    /**
     * save message that contains multimedia
     * @param message
     * @param multipartFile
     * @return
     * @throws GenericException
     */
    Message saveMessageMedia(Message message, MultipartFile multipartFile) throws GenericException;

    /**
     * find all messages of a conversation, and set active of each message at true if active is true
     * @param conversation conversation
     * @param active: if active is true, update attribute active of each messages to true, nothing otherwise
     * @param pageable number of elements to withdraw
     * @return list of messages
     * @throws GenericException if something goes wrong
     */
    List<Message> findAllMessagesByConversation(Conversation conversation, boolean active, Pageable pageable) throws GenericException;

    /**
     * find all unread messages from receiverAccount that belong to a conversation between emitterAccount and receiverAccount
     * @param receiverAccount
     * @param emitterAccount
     * @return
     * @throws GenericException
     */
    List<Message> findUnreadMessagesByReceiver(Account receiverAccount, Account emitterAccount) throws GenericException;

    List<Message> findUnreadMessagesByReceiver(Account receiverAccount, Pageable pageable) throws GenericException;

    List<Message> findUnreadMessagesByReceiverAndEmitter(Account receiverAccount, Account emitterAccount, Pageable pageable) throws GenericException;
}
