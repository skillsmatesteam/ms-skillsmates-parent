package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.accounts.http.EmailService;
import com.yamo.skillsmates.mail.service.MailService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.repositories.account.*;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionMapRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionTypeRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.CryptUtil;
import org.mockito.Mockito;

public class AbstractServiceTests {

    protected final AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
    protected final LibAccountService libAccountService = Mockito.mock(LibAccountService.class);
    protected final ActivityAreaRepository activityAreaRepository = Mockito.mock(ActivityAreaRepository.class);
    protected final LibSocialInteractionService libSocialInteractionService = Mockito.mock(LibSocialInteractionService.class);
    protected final SocialInteractionRepository socialInteractionRepository = Mockito.mock(SocialInteractionRepository.class);
    protected final CryptUtil cryptUtil = Mockito.mock(CryptUtil.class);
    protected final MailService mailService = Mockito.mock(MailService.class);
    protected final PostRepository postRepository = Mockito.mock(PostRepository.class);
    protected final EmailService emailService = Mockito.mock(EmailService.class);
    protected final SocialInteractionTypeRepository socialInteractionTypeRepository = Mockito.mock(SocialInteractionTypeRepository.class);
    protected final SocialInteractionMapRepository socialInteractionMapRepository = Mockito.mock(SocialInteractionMapRepository.class);

    protected final String emailNotExists =  "does.not.exist@email.com";
    protected final String emailExists =  "belinda.mengue@yopmail.com";
    protected String password = "belinda.mengue@yopmail.com";
    protected String securePassword = "9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a";

    protected Account generateAccount(){
        return generateAccount(emailExists, password);
    }

    protected Account generateAccount(String email, String password){
        Account account = new Account();
        account.setActive(true);
        account.setEmail(email);
        account.setPassword(password);
        account.setFirstname("Belinda");
        account.setLastname("MENGUE");

        return account;
    }
}
