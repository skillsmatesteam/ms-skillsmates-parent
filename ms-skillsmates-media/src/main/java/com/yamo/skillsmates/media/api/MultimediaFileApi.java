package com.yamo.skillsmates.media.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/skillsmates-media/multimediafiles", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
public interface MultimediaFileApi {
    @PostMapping
    ResponseEntity<GenericResultDto> uploadMultimedia(String token, String id, String typeDocument, String multimediaId, MultipartFile file) throws GenericException;

    @PostMapping("/avatar")
    ResponseEntity<GenericResultDto> uploadAvatarMultimedia(String token, String id, MultipartFile avatarFile, MultipartFile originalFile) throws GenericException;

    @DeleteMapping
    ResponseEntity<GenericResultDto> deleteMultimedia(String token, String id, String checksum, String documentId) throws GenericException;
}
