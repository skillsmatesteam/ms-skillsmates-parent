package com.yamo.skillsmates.accounts.enums;

import com.yamo.skillsmates.models.post.SocialInteractionType;
import lombok.Getter;

@Getter
public enum TypeEnum {
    REQUEST_RESET,
    CONFIRM_RESET,
    ACCOUNT_CREATED,
    ACCOUNT_ACTIVATED,
    ACCOUNT_DELETED_NOTIFICATION,
    ACCOUNT_DELETED_CONFIRMATION

}
