package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.post.SearchParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/skillsmates-accounts/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
public interface AccountApi {
    @GetMapping
    ResponseEntity<GenericResultDto> findAllAccounts(String token, String id) throws GenericException;

    @GetMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> findAccountById(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/offline/{accountId}")
    ResponseEntity<GenericResultDto> findOfflineAccountById(String accountId) throws GenericException;

    @GetMapping(value = "/infos/{accountId}")
    ResponseEntity<GenericResultDto> findAccountInfos(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/infos/offline/{accountId}")
    ResponseEntity<GenericResultDto> findAccountInfosOffline(String accountId) throws GenericException;

    @PutMapping
    ResponseEntity<GenericResultDto> updateAccount(String token, String id, AccountDto accountDto) throws GenericException;

    @PostMapping
    ResponseEntity<GenericResultDto> createAccount(AccountDto accountDto) throws GenericException;

    @DeleteMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> deleteAccount(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/activate/{accountId}")
    String activateAccount(String accountId) throws GenericException;

    @GetMapping(value = "/logout")
    ResponseEntity<GenericResultDto> logoutAccount(String token, String id) throws GenericException;

    @GetMapping(value = "/profile/{accountId}")
    ResponseEntity<GenericResultDto> countProfileInfos(String token, String id, String accountId) throws GenericException;

    @PutMapping(value = "/deactivate/{accountId}")
    ResponseEntity<GenericResultDto> deactivateAccount(String token, String id, String accountId) throws GenericException;

    @PostMapping(value = "/searchPerson")
    ResponseEntity<GenericResultDto> searchAccounts(String token, String id, SearchParam searchParam, int page, int limit) throws GenericException;

    @GetMapping(value = "/generateUrl/{accountId}")
    ResponseEntity<GenericResultDto> generateUrlProfile(String token, String id, String accountId) throws GenericException;

}
