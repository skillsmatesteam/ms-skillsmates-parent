package com.yamo.skillsmates.pages.enums;

import com.yamo.skillsmates.models.post.SocialInteractionType;
import lombok.Getter;

@Getter
public enum TypeEnum {
    LIKE,
    COMMENT,
    FAVORITE,
    FOLLOWER,
    CHAT,
    POST,
    SHARE,
    REQUEST_RESET,
    CONFIRM_RESET,
    ACCOUNT_CREATED,
    ACCOUNT_ACTIVATED,
    ACCOUNT_DEACTIVATED,
    ACCOUNT_DELETED_NOTIFICATION,
    ACCOUNT_DELETED_CONFIRMATION;

    public static TypeEnum fromSocialInteractionType(SocialInteractionType type){
        switch (type.getLabel()){
            case "LIKE":
                return TypeEnum.LIKE;
            case "COMMENT":
                return TypeEnum.COMMENT;
            case "FAVORITE":
                return TypeEnum.FAVORITE;
            case "FOLLOWER":
                return TypeEnum.FOLLOWER;
            case "CHAT":
                return TypeEnum.CHAT;
            case "POST":
                return TypeEnum.POST;
            case "SHARE":
                return TypeEnum.SHARE;
            default:
                return null;
        }
    }
}
