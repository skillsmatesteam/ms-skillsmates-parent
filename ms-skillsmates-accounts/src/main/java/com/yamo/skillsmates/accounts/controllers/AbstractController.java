package com.yamo.skillsmates.accounts.controllers;


import com.yamo.skillsmates.accounts.services.*;
import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.exception.InvalidParameterException;
import com.yamo.skillsmates.common.logging.LoggingHelper;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.HeaderValidator;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

    protected static final String TOKEN = SkillsmatesHeader.TOKEN;
    protected static final String ID = SkillsmatesHeader.ID;
    protected static final String PAGE = "page";
    protected static final String LIMIT = "limit";
    protected static final String CONTENT = "content";
    protected static final String COUNTRY = "country";
    protected static final String STATUS = "status";
    protected static final String CITY = "city";


    @Autowired
    protected AccountService accountService;
    @Autowired
    protected HeaderValidator headerValidator;
    @Autowired
    protected AuthenticateService authenticateService;
    @Autowired
    protected JwtTokenUtil jwtTokenUtil;
    @Autowired
    protected TokenService tokenService;
    @Autowired
    protected LibAccountService libAccountService;
    @Autowired
    protected AcademicService academicService;
    @Autowired
    protected ProfessionalService professionalService;
    @Autowired
    protected SkillService skillService;
    @Autowired
    protected PasswordService passwordService;
    @Autowired
    protected LibSocialInteractionService libSocialInteractionService;
    @Autowired
    protected SocialInteractionRepository socialInteractionRepository;

    protected GenericResultDto result = null;
    protected String newToken = null ;

    protected ResponseEntity<GenericResultDto> genericResultDtoError(String message, HttpStatus httpStatus){
        InvalidParameterException e = new InvalidParameterException(message);
        result = new GenericResultDto();
        result.setHttpStatus(httpStatus);
        result.setMessage(e.getMessage());
        result.setGenericMessageDetailsList(LoggingHelper.buildGenericMessageDetails(e));
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    protected GenericResultDto processToken(String token, String id) throws GenericException {
        if (StringUtils.isBlank(token) || StringUtils.isBlank(id)){
            throw new GenericException("Invalid input data");
        }

        // validation du token
        result = headerValidator.processHeader(token, id);
        if (result != null)
            throw new GenericException("Invalid input data");

        result = new GenericResultDto<>();
        return result;
    }

    protected void genericResultDtoSuccess(String id){
        result.setHttpStatus(HttpStatus.OK);
        result.setMessage("Success");

        //generate token
        result.setHttpHeaders(ID, id);
        newToken = jwtTokenUtil.generateToken(id);
        result.setHttpHeaders(TOKEN, newToken);

        /*Stockage de l'information de connexion*/
        tokenService.saveToken(id, newToken);
    }
}
