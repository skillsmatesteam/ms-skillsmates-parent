package com.yamo.skillsmates.messages.controllers;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.messages.api.MessageFileApi;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@CrossOrigin
@RestController
public class MessageFileController extends AbstractController implements MessageFileApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageFileController.class);

    @Override
    public ResponseEntity<GenericResultDto> createMessage(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestParam(value = "receiver") String idServerReceiver, @RequestParam(value = "file") MultipartFile file) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);

            if (StringUtils.isBlank(idServerReceiver)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Message message  = new Message();
            Account account = libAccountService.findAccountByIdServer(id);
            if (account == null){
                return genericResultDtoError("Account unidentified", HttpStatus.BAD_REQUEST);
            }
            message.setEmitterAccount(account);

            Account receiver = libAccountService.findAccountByIdServer(idServerReceiver);
            if (receiver == null){
                return genericResultDtoError("Receiver unidentified", HttpStatus.BAD_REQUEST);
            }

            /* Ajout du destinataire */
            message.setReceiverAccount(receiver);

            /* Creation du Conversation */
            String idServerConversation = libConversationService.getConversationId(message);
            Conversation conversation = conversationService.saveConversation(idServerConversation);

            /* Enregistrement du message en  */
            message.setConversation(conversation);
            message = messageService.saveMessageMedia(message, file);

            result.setResultObject(GenericMapper.INSTANCE.asDto(message));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            System.out.println("error = " + e.getMessage());
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
