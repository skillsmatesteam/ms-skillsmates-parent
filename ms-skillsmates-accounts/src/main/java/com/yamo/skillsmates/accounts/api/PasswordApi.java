package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.account.AccountDto;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-accounts/accounts/password")
public interface PasswordApi {
    @GetMapping(value = "/reset/{accountId}/token/{token}")
    String resetPasswordAccount(Model model, String accountId, String token) throws GenericException;

    @PostMapping
    String newPasswordAccount(Model model, AccountDto accountDto, String idServer, String token) throws GenericException, InterruptedException;
}
