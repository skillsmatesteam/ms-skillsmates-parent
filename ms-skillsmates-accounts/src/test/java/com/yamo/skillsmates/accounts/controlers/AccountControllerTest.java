package com.yamo.skillsmates.accounts.controlers;

import com.yamo.skillsmates.accounts.controllers.AccountController;
import com.yamo.skillsmates.accounts.services.*;
import com.yamo.skillsmates.mail.service.LibConversationService;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.repositories.communication.ConversationRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.HeaderValidator;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

@WebMvcTest(controllers = AccountController.class)
@ActiveProfiles("dev")
public class AccountControllerTest extends AbstractControllerTests{

    @BeforeEach
    void setUp() {
        System.out.println("setup");
    }

    @Test
    void shouldFetchAllAccounts() throws Exception {
        Mockito.when(accountService.findAllAccounts()).thenReturn(new ArrayList<>());
    }

}
