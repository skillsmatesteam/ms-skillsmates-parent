package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Skill;
import com.yamo.skillsmates.models.account.config.Discipline;
import com.yamo.skillsmates.models.account.config.Level;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SkillService {
    Skill saveSkill(Skill skill) throws GenericException;
    Skill updateSkill(Skill skill) throws GenericException;
    List<Skill> findSkills(String accountId, Pageable pageable) throws GenericException;
    boolean deleteSkill(String idServer, String accountId) throws GenericException;
    List<Discipline> findDisciplines();
    List<Level> findLevels();
    long countSkills(String accountId) throws GenericException;
}
