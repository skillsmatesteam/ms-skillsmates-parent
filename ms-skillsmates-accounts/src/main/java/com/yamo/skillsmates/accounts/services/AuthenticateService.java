package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface AuthenticateService {
    Account authenticate(String identifier, String password) throws GenericException;
    Account verificate(String email) throws GenericException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException;
}
