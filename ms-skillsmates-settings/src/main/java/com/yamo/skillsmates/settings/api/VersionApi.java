package com.yamo.skillsmates.settings.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-settings/updates", produces = MediaType.APPLICATION_JSON_VALUE)
public interface VersionApi {
    @GetMapping
    ResponseEntity<GenericResultDto> getVersions(String token, String id) throws GenericException;

    @PutMapping
    ResponseEntity<GenericResultDto> dismissVersion(String token, String id) throws GenericException;
}
