package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.services.TokenService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Token;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.account.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository, AccountRepository accountRepository) {
        this.tokenRepository = tokenRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveToken(String accountId, String myToken){
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (account.isPresent()){
            Optional<Token> token = tokenRepository.findByAccount(account.get());
            if (!token.isPresent()){
                Token newToken = new Token();
                newToken.setIdServer(String.valueOf(System.nanoTime()));
                newToken.setToken(myToken);
                newToken.setAccount(account.get());
                tokenRepository.save(newToken);
            } else {
                token.get().setToken(myToken);
                tokenRepository.save(token.get());
            }
        }
    }
}
