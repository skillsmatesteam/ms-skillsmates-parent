package com.yamo.skillsmates.notifications.services.impl;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.notifications.Notification;
import com.yamo.skillsmates.notifications.services.LibNotificationService;
import com.yamo.skillsmates.notifications.services.NotificationService;
import com.yamo.skillsmates.repositories.notifications.NotificationRepository;
import com.yamo.skillsmates.services.LibAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository notificationRepository;
    private final LibNotificationService libNotificationService;
    private final LibAccountService libAccountService;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository, LibNotificationService libNotificationService, LibAccountService libAccountService) {
        this.notificationRepository = notificationRepository;
        this.libNotificationService = libNotificationService;
        this.libAccountService = libAccountService;
    }

    @Override
    public boolean addNotification(Account notifiedAccount, String idElement, Account account, String type) throws GenericException {
        return libNotificationService.addNotification(notifiedAccount, idElement, account, type);
    }

    @Override
    public List<Notification> findNotifications(String accountId, Pageable pageable) throws GenericException {
        return null;
    }

    @Override
    public Long countActiveNotificationsByAccountAndType(String accountId, String labelTypeNotification) throws GenericException {
        return notificationRepository.countAllByEmitterAccountAndNotificationType_LabelAndActiveTrue(libAccountService.findAccountByIdServer(accountId), labelTypeNotification);
    }

    @Override
    public List<Notification> findNotificationsByReceiverAccountAndType(String accountId, String labelTypeNotification) throws GenericException {
        return notificationRepository.findByReceiverAccountAndNotificationType_LabelAndActiveTrue(libAccountService.findAccountByIdServer(accountId), labelTypeNotification).orElse(new ArrayList<>());
    }

    @Override
    public Notification deactivateNotification(String accountId, Notification notification) throws GenericException {
        libAccountService.findAccountByIdServer(accountId);
        Optional<Notification> optionalNotification = notificationRepository.findByIdServer(notification.getIdServer());
        if (!optionalNotification.isPresent()){
            throw new GenericException("Notification unidentified");
        }
        optionalNotification.get().setActive(false);
        return notificationRepository.save(optionalNotification.get());
    }
}
