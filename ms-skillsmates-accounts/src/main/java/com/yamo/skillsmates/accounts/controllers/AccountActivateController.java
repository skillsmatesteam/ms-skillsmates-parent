package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.AccountActiveApi;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class AccountActivateController extends AbstractController implements AccountActiveApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountActivateController.class);

    @Value("${ms.skillsmates.hostname.email-confirmation}")
    private String hostname;

    @Override
    public String activateAccount(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        if (StringUtils.isBlank(accountId)){
            return "Invalid input data";
        }

        try {
            accountService.activateAccount(accountId);
            LOGGER.info("{}" , LoggingUtil.END);
            return "redirect:" + hostname;
        }catch (Exception e){
            return e.getMessage();
        }
    }
}
