package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.SkillApi;
import com.yamo.skillsmates.accounts.services.SkillService;
import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountSkillsDto;
import com.yamo.skillsmates.dto.account.SkillDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.LevelMapper;
import com.yamo.skillsmates.mapper.account.SkillMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class SkillController extends AbstractController implements SkillApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkillController.class);

    protected static final String SKILL_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Skill-Id";

    @Autowired
    private SkillService skillService;

    @Override
    public ResponseEntity<GenericResultDto> createSkill(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody SkillDto skillDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateAccountSkill(skillDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            skillService.saveSkill(GenericMapper.INSTANCE.asEntity(skillDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updateSkill(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody SkillDto skillDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateAccountSkill(skillDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            skillService.updateSkill(GenericMapper.INSTANCE.asEntity(skillDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findSkillsById(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 100, Sort.by(Sort.Order.desc("id")));
            AccountSkillsDto accountSkillsDto = new AccountSkillsDto();
            accountSkillsDto.setSkills(SkillMapper.getInstance().asDtos(skillService.findSkills(accountId, pageable)));
            accountSkillsDto.setLevels(LevelMapper.getInstance().asDtos(skillService.findLevels()));

            result.setResultObject(accountSkillsDto);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findSkillsOfflineById(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 100, Sort.by(Sort.Order.desc("id")));
            AccountSkillsDto accountSkillsDto = new AccountSkillsDto();
            accountSkillsDto.setSkills(SkillMapper.getInstance().asDtos(skillService.findSkills(accountId, pageable)));
            accountSkillsDto.setLevels(LevelMapper.getInstance().asDtos(skillService.findLevels()));

            result.setResultObject(accountSkillsDto);
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteSkill(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestHeader(value = SKILL_HEADER) String skillId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(skillId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            skillService.deleteSkill(skillId, id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validateAccountSkill(SkillDto skillDto){
        return skillDto != null &&
                skillDto.getDiscipline() != null &&
                skillDto.getLevel() != null &&
                skillDto.getAccount() != null &&
                StringUtils.isNotBlank(skillDto.getLabel());
    }
}
