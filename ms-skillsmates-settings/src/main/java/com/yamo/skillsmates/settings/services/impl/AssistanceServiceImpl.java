package com.yamo.skillsmates.settings.services.impl;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.assistance.Assistance;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.assistance.AssistanceRepository;
import com.yamo.skillsmates.settings.services.AssistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssistanceServiceImpl implements AssistanceService {

    private final AssistanceRepository assistanceRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public AssistanceServiceImpl(AssistanceRepository assistanceRepository, AccountRepository accountRepository) {
        this.assistanceRepository = assistanceRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Assistance saveAssistance(Assistance assistance) throws GenericException {
        if (!accountRepository.findByIdServer(assistance.getAccount().getIdServer()).isPresent()){
            throw new GenericException("Account unidentified");
        }

        assistance.setIdServer(String.valueOf(System.nanoTime()));
        return assistanceRepository.save(assistance);
    }

    @Override
    public List<Assistance> findAllAssistances() {
        return assistanceRepository.findAll();
    }
}
