package com.yamo.skillsmates.pages.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.post.PostDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/skillsmates-pages/postsfile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
public interface PostFileApi {

    @PostMapping
    ResponseEntity<GenericResultDto> createPost(String token, String id, String mediaSubtypeId, String title, String type, String keywords, String discipline, String description, MultipartFile file) throws GenericException;

    @PutMapping
    ResponseEntity<GenericResultDto> updatePost(String token, String id, String postId, String mediaSubtypeId, String title, String type, String keywords, String discipline, String description, MultipartFile file) throws GenericException;

    @PostMapping(value = "/new")
    ResponseEntity<GenericResultDto> saveNewPost(String token, String id, PostDto postDto, MultipartFile file) throws GenericException;

    @PostMapping(value = "/new/{postId}")
    ResponseEntity<GenericResultDto> updateNewPost(String token, String id, String postId, PostDto postDto, MultipartFile file) throws GenericException;
}
