package com.yamo.skillsmates.pages.services.impl;

import com.yamo.skillsmates.common.enumeration.NotificationTypeEnum;
import com.yamo.skillsmates.common.enumeration.OriginMultimedia;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.TypeMultimedia;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.media.helpers.AwsAmazonHelper;
import com.yamo.skillsmates.media.helpers.MetadataHelper;
import com.yamo.skillsmates.media.helpers.MultimediaHelper;
import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.multimedia.Metadata;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import com.yamo.skillsmates.models.multimedia.config.MediaType;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.models.post.PostSocialInteractions;
import com.yamo.skillsmates.models.post.Publication;
import com.yamo.skillsmates.models.post.PublicationType;
import com.yamo.skillsmates.notifications.services.LibNotificationService;
import com.yamo.skillsmates.pages.services.PostService;
import com.yamo.skillsmates.repositories.multimedia.MediaSubTypeRepository;
import com.yamo.skillsmates.repositories.multimedia.MediaTypeRepository;
import com.yamo.skillsmates.repositories.multimedia.MetadataRepository;
import com.yamo.skillsmates.repositories.multimedia.MultimediaRepository;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.PublicationRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.specifications.ModelSpecification;
import com.yamo.skillsmates.specifications.SearchCriteria;
import com.yamo.skillsmates.specifications.SearchOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hornetq.utils.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@Transactional
public class PostServiceImpl implements PostService {
    private final SocialInteractionRepository socialInteractionRepository;
    private final MultimediaHelper multimediaHelper;
    private final MultimediaRepository multimediaRepository;
    private final PostRepository postRepository;
    private final MediaTypeRepository mediaTypeRepository;
    private final MediaSubTypeRepository mediaSubTypeRepository;
    private final MetadataRepository metadataRepository;
    private final LibAccountService libAccountService;
    private final LibNotificationService libNotificationService;
    private final PublicationRepository publicationRepository;

    @Value("${media.directory.root}")
    protected String rootDirectory;
    @Value("${ms.skillsmates.media.aws.accessKey}")
    protected String awsAccessKey;
    @Value("${ms.skillsmates.media.aws.secretKey}")
    protected String secretKey;
    @Value("${ms.skillsmates.media.aws.s3bucket.name}")
    protected String awsS3Bucket;

    @Autowired
    public PostServiceImpl(SocialInteractionRepository socialInteractionRepository, MultimediaHelper multimediaHelper, MultimediaRepository multimediaRepository, PostRepository postRepository, MediaTypeRepository mediaTypeRepository, MediaSubTypeRepository mediaSubTypeRepository, MetadataRepository metadataRepository, LibAccountService libAccountService, LibNotificationService libNotificationService, PublicationRepository publicationRepository) {
        this.socialInteractionRepository = socialInteractionRepository;
        this.multimediaHelper = multimediaHelper;
        this.multimediaRepository = multimediaRepository;
        this.postRepository = postRepository;
        this.mediaTypeRepository = mediaTypeRepository;
        this.mediaSubTypeRepository = mediaSubTypeRepository;
        this.metadataRepository = metadataRepository;
        this.libAccountService = libAccountService;
        this.libNotificationService = libNotificationService;
        this.publicationRepository = publicationRepository;
    }

    @Override
    public Post savePost(Post post, MultipartFile multipartFile) throws GenericException {
        try {
            libAccountService.findAccountByIdServer(post.getAccount().getIdServer());
            savePostMultimedia(post, multipartFile);
            post.setIdServer(String.valueOf(System.nanoTime()));
            return postRepository.save(post);
        } catch (IOException | JSONException e) {
            throw new GenericException(ResponseMessage.ERROR_SAVING_POST.getMessage());
        }
    }

    @Override
    public Post saveNewPost(PostDto postDto, MultipartFile multipartFile) throws GenericException {
        try {
            Post post = new Post();
            post.setIdServer(String.valueOf(System.nanoTime()));
            post.setTitle(postDto.getTitle());
            post.setDescription(postDto.getDescription());
            post.setType(postDto.getType());
            post.setKeywords(postDto.getKeywords());
            post.setDiscipline(postDto.getDiscipline());
            post.setAccount(libAccountService.findAccountByIdServer(postDto.getAccount().getIdServer()));
            post.setMediaSubtype(mediaSubTypeRepository.findByIdServer(postDto.getMediaSubtype().getIdServer()).orElse(null));

            savePostMultimedia(post, multipartFile, postDto.getUrl());
            post = postRepository.save(post);
            publicationRepository.save(Publication.builder().authorId(post.getAccount().getId()).
                    contentId(post.getId()).type(PublicationType.POST).build());
            return post;
        } catch (IOException | JSONException e) {
            throw new GenericException(ResponseMessage.ERROR_SAVING_POST.getMessage());
        }
    }

    @Override
    public Post savePost(Post post) throws GenericException {
        return savePost(post, null);
    }

    @Override
    public List<Post> findPosts(String accountId, Pageable pageable) throws GenericException {
        Account account = libAccountService.findAccountByIdServer(accountId);
        return postRepository.findByActiveTrueAndDeletedFalseAndAccount(account, pageable).orElse(new ArrayList<>());
    }

    @Override
    public List<Post> findPostsByContentAndMediaSubtype(String content, String labelMediaSubType, String labelMediaType, Pageable pageable) throws GenericException {

        Optional<MediaType> mediaType = mediaTypeRepository.findByLabel(labelMediaType);

        if (!mediaType.isPresent()) {
            throw new GenericException("MediaType unidentified");
        }

        Optional<MediaSubtype> mediaSubType = mediaSubTypeRepository.findByMediaTypeAndLabelIgnoreCase(mediaType.get(), labelMediaSubType);

        if (!mediaSubType.isPresent()) {
            throw new GenericException("MediaSubType unidentified");
        }
        return postRepository.findByTitleIgnoreCaseContainingAndMediaSubtype(content, mediaSubType.get(), pageable).orElse(new ArrayList<>());
    }

    @Override
    public boolean deletePost(String accountId, String postId) throws GenericException {
        Account account = libAccountService.findAccountByIdServer(accountId);

        Optional<Post> post = postRepository.findByIdServer(postId);
        if (!post.isPresent()) {
            throw new GenericException(ResponseMessage.POST_NOT_FOUND.getMessage());
        }
        if (!post.get().getAccount().getIdServer().equals(account.getIdServer())) {
            throw new GenericException(ResponseMessage.POST_DOES_NOT_BELONG_TO_ACCOUNT.getMessage());
        }
//        post.get().getMultimedia().forEach(multimedia -> multimedia.setDeleted(true));
        if (post.get().getMultimedia() != null) {
            post.get().getMultimedia().setDeleted(true);
        }
        post.get().setDeleted(true);
        publicationRepository.
                findByAuthorIdAndContentId(account.getId(), post.get().getId())
                .ifPresent(publicationRepository::delete);
        postRepository.save(post.get());
        return true;
    }

    @Override
    public Post findPostByIdServer(String idServer) {
        return postRepository.findByIdServer(idServer).orElse(null);
    }

    @Override
    public Post updatePost(Post post, MultipartFile multipartFile) throws GenericException {
        try {
            Optional<Post> existingPost = postRepository.findByIdServer(post.getIdServer());
            if (!existingPost.isPresent()) {
                throw new GenericException(ResponseMessage.POST_NOT_FOUND.getMessage());
            }
            post.setIdServer(existingPost.get().getIdServer());
            post.setId(existingPost.get().getId());

            // check if multimedia have been edited
            if (multipartFile != null && havePostMultimediaBeenEdited(post, existingPost.get(), multipartFile)) {
                deletePostMultimedia(existingPost.get());
                savePostMultimedia(post, multipartFile);
            }

            postRepository.save(post);
            return post;
        } catch (GenericException | JSONException | IOException e) {
            throw new GenericException("Error updating post", e);
        }
    }

    @Override
    public Post updateNewPost(PostDto postDto, MultipartFile multipartFile) throws GenericException {
        try {
            Optional<Post> existingPost = postRepository.findByIdServer(postDto.getIdServer());
            if (!existingPost.isPresent()) {
                throw new GenericException(ResponseMessage.POST_NOT_FOUND.getMessage());
            }

            existingPost.get().setTitle(postDto.getTitle());
            existingPost.get().setDescription(postDto.getDescription());
            existingPost.get().setType(postDto.getType());
            existingPost.get().setKeywords(postDto.getKeywords());
            existingPost.get().setDiscipline(postDto.getDiscipline());
            existingPost.get().setMediaSubtype(mediaSubTypeRepository.findByIdServer(postDto.getMediaSubtype().getIdServer()).orElse(null));

            boolean alreadyInDb;
            if (multipartFile != null) {
                alreadyInDb = isFileAlreadyUploaded(existingPost.get(), multipartFile);
            } else {
                alreadyInDb = isUrlAlreadyUploaded(existingPost.get(), postDto.getUrl());
            }

            // check if multimedia have been edited
            if (multipartFile != null && !alreadyInDb) {
                deletePostMultimedia(existingPost.get());
                savePostMultimedia(existingPost.get(), multipartFile, postDto.getUrl());
            }

            postRepository.save(existingPost.get());
            return existingPost.get();
        } catch (GenericException | JSONException | IOException e) {
            throw new GenericException("Error updating post", e);
        }
    }

    @Override
    public Post updatePost(Post post) throws GenericException {
        return updatePost(post, null);
    }

    @Override
    public List<PostSocialInteractions> searchPosts(Specification<Post> specification, int page, int limit) throws GenericException {
        List<Post> posts = postRepository.findAll(specification, PageRequest.of(page, limit)).getContent();
        return convertToPostSocialInteractions(posts);
    }

    @Override
    public Long countSearchPosts(Specification<Post> specification) throws GenericException {
        return postRepository.count(specification);
    }

    @Override
    public Long countPosts(Account account) throws GenericException {
        return postRepository.countByActiveTrueAndDeletedFalseAndAccount(account);
    }

    @Override
    public Page<Post> findPageSearchPosts(Specification<Post> specification, int page, int limit) throws GenericException {
        return postRepository.findAll(specification, PageRequest.of(page, limit));
    }

    @Override
    public Specification<Post> generatePostSpecification(SearchParam searchParam) {
        return generatePostSpecification(searchParam, null);
    }

    public Specification<Post> generatePostSpecification(SearchParam searchParam, MediaSubtypeDto mediaSubtypeDto) {

        Specification<Post> specification = null;

        if (mediaSubtypeDto != null) {
            Optional<MediaSubtype> mediaSubtype = mediaSubTypeRepository.findByIdServer(mediaSubtypeDto.getIdServer());
            if (mediaSubtype.isPresent()) {
                ModelSpecification<Post> mediaSpec = new ModelSpecification<>();
                mediaSpec.add(new SearchCriteria(Post.MEDIA_SUBTYPE_COLUMN, mediaSubtype.get().getId(), SearchOperation.EQUAL));
                specification = specification != null ? mediaSpec.and(specification) : Specification.where(mediaSpec);
            }
        }

        if (StringUtils.isNotBlank(searchParam.getContent())) {
            Specification<Post> searchParamSpeccification = null;
            List<String> toIgnore = Arrays.asList("le", "la", "l'", "les", "mon", "ma", "mes", "m'", "en", "de", "du", "des", "d'", "se", "s'", "je", "j'ai");
            List<String> searchParamList = Stream.of(searchParam.getContent().split(" ")).map(String::trim).filter(e -> !toIgnore.contains(e)).collect(Collectors.toList());
            for (String stringToSearch : searchParamList) {
                ModelSpecification<Post> titleSpec = new ModelSpecification<>();
                titleSpec.add(new SearchCriteria(Post.TITLE_COLUMN, stringToSearch, SearchOperation.MATCH));
                ModelSpecification<Post> keywordsSpec = new ModelSpecification<>();
                keywordsSpec.add(new SearchCriteria(Post.KEYWORDS_COLUMN, stringToSearch, SearchOperation.MATCH));
                ModelSpecification<Post> descriptionSpec = new ModelSpecification<>();
                descriptionSpec.add(new SearchCriteria(Post.DESCRIPTION_COLUMN, stringToSearch, SearchOperation.MATCH));
                Specification<Post> allFieldsSearch = titleSpec.or(keywordsSpec).or(descriptionSpec);
                searchParamSpeccification = searchParamSpeccification != null ? searchParamSpeccification.and(allFieldsSearch) : Specification.where(allFieldsSearch);
            }
            specification = specification != null ? specification.and(searchParamSpeccification) : Specification.where(searchParamSpeccification);
        }

        if (specification != null) {
            ModelSpecification<Post> baseAccountSpec = new ModelSpecification<>();
            baseAccountSpec.add(new SearchCriteria(BaseActiveModel.ACTIVE_COLUMN, true, SearchOperation.EQUAL));
            baseAccountSpec.add(new SearchCriteria(BaseActiveModel.DELETED_COLUMN, false, SearchOperation.EQUAL));
            specification = specification.and(baseAccountSpec);
        }
        return specification;
    }

    @Override
    public List<PostSocialInteractions> findPostsSocialInteractions(String accountId, Pageable pageable) throws GenericException {
        Account account = libAccountService.findAccountByIdServer(accountId);
        List<Publication> publications = publicationRepository.findByAuthorIdOrderByDateDesc(account.getId(), pageable);
        return fromPublicationToPostSocialInteraction(publications);
    }

    @Override
    public List<PostSocialInteractions> findDashboardPostsSocialInteractions(String accountId, int page, int limit) throws GenericException {
        Account account = libAccountService.findAccountByIdServer(accountId);
        List<Integer> ids = libAccountService.findFollowees(account).parallelStream().map(Account::getId).collect(Collectors.toList());
        ids.add(account.getId());

        List<Publication> publications = publicationRepository.findByAuthorIdInOrderByDateDesc(ids, PageRequest.of(page, limit));
        return fromPublicationToPostSocialInteraction(publications);
    }

    @Override
    public PostSocialInteractions findSinglePostSocialInteractions(String postId) throws GenericException {
        Optional<Post> post = postRepository.findByIdServer(postId);
        if (!post.isPresent()) {
            throw new GenericException(ResponseMessage.POST_NOT_FOUND.getMessage());
        }
        PostSocialInteractions postSocialInteractions = new PostSocialInteractions();
        postSocialInteractions.setPost(post.get());
        postSocialInteractions.setSocialInteractions(socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(post.get().getIdServer()));
        return postSocialInteractions;
    }

    private Specification<Post> generateMediaSubtypeSpecifications(Specification<Post> specification, List<MediaSubtype> mediaSubtypes) {
        if (mediaSubtypes != null && !mediaSubtypes.isEmpty()) {
            int index = 0;
            for (MediaSubtype mediaSubtype : mediaSubtypes) {
                ModelSpecification<Post> mediaSpec = new ModelSpecification<>();
                mediaSpec.add(new SearchCriteria(Post.MEDIA_SUBTYPE_COLUMN, mediaSubtype.getId(), SearchOperation.EQUAL));
                if (index == 0) {
                    specification = specification.and(mediaSpec);
                } else {
                    specification = mediaSpec.or(specification);
                }
                index++;
            }
        }
        return specification;
    }

    /**
     * get type of multimedia
     *
     * @param multimedia
     * @return
     */
    private TypeMultimedia getTypeMultimedia(Multimedia multimedia) {
        if (multimedia.getMimeType().startsWith("image")) {
            return TypeMultimedia.IMAGE;
        } else if (multimedia.getMimeType().startsWith("application/pdf")) {
            return TypeMultimedia.DOCUMENT;
        } else if (multimedia.getMimeType().startsWith("audio")) {
            return TypeMultimedia.AUDIO;
        } else if (multimedia.getMimeType().startsWith("video")) {
            return TypeMultimedia.VIDEO;
        } else {
            return TypeMultimedia.DOCUMENT;
        }
    }

    /**
     * get media type of a multimedia
     *
     * @param multimedia
     * @return
     */
    private MediaType getMediaType(Multimedia multimedia) {
        for (MediaType mediaType : mediaTypeRepository.findAll()) {
            if (mediaType.getLabel().equals(getTypeMultimedia(multimedia).name())) {
                return mediaType;
            }
        }
        return null;
    }

    /**
     * save multimedia of a post
     *
     * @param post
     * @param multipartFile
     * @return
     * @throws IOException
     * @throws GenericException
     * @throws JSONException
     */
    private Post savePostMultimedia(Post post, MultipartFile multipartFile) throws IOException, GenericException, JSONException {
//        if (multipartFile != null) {
//            return uploadMultimediaFiles(post, multipartFile);
//        } else if (post.getMultimedia() != null){ /* Gestion des urls des videos ou documents*/
//            return uploadLinkFiles(post);
//        }
        return savePostMultimedia(post, multipartFile, post.getMultimedia().getUrl());
    }

    private Post savePostMultimedia(Post post, MultipartFile multipartFile, String url) throws IOException, GenericException, JSONException {
        if (multipartFile != null) {
            return uploadMultimediaFiles(post, multipartFile);
        } else if (StringUtils.isNotBlank(url)) { /* Gestion des urls des videos ou documents*/
            return uploadLinkFiles(post, url);
        }
        return post;
    }

    /**
     * send notifications to all my followers
     *
     * @param post: post
     * @throws GenericException: throw an exception if something goes wrong
     */
    private void notifyFollowers(Post post) throws GenericException {
        List<Account> followers = libAccountService.findFollowers(post.getAccount());

        // Send notification to all my followers
        for (Account follower : followers) {
            libNotificationService.addNotification(post.getAccount(), post.getIdServer(), follower, NotificationTypeEnum.POST.name());
        }
    }

    /**
     * upload files into AWS S3
     *
     * @param post
     * @param multipartFile
     * @return
     * @throws GenericException
     * @throws IOException
     */
    private Post uploadMultimediaFiles(Post post, MultipartFile multipartFile) throws GenericException, IOException {
        if (!multimediaHelper.validateFileMimetype(multipartFile)) {
            throw new GenericException(ResponseMessage.DOCUMENT_TYPE_NOT_ALLOWED.getMessage());
        }
        MediaSubtype mediaSubtype = getMediaSubtype(post);
//        Set<Multimedia> multimediaSet = new HashSet<>();
        AwsAmazonHelper awsAmazonHelper = new AwsAmazonHelper(awsAccessKey, secretKey, awsS3Bucket, rootDirectory);
        Multimedia multimedia = new Multimedia();
        multimedia.setIdServer(String.valueOf(System.nanoTime()));
        multimedia.setAccount(post.getAccount());
        multimedia.setName(StringUtils.stripAccents(multipartFile.getOriginalFilename()));
        multimedia.setExtension(multimediaHelper.getExtension(multipartFile));
        multimedia.setMimeType(multipartFile.getContentType());
        multimedia.setChecksum(multimediaHelper.computeChecksum(multipartFile.getInputStream()));
        multimedia.setDirectory(post.getAccount().getIdServer());
        multimedia.setType(mediaSubtype != null ? mediaSubtype.getMediaType().getLabel() : getTypeMultimedia(multimedia).name());
        multimedia.setOrigin(OriginMultimedia.POST.name());

        awsAmazonHelper.upload(multimedia, multipartFile.getInputStream());
        post.setMultimedia(multimediaRepository.save(multimedia));
//        multimediaSet.add(multimedia);
//        post.setMultimedia(multimediaRepository.saveAll(multimediaSet));
        post.setMediaSubtype(mediaSubtype);
        return post;
    }

    /**
     * upload link as multimedia
     *
     * @param post
     * @return
     * @throws IOException
     * @throws JSONException
     */
    private Post uploadLinkFiles(Post post, String url) throws IOException, JSONException {
        if (StringUtils.isBlank(url)) {
            return post;
        }
        MediaSubtype mediaSubtype = getMediaSubtype(post);
//        Set<Multimedia> multimediaSet = new HashSet<>();
//        for (Multimedia multimedia: post.getMultimedia()) {
        Multimedia multimedia = post.getMultimedia();
        if (multimedia == null) {
            multimedia = new Multimedia();
            multimedia.setIdServer(String.valueOf(System.nanoTime()));
            multimedia.setAccount(post.getAccount());
        }
        multimedia.setUrl(url);
//        if (!StringUtils.isBlank(multimedia.getUrl())) {

        multimedia.setType(mediaSubtype != null ? mediaSubtype.getMediaType().getLabel() : TypeMultimedia.LINK.name());
        multimedia.setOrigin(OriginMultimedia.POST.name());

        String html = MetadataHelper.getUrlContents(url);
        Metadata metadata = GenericMapper.INSTANCE.asEntity(MetadataHelper.parseHtml(html, url));
        metadata.setIdServer(String.valueOf(System.nanoTime()));
        multimedia.setMetadata(metadataRepository.save(metadata));

        post.setMultimedia(multimediaRepository.save(multimedia));

//                multimediaSet.add(multimedia);
//        }
//        }

        post.setMediaSubtype(mediaSubtype);
        return post;
    }

    /**
     * delete multimedia and metadata => set active to false and deleted to true
     *
     * @param post
     * @return
     * @throws GenericException
     */
    private boolean deletePostMultimedia(Post post) throws GenericException {
        if (post.getMultimedia() == null) {
            return true;
        }
//        Set<Multimedia> multimediaSet = new HashSet<>();
//        for (Multimedia multimedia: post.getMultimedia()){
        Multimedia multimedia = post.getMultimedia();
        if (StringUtils.isNotBlank(multimedia.getUrl())) {
            multimedia.getMetadata().setActive(false);
            multimedia.getMetadata().setDeleted(true);
        }
        multimedia.setActive(false);
        multimedia.setDeleted(true);
//            multimediaSet.add(multimedia);
//        }
        multimediaRepository.save(multimedia);
        return true;
    }

    /**
     * check if multimedia have to be deleted
     * multimedia are to be deleted if incoming file or url do not already exist in db
     *
     * @param newPost:      incoming post
     * @param existingPost: existing post
     * @param file:         incoming file
     * @throws GenericException: if something goes wrong
     * @throws IOException:      if unable to read file
     */
    private boolean havePostMultimediaBeenEdited(Post newPost, Post existingPost, MultipartFile file) throws GenericException, IOException {
        if (file != null) {
            return !isFileAlreadyUploaded(existingPost, file);
        } else {
            return !isUrlAlreadyUploaded(existingPost, newPost.getMultimedia().getUrl());
        }
    }

    /**
     * check if file has already been uploaded in the db
     *
     * @param post: existing post
     * @param file: incoming file
     * @return true if the file is already in db, false otherwise
     * @throws IOException if something goes wrong
     */
    private boolean isFileAlreadyUploaded(Post post, MultipartFile file) throws IOException {
        if (file == null) {
            return true;
        }
        Multimedia multimedia = post.getMultimedia();
        if (multimedia == null) {
            return false;
        }
        String checksum = multimediaHelper.computeChecksum(file.getInputStream());
//        for (Multimedia m: multimedia){
//        Multimedia m = post.getMultimedia();
        return StringUtils.isBlank(multimedia.getUrl()) && checksum.equals(multimedia.getChecksum());
//        }
    }

    /**
     * check if URL ihas alreeady been uploaded in db
     *
     * @param post: existing post
     * @param url:  incoming url
     * @return true if url already in db, false otherwise
     */
    private boolean isUrlAlreadyUploaded(Post post, String url) {
        Multimedia multimedia = post.getMultimedia();
        if (multimedia == null) {
            return false;
        }
//        for (Multimedia m: multimedia){
        return StringUtils.isNotBlank(multimedia.getUrl()) && url.equals(multimedia.getUrl());
//        }
    }

    private MediaSubtype getMediaSubtype(Post post) {
        return mediaSubTypeRepository.findByIdServer(post.getMediaSubtype().getIdServer()).orElse(null);
    }

    private List<PostSocialInteractions> convertToPostSocialInteractions(List<Post> posts) {
        List<PostSocialInteractions> postsSocialInteractions = new ArrayList<>();
        for (Post post : posts) {
            PostSocialInteractions postSocialInteractions = new PostSocialInteractions();
            postSocialInteractions.setId(post.getId());
            postSocialInteractions.setIdServer(post.getIdServer());
            postSocialInteractions.setPost(post);
            postSocialInteractions.setSocialInteractions(socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(post.getIdServer()));
            postsSocialInteractions.add(postSocialInteractions);
        }
        return postsSocialInteractions;
    }

    private List<PostSocialInteractions> fromPublicationToPostSocialInteraction(List<Publication> publications) {
        List<PostSocialInteractions> postsSocialInteractions = new ArrayList<>();
        for (Publication publication : publications) {
            PostSocialInteractions postSocialInteractions = new PostSocialInteractions();
            postSocialInteractions.setShared(publication.getType().equals(PublicationType.SHARE));
            if (postSocialInteractions.isShared()) {
                postSocialInteractions.setShareSocialInteraction(socialInteractionRepository.findById(publication.getContentId())
                        .orElse(null));
                postSocialInteractions.setPost(postRepository.findByIdServer(postSocialInteractions.getShareSocialInteraction()
                        .getSocialInteractionMap().getElement()).orElse(null));
            } else {
                postSocialInteractions.setPost(postRepository.findById(publication.getContentId()).orElse(null));
            }
            postSocialInteractions.setSocialInteractions(socialInteractionRepository.
                    findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(postSocialInteractions.getPost().getIdServer())
            );
            postsSocialInteractions.add(postSocialInteractions);
        }
        return postsSocialInteractions;
    }
}
