package com.yamo.skillsmates.pages.http.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamo.skillsmates.pages.enums.TypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailDto extends BaseDto{

    @NotNull(message = "message is required")
    @Valid
    @JsonProperty("message")
    private MessageDto message;

    @JsonProperty("attachments")
    private List<AttachmentDto> attachments;

    @JsonProperty("values")
    private List<KeyValueDto> values;

    @JsonProperty("template")
    private String template;

    @NotNull(message = "type of template is required")
    @JsonProperty("type")
    private TypeEnum type;
}
