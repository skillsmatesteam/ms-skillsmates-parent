package com.yamo.skillsmates.admin.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.version.Version;

import java.util.List;

public interface VersionService {
    List<Version> findAllVersions();
    List<Version> saveVersion(List<Version> versions) throws GenericException;
}
