package com.yamo.skillsmates.media.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-media/metadata", produces = MediaType.APPLICATION_JSON_VALUE)
public interface MetadataApi {
    @GetMapping("/preview/{url}")
    ResponseEntity<GenericResultDto> getMetadata(String token, String id, String url) throws GenericException;
}
