package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.enums.TemplateKeysEnum;
import com.yamo.skillsmates.accounts.http.EmailService;
import com.yamo.skillsmates.accounts.http.dto.EmailDto;
import com.yamo.skillsmates.accounts.http.dto.KeyValueDto;
import com.yamo.skillsmates.accounts.http.dto.MessageDto;
import com.yamo.skillsmates.accounts.services.PasswordService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.services.LibAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class PasswordServiceImpl implements PasswordService {

    private final LibAccountService libAccountService;
    private final EmailService emailService;

    @Value("${ms.skillsmates.hostname}")
    private String hostname;

    @Autowired
    public PasswordServiceImpl(LibAccountService libAccountService, EmailService emailService) {
        this.libAccountService = libAccountService;
        this.emailService = emailService;
    }

    @Override
    public Account create(String identifier, String newPassword) throws GenericException {
        try {
            Account account = libAccountService.findAccountByIdServer(identifier);
            account.setPassword(StringHelper.generateSecurePassword(newPassword, account.getEmail()));
            account = libAccountService.updateAccount(account);

            List<KeyValueDto> values = new ArrayList<>();
            values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_USERNAME.name(), account.getFirstname() + " " + account.getLastname()));
            values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(),  hostname + "/login"));
            values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_TITLE.name(),  "Password modifié"));
            values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_MESSAGE.name(),  "Bienvenue "  + account.getFirstname()));
            values.add(new KeyValueDto(TemplateKeysEnum.CTA_END_TEXT.name(),  "pour afficher le message"));

            sendEmail(account, values, "Password modifié", "account.ftl");
            return account;
        } catch (Exception e) {
            throw new GenericException("Error occurred while creating password");
        }
    }

    private void sendEmail(Account account, List<KeyValueDto> values, String subject, String template) {
        EmailDto emailDto = new EmailDto();

        emailDto.setTemplate(template);

        MessageDto messageDto = new MessageDto();
        messageDto.setFrom(account.getEmail());
        messageDto.setTo(Collections.singletonList(account.getEmail()));
        messageDto.setBody(subject);
        messageDto.setSubject(subject);
        emailDto.setMessage(messageDto);
        emailDto.setValues(values);
        emailDto.setAttachments(new ArrayList<>());
        emailService.sendEmail(emailDto);
    }

}
