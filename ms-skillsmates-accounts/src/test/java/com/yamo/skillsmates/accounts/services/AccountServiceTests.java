package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.accounts.services.impl.AccountServiceImpl;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTests extends AbstractServiceTests{

    public AccountService accountService;

    @BeforeEach
    public void setup(){
        accountService = new AccountServiceImpl(accountRepository, mailService, cryptUtil, libAccountService, activityAreaRepository, libSocialInteractionService, socialInteractionRepository, postRepository, emailService, socialInteractionTypeRepository, socialInteractionMapRepository);
    }

    @Test
    public void accountShouldAlreadyExists(){
        setup();
        Account account = generateAccount();
        Mockito.when(accountRepository.findByDeletedFalseAndEmail(account.getEmail())).thenReturn(Optional.of(account));

        assertThatThrownBy(() -> accountService.saveAccount(account))
                .isInstanceOf(GenericException.class)
                .hasMessage(ResponseMessage.ACCOUNT_ALREADY_EXISTS.getMessage());

    }
}
