package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.account.AccountFilterDto;
import com.yamo.skillsmates.models.account.Professional;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.account.config.ActivitySector;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProfessionalService {
    Professional saveProfessional(Professional Professional) throws GenericException;
    List<Professional> findProfessionals(String accountId, Pageable pageable) throws GenericException;
    boolean deleteProfessional(String idServer, String accountId) throws GenericException;
    List<ActivityArea> findActivityAreas();
    List<ActivitySector> findActivitySectors();
    long countProfessionals(String accountId) throws GenericException;
    List<AccountDto> filterAccounts(AccountFilterDto accountFilterDto);
}
