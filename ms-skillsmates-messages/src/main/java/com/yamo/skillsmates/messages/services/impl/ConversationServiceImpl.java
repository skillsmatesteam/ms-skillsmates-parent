package com.yamo.skillsmates.messages.services.impl;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.messages.services.ConversationService;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.repositories.communication.ConversationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConversationServiceImpl implements ConversationService {

    private final ConversationRepository conversationRepository;

    @Autowired
    public ConversationServiceImpl(ConversationRepository conversationRepository) {
        this.conversationRepository = conversationRepository;
    }

    @Override
    public Conversation saveConversation(String idServer) throws GenericException {

        Optional<Conversation> conversation  = conversationRepository.findByIdServer(idServer);

        if(!conversation.isPresent()){
            Conversation newConversation = new Conversation();
            newConversation.setName("simpleChat");
            newConversation.setIdServer(idServer);
            return conversationRepository.save(newConversation);
        }

        return conversation.get();
    }

    @Override
    public Optional<Conversation> findConversationByIdServer(String idServer) throws GenericException{
        return conversationRepository.findByIdServer(idServer);
    }
}
