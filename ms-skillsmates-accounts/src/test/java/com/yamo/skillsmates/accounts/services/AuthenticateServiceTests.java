package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.accounts.http.EmailService;
import com.yamo.skillsmates.accounts.services.impl.AuthenticateServiceImpl;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.CryptUtil;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

public class AuthenticateServiceTests extends AbstractServiceTests{

    private final LibAccountService libAccountService = Mockito.mock(LibAccountService.class);
    private final EmailService emailService = Mockito.mock(EmailService.class);
    private final JwtTokenUtil jwtTokenUtil = Mockito.mock(JwtTokenUtil.class);
    private final CryptUtil cryptUtil = Mockito.mock(CryptUtil.class);

    public AuthenticateService authenticateService;

    @BeforeEach
    public void setup(){
        authenticateService = new AuthenticateServiceImpl(libAccountService, emailService, jwtTokenUtil, cryptUtil);
    }

    @Test
    public void shouldFireAuthenticate() throws GenericException {
        setup();
        Account account = generateAccount();
        Mockito.when(libAccountService.authenticate(account.getEmail(), account.getPassword())).thenReturn(account);

        authenticateService.authenticate(emailExists, password);

        Mockito.verify(libAccountService, Mockito.times(1)).authenticate(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
    }
}
