package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.PasswordApi;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.mail.enums.TemplateEnum;
import com.yamo.skillsmates.validators.CryptUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PasswordController extends AbstractController implements PasswordApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordController.class);

    @Value("${ms.skillsmates.resources.baseurl}")
    private String baseUrl;
    @Value("${ms.skillsmates.accounts.url}")
    private String accountUrl;
    @Value("${ms.skillsmates.accounts.link}")
    private String accountLink;
    @Value("${ms.skillsmates.resources.baseurlnewpassword}")
    private String baseUrlNewPassword;
    @Value("${ms.skillsmates.hostname}")
    private String hostname;
    @Autowired
    private CryptUtil cryptUtil;

    @Override
    public String resetPasswordAccount(Model model, @PathVariable("accountId") String accountId, @PathVariable("token") String token) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            model.addAttribute("id", accountId);
            model.addAttribute("token", token);
            model.addAttribute("BASE_URL", baseUrl);
            model.addAttribute("BASE_URL_NEW_PASSWORD", accountUrl + accountLink + baseUrlNewPassword);
            model.addAttribute("accountDto", new AccountDto());
            model.addAttribute("loading", true);
            validateToken(token, cryptUtil.decrypt(accountId));
            LOGGER.info("{}" , LoggingUtil.END);
            return TemplateEnum.CONFIRM_RESET_PASSWORD.getValue();
        }catch (Exception e){
            model.addAttribute("exception",e);
            return TemplateEnum.ERROR_RESET_PASSWORD.getValue();
        }
    }

    @Override
    public String newPasswordAccount(Model model, @ModelAttribute("accountDto") AccountDto accountDto,
                                     String idServer, String token) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        model.addAttribute("id", idServer);
        model.addAttribute("token", token);
        model.addAttribute("BASE_URL", baseUrl);
        model.addAttribute("BASE_URL_NEW_PASSWORD", accountUrl + accountLink + baseUrlNewPassword);
        model.addAttribute("accountDto", accountDto);

        try{
            idServer = cryptUtil.decrypt(idServer);
            validateAccountParameters(accountDto);
            validateToken(token, idServer);

            passwordService.create(idServer, accountDto.getConfirmPassword());
            jwtTokenUtil.invalidateToken(idServer);
            model.addAttribute("loading", false);
            LOGGER.info("{}" , LoggingUtil.END);
            return "redirect:" + hostname;
        }catch (Exception e){
            model.addAttribute("loading", true);
            model.addAttribute("exception",e);
            return TemplateEnum.CONFIRM_RESET_PASSWORD.getValue();
        }
    }
    private void validateAccountParameters(AccountDto accountDto) throws GenericException{
        if (StringUtils.isBlank(accountDto.getPassword()) || StringUtils.isBlank(accountDto.getConfirmPassword())){
            throw new GenericException("Invalid input data");
        }
        else if (accountDto.getPassword().length() < 5 || accountDto.getConfirmPassword().length() < 5){
            throw new GenericException("Le mot de passe doit avoir au moins 5 caractères");
        }
        else if (!accountDto.getPassword().equals(accountDto.getConfirmPassword())){
            throw new GenericException("Les mots de passes sont différents");
        }
    }

    private void validateToken(String token, String id) throws Exception {
        if (StringUtils.isBlank(token) || StringUtils.isBlank(id)){
            throw new GenericException("Invalid input data");
        }
        if (!jwtTokenUtil.validateToken(token, id)) {
            throw new GenericException("Invalid token");
        }
    }
}
