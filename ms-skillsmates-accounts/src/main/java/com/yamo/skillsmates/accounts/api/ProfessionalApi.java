package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountFilterDto;
import com.yamo.skillsmates.dto.account.ProfessionalDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-accounts/accounts/professional", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ProfessionalApi {
    @PostMapping
    ResponseEntity<GenericResultDto> createProfessional(String token, String id, ProfessionalDto professionalDto) throws GenericException;

    @GetMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> findProfessionalsById(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/offline/{accountId}")
    ResponseEntity<GenericResultDto> findProfessionalsOfflineById(String accountId) throws GenericException;

    @DeleteMapping
    ResponseEntity<GenericResultDto> deleteProfessional(String token, String id, String professionalId) throws GenericException;

    @PostMapping(value = "/filter")
    ResponseEntity<GenericResultDto> filterProfessionals(String token, String id, AccountFilterDto accountFilterDto) throws GenericException;

}
