package com.yamo.skillsmates.pages.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.dto.post.SearchParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/skillsmates-pages/posts", produces = MediaType.APPLICATION_JSON_VALUE)
public interface PostApi {
    @PostMapping
    ResponseEntity<GenericResultDto> createPost(String token, String id, PostDto postDto) throws GenericException;

    @GetMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> findPosts(String token, String id, String page, String limit, String accountId) throws GenericException;

    @GetMapping(value = "/offline/{accountId}")
    ResponseEntity<GenericResultDto> findPostsOffline(String page, String limit, String accountId) throws GenericException;

    @GetMapping(value = "/dashboard/{accountId}")
    ResponseEntity<GenericResultDto> findDashboardPosts(String token, String id, String page, String limit, String accountId) throws GenericException;

    @GetMapping(value = "/single/{postId}")
    ResponseEntity<GenericResultDto> findSinglePost(String token, String id, String postId) throws GenericException;

    @DeleteMapping
    ResponseEntity<GenericResultDto> deletePost(String token, String id, String postId) throws GenericException;

    @GetMapping(value = "/generateUrl/{postId}")
    ResponseEntity<GenericResultDto> generateUrlPost(String token, String id, String postId) throws GenericException;

    @GetMapping(value = "/view/{postId}")
    ResponseEntity<GenericResultDto> viewPostOffline(String postId, String page, String limit) throws GenericException;

    @PutMapping
    ResponseEntity<GenericResultDto> updatePost(String token, String id, PostDto postDto) throws GenericException;

//    @GetMapping(value = "/searchPosts")
//    ResponseEntity<GenericResultDto> searchPosts(String token, String id, String content, String mediaSubType, int page, int limit) throws GenericException;

    @PostMapping(value = "/searchPosts")
    ResponseEntity<GenericResultDto> searchPosts(String token, String id, SearchParam searchParam, int page, int limit) throws GenericException;

}
