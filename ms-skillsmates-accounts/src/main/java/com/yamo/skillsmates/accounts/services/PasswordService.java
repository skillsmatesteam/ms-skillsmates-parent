package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;

public interface PasswordService {
    Account create(String identifier, String newPassword) throws GenericException;
}
