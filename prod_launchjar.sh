#!/bin/bash
#
# Cyrille MOFFO
#
# Script d'execution des jars skillsmates
#

# kill processes (ports starting with 9 are for prod)
sudo kill -9 $(sudo lsof -t -i:9001)
sudo kill -9 $(sudo lsof -t -i:9002)
sudo kill -9 $(sudo lsof -t -i:9003)
sudo kill -9 $(sudo lsof -t -i:9004)
sudo kill -9 $(sudo lsof -t -i:9005)
sudo kill -9 $(sudo lsof -t -i:9006)
sudo kill -9 $(sudo lsof -t -i:9007)
sudo kill -9 $(sudo lsof -t -i:9008)

# launch processes
nohup java -jar ms-skillsmates-accounts.jar > ms-skillsmates-accounts.log 2>&1 &
nohup java -jar ms-skillsmates-communications.jar > ms-skillsmates-communications.log 2>&1 &
nohup java -jar ms-skillsmates-media.jar > ms-skillsmates-media.log 2>&1 &
nohup java -jar ms-skillsmates-notifications.jar > ms-skillsmates-notifications.log 2>&1 &
nohup java -jar ms-skillsmates-pages.jar > ms-skillsmates-pages.log 2>&1 &
nohup java -jar ms-skillsmates-settings.jar > ms-skillsmates-settings.log 2>&1 &