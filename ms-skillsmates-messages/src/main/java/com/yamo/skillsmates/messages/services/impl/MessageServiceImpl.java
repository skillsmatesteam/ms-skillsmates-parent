package com.yamo.skillsmates.messages.services.impl;

import com.yamo.skillsmates.common.enumeration.OriginMultimedia;
import com.yamo.skillsmates.common.enumeration.TypeMultimedia;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.mail.service.LibConversationService;
import com.yamo.skillsmates.mail.service.LibMessageService;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.media.helpers.AwsAmazonHelper;
import com.yamo.skillsmates.media.helpers.MetadataHelper;
import com.yamo.skillsmates.media.helpers.MultimediaHelper;
import com.yamo.skillsmates.messages.services.MessageService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.multimedia.Metadata;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.repositories.communication.MessageRepository;
import com.yamo.skillsmates.repositories.multimedia.MetadataRepository;
import com.yamo.skillsmates.repositories.multimedia.MultimediaRepository;
import org.apache.commons.lang3.StringUtils;
import org.hornetq.utils.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;
    private final MultimediaRepository multimediaRepository;
    private final MultimediaHelper multimediaHelper;
    private final MetadataRepository metadataRepository;
    private final LibMessageService libMessageService;
    private final LibConversationService libConversationService;

    @Value("${media.directory.root}")
    protected String rootDirectory;
    @Value("${ms.skillsmates.media.aws.accessKey}")
    protected String awsAccessKey;
    @Value("${ms.skillsmates.media.aws.secretKey}")
    protected String secretKey;
    @Value("${ms.skillsmates.media.aws.s3bucket.name}")
    protected String awsS3Bucket;

    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository, MultimediaRepository multimediaRepository, MultimediaHelper multimediaHelper, MetadataRepository metadataRepository, LibMessageService libMessageService, LibConversationService libConversationService) {
        this.messageRepository = messageRepository;
        this.multimediaRepository = multimediaRepository;
        this.multimediaHelper = multimediaHelper;
        this.metadataRepository = metadataRepository;
        this.libMessageService = libMessageService;
        this.libConversationService = libConversationService;
    }

    @Override
    public Message saveMessage(Message message) throws GenericException {
        return libMessageService.saveMessage(message);
    }

    @Override
    public Message saveMessageMedia(Message message, MultipartFile multipartFile) throws GenericException {
        try {
            Set<Multimedia> multimediaSet = new HashSet<>();
            if (multipartFile != null) {
                if (!multimediaHelper.validateFileMimetype(multipartFile)){
                    throw new GenericException("This kind of document is not allowed");
                }

                AwsAmazonHelper awsAmazonHelper = new AwsAmazonHelper(awsAccessKey, secretKey, awsS3Bucket, rootDirectory);
                Multimedia multimedia = new Multimedia();
                multimedia.setIdServer(String.valueOf(System.nanoTime()));
                multimedia.setAccount(message.getEmitterAccount());
                multimedia.setName(StringUtils.stripAccents(multipartFile.getOriginalFilename()));
                multimedia.setExtension(multimediaHelper.getExtension(multipartFile));
                multimedia.setMimeType(multipartFile.getContentType());
                multimedia.setChecksum(multimediaHelper.computeChecksum(multipartFile.getInputStream()));
                multimedia.setDirectory(message.getEmitterAccount().getIdServer());
                multimedia.setType(getTypeMultimedia(multimedia).name());
                multimedia.setOrigin(OriginMultimedia.MESSAGE.name());

                awsAmazonHelper.upload(multimedia, multipartFile.getInputStream());
                multimediaSet.add(multimedia);

            } else if (!CollectionUtils.isEmpty(message.getMultimedia())){ /* Gestion des urls des videos ou documents*/
                for (Multimedia multimedia: message.getMultimedia()) {
                    if (StringUtils.isNotBlank(multimedia.getUrl())) {
                        multimedia.setAccount(message.getEmitterAccount());
                        multimedia.setIdServer(String.valueOf(System.nanoTime()));
                        multimedia.setOrigin(OriginMultimedia.MESSAGE.name());

                        String html = MetadataHelper.getUrlContents(multimedia.getUrl());
                        Metadata metadata = GenericMapper.INSTANCE.asEntity(MetadataHelper.parseHtml(html, multimedia.getUrl()));
                        metadata.setIdServer(String.valueOf(System.nanoTime()));
                        multimedia.setMetadata(metadataRepository.save(metadata));
                        multimediaSet.add(multimedia);
                    }
                }
            }

            message.setMultimedia(multimediaRepository.saveAll(multimediaSet));
            message.setIdServer(String.valueOf(System.nanoTime()));
            message.setActive(false); //to be active after reading
            message.setContent("");

            return messageRepository.save(message);
        }catch (IOException | JSONException e){
            throw new GenericException("Error saving document message");
        }
    }

    @Override
    public List<Message> findAllMessagesByConversation(Conversation conversation, boolean active, Pageable pageable) throws GenericException {
        List<Message> messages = libMessageService.findMessagesByConversation(conversation, pageable);
        if ( active && !CollectionUtils.isEmpty(messages)){
            messages.forEach(message -> message.setActive(true));
            messageRepository.saveAll(messages);
        }
        return messages;
    }

    @Override
    public List<Message> findUnreadMessagesByReceiver(Account receiverAccount, Account emitterAccount) throws GenericException {
        Conversation conversation = libConversationService.findConversationByIdServer(libConversationService.getConversationId(receiverAccount.getIdServer(), emitterAccount.getIdServer()));
        return messageRepository.findAllByActiveFalseAndDeletedFalseAndReceiverAccountAndConversation(receiverAccount, conversation).orElse(new ArrayList<>());
    }

    @Override
    public List<Message> findUnreadMessagesByReceiver(Account receiverAccount, Pageable pageable) throws GenericException {
        return messageRepository.findAllByActiveFalseAndDeletedFalseAndReceiverAccount(receiverAccount, pageable).orElse(new ArrayList<>());
    }

    @Override
    public List<Message> findUnreadMessagesByReceiverAndEmitter(Account receiverAccount, Account emitterAccount, Pageable pageable) throws GenericException {
        return messageRepository.findAllByActiveFalseAndDeletedFalseAndReceiverAccountAndEmitterAccount(receiverAccount, emitterAccount, pageable).orElse(new ArrayList<>());
    }

    /**
     * get type of media knowing the multimedia object
     * @param multimedia object
     * @return enum of the type of the media
     */
    private TypeMultimedia getTypeMultimedia(Multimedia multimedia){
        if (multimedia.getMimeType().startsWith("image")){
            return TypeMultimedia.IMAGE;
        }else if (multimedia.getMimeType().startsWith("application/pdf")){
            return TypeMultimedia.DOCUMENT;
        }else if (multimedia.getMimeType().startsWith("audio")){
            return TypeMultimedia.AUDIO;
        }else if (multimedia.getMimeType().startsWith("video")){
            return TypeMultimedia.VIDEO;
        }else {
            return TypeMultimedia.DOCUMENT;
        }
    }
}
