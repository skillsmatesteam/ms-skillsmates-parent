package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.AccountApi;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.DateHelper;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.*;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.dto.post.ResultFound;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.mail.util.MailUtil;
import com.yamo.skillsmates.mail.util.PasswordUtil;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.AccountMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.Post;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
public class AccountController extends AbstractController implements AccountApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);
    private static final String defaultValueSizePage = "32";
    private static final String defaultValuePage = "0";

    @Value("${ms.skillsmates.hostname}")
    private String hostname;
    @Value("${ms.skillsmates.url.profile}")
    private String baseUrlProfile;

    @Override
    public ResponseEntity<GenericResultDto> findAllAccounts(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
//            processToken(token, id);

            List<Account>  listAccount = accountService.findAllAccounts();
            for (Account account: listAccount){
                libAccountService.updateProfessionalTitleToAccount(account);
            }
            listAccount = accountService.findAllAccounts();
//            genericResultDtoSuccess(id);
            result.setResultObjects(AccountMapper.getInstance().asDtos(listAccount));
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findAccountById(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {

        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError(ResponseMessage.INPUT_DATA_INVALID.getMessage(), HttpStatus.BAD_REQUEST);
            }

            Account account = accountService.findAccountByIdServer(accountId);

            if (account == null){
                return genericResultDtoError(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage(), HttpStatus.NOT_FOUND);
            }

            result.setResultObject(account);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findOfflineAccountById(@PathVariable String accountId) throws GenericException {

        LOGGER.info("{}" , LoggingUtil.START );
        try {
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError(ResponseMessage.INPUT_DATA_INVALID.getMessage(), HttpStatus.BAD_REQUEST);
            }

            Account account = accountService.findAccountByIdServer(accountId);

            if (account == null){
                return genericResultDtoError(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage(), HttpStatus.NOT_FOUND);
            }

            result.setResultObject(account);
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findAccountInfos(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            genericResultDtoSuccess(id);
            result.setResultObject(AccountMapper.getInstance().asAccountSocialInteractionsDto(accountService.findAccountSocialInteractions(accountId)));
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findAccountInfosOffline(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            result.setResultObject(AccountMapper.getInstance().asAccountSocialInteractionsDto(accountService.findAccountSocialInteractions(accountId)));
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }


    @Override
    public ResponseEntity<GenericResultDto> updateAccount(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateUpdateAccountParameters(accountDto)){
                return genericResultDtoError(ResponseMessage.INPUT_DATA_INVALID.getMessage(), HttpStatus.BAD_REQUEST);
            }
            if (StringUtils.isNotBlank(accountDto.getCity())){
                accountDto.setCity(StringHelper.capitalizeFirstCharacter(accountDto.getCity()));
            }
            accountDto.setFirstname(StringHelper.capitalizeFirstCharacter(accountDto.getFirstname()));
            accountDto.setLastname(accountDto.getLastname().toUpperCase());
            accountDto.setBirthdate(DateHelper.isDateCorrect(accountDto.getBirthdate(), new Date()) ? accountDto.getBirthdate() : null);
            Account account = accountService.updateAccount(GenericMapper.INSTANCE.asEntity(accountDto));
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> createAccount(@RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        result  = new GenericResultDto();
        if (!validateAccountParameters(accountDto)){
            return genericResultDtoError(ResponseMessage.INPUT_DATA_INVALID.getMessage(), HttpStatus.BAD_REQUEST);
        }

        try {
            PasswordUtil.PasswordValidator(accountDto.getPassword(), accountDto);
            accountDto.setFirstname(StringHelper.capitalizeFirstCharacter(accountDto.getFirstname()));
            accountDto.setLastname(accountDto.getLastname().toUpperCase());
            accountDto.setBirthdate(DateHelper.isDateCorrect(accountDto.getBirthdate(), new Date()) ? accountDto.getBirthdate() : null);
            accountDto.setEmail(accountDto.getEmail().toLowerCase());
            Account account = GenericMapper.INSTANCE.asEntity(accountDto);
            account = accountService.saveAccount(account);
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        LOGGER.info("{}" , LoggingUtil.END);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteAccount(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError(ResponseMessage.INPUT_DATA_INVALID.getMessage(), HttpStatus.BAD_REQUEST);
            }

            accountService.deleteAccount(accountId);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public String activateAccount(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        if (StringUtils.isBlank(accountId)){
            return ResponseMessage.INPUT_DATA_INVALID.getMessage();
        }

        try {
            accountService.activateAccount(accountId);
            LOGGER.info("{}" , LoggingUtil.END);
            return "Votre compte a été activé, connectez-vous: " + hostname;
        }catch (Exception e){
            return e.getMessage();
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> logoutAccount(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            jwtTokenUtil.invalidateToken(id);
            accountService.setConnectedStatus(id, false);
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> countProfileInfos(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError(ResponseMessage.INPUT_DATA_INVALID.getMessage(), HttpStatus.BAD_REQUEST);
            }
            Account account = libAccountService.findAccountByIdServer(accountId);
            AccountProfileInfosDto accountProfileInfosDto = new AccountProfileInfosDto();
            accountProfileInfosDto.setAcademics(academicService.countAcademics(accountId));
            accountProfileInfosDto.setProfessionals(professionalService.countProfessionals(accountId));
            accountProfileInfosDto.setSkills(skillService.countSkills(accountId));
            accountProfileInfosDto.setFollowees(libAccountService.countFollowees(account));
            accountProfileInfosDto.setFollowers(libAccountService.countFollowers(account));
            accountProfileInfosDto.setFavorites(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name()));
            accountProfileInfosDto.setPosts(libSocialInteractionService.countPosts(account));
            genericResultDtoSuccess(id);
            result.setResultObject(accountProfileInfosDto);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deactivateAccount(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            accountService.deactivateAccount(accountId);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> searchAccounts(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id,
                                                           @RequestBody SearchParam searchParam,
                                                           @RequestParam(name = PAGE, defaultValue = defaultValuePage) int page,
                                                           @RequestParam(name = LIMIT, defaultValue = defaultValueSizePage) int limit) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            libAccountService.findAccountByIdServer(id);
            searchParam = retrieveStatusesFromSearchParam(searchParam);
            AccountSocialInteractionsSearchDto accountSocialInteractionsSearchDto = new AccountSocialInteractionsSearchDto();
            Specification<Account> specification = null;
            if (CollectionUtils.isEmpty(searchParam.getStatuses())){
                specification = libAccountService.generateAccountSpecification(searchParam);
            }else {
               for (Status status: searchParam.getStatuses()){
                   if (specification != null){
                       specification = specification.or(libAccountService.generateAccountSpecification(searchParam, status));
                   }else {
                       specification = libAccountService.generateAccountSpecification(searchParam, status);
                   }
               }
            }

            if (specification != null) {
                List<AccountSocialInteractionsDto> accountSocialInteractionsDtos = AccountMapper.getInstance().asAccountSocialInteractionsDtos(libAccountService.searchAccounts(specification, page, limit));
                accountSocialInteractionsSearchDto.setAccountSocialInteractions(accountSocialInteractionsDtos);
                accountSocialInteractionsSearchDto.setNumberAccounts(libAccountService.countSearchAccounts(specification));
                accountSocialInteractionsSearchDto.setPageAccounts(new int[libAccountService.findPageSearchAccounts(specification, page, limit).getTotalPages()]);
                accountSocialInteractionsSearchDto.setCurrentPage(page); // mémorise la page courante
//                ResultFound resultFound = countSocialInteraction(searchParam, page, limit);
                ResultFound resultFound = new ResultFound();
                resultFound.setProfile(accountSocialInteractionsSearchDto.getNumberAccounts().intValue());
                accountSocialInteractionsSearchDto.setResultFound(resultFound);
            }
            result.setResultObject(accountSocialInteractionsSearchDto);
            genericResultDtoSuccess(id);

            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
    @Override
    public ResponseEntity<GenericResultDto> generateUrlProfile(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Account account = accountService.findAccountByIdServer(accountId);
            if (account != null){
                AccountProfileUrlDto accountProfileUrlDto = new AccountProfileUrlDto();
                accountProfileUrlDto.setUrl(hostname + baseUrlProfile + account.getIdServer());
                result.setResultObject(accountProfileUrlDto);
                genericResultDtoSuccess(id);
            }else {
                genericResultDtoError("Account not found", HttpStatus.NOT_FOUND);
            }
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validateAccountParameters(AccountDto accountDto){
        return accountDto != null &&
                StringUtils.isNotBlank(accountDto.getLastname()) &&
                StringUtils.isNotBlank(accountDto.getFirstname()) &&
                MailUtil.isEmailValid(accountDto.getEmail()) &&
                StringUtils.isNotBlank(accountDto.getPassword()) &&
                //accountDto.getPassword().length() > 5 &&
                accountDto.getPassword().equals(accountDto.getConfirmPassword());
    }

    private boolean validateUpdateAccountParameters(AccountDto accountDto){
        return accountDto != null &&
                StringUtils.isNotBlank(accountDto.getLastname()) &&
                StringUtils.isNotBlank(accountDto.getFirstname());
    }

    private SearchParam retrieveStatusesFromSearchParam(SearchParam searchParam){
        List<Status> statuses = new ArrayList<>();
        if (searchParam != null && !CollectionUtils.isEmpty(searchParam.getMediaSubtypes())){
            for (MediaSubtypeDto mediaSubtypeDto: searchParam.getMediaSubtypes()){
                if (mediaSubtypeDto.getMediaType().getLabel().equalsIgnoreCase("PROFILE")){
                    statuses.add(Status.fromString(mediaSubtypeDto.getLabel()));
                }
            }
        }
        searchParam.setStatuses(statuses);
        return searchParam;
    }

    private ResultFound countSocialInteraction(SearchParam searchParam, int page, int limit) {
        searchParam.setMediaSubtypes(new ArrayList<>());
        ResultFound  resultFound = libAccountService.countPosts(libAccountService.generatePostSpecification(searchParam), page, limit);
        return  resultFound;
    }
}
