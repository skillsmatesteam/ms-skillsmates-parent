package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-accounts/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
public interface AuthenticateApi {
    @PostMapping
    ResponseEntity<GenericResultDto> authenticateAccount(String identifier, String password) throws GenericException;

    @PostMapping(value = "/renew-password")
    ResponseEntity<GenericResultDto> renewPasswordAccount(String identifier) throws GenericException;

    @PostMapping(value = "/email")
    ResponseEntity<GenericResultDto> verificateAccount(String email) throws GenericException;

    @PostMapping(value = "/admin")
    ResponseEntity<GenericResultDto> authenticateAdminAccount(String identifier, String password) throws GenericException;
}
