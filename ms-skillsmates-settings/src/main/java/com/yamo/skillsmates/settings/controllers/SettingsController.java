package com.yamo.skillsmates.settings.controllers;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.mail.util.MailUtil;
import com.yamo.skillsmates.mail.util.PasswordUtil;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.settings.api.SettingsApi;
import com.yamo.skillsmates.settings.enums.TemplateKeysEnum;
import com.yamo.skillsmates.settings.http.dto.EmailDto;
import com.yamo.skillsmates.settings.http.dto.KeyValueDto;
import com.yamo.skillsmates.settings.http.dto.MessageDto;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@CrossOrigin
@RestController
public class SettingsController extends AbstractController implements SettingsApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsController.class);
    @Value("${ms.skillsmates.hostname}")
    private String hostname;

    @Override
    public ResponseEntity<GenericResultDto> updateAccountByNameSetting(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateUpdateAccountParametersByNameSetting(accountDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Account account = libAccountService.findAccountByIdServer(id);
            account = libAccountService.authenticate(account.getEmail(), accountDto.getPassword());
            account.setFirstname(StringHelper.capitalizeFirstCharacter(accountDto.getFirstname()));
            account.setLastname(accountDto.getLastname().toUpperCase());
            account = libAccountService.updateAccount(account);
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updateAccountByEmailSetting(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateUpdateAccountParametersByEmailSetting(accountDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Account account = libAccountService.findAccountByIdServer(id);
            account = libAccountService.authenticate(account.getEmail(), accountDto.getPassword());

            account.setEmail(accountDto.getEmail());
            account.setPassword(StringHelper.generateSecurePassword(accountDto.getPassword(), accountDto.getEmail()));

            account = libAccountService.updateAccount(account);
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updateAccountByPhoneSetting(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateUpdateAccountParametersByPhoneNumberSetting(accountDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Account account = libAccountService.findAccountByIdServer(id);
            account = libAccountService.authenticate(account.getEmail(), accountDto.getPassword());
            account.setPhoneNumber(accountDto.getPhoneNumber());
            account = libAccountService.updateAccount(account);
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updateAccountByPasswordSetting(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateUpdateAccountParametersByPasswordSetting(accountDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            PasswordUtil.PasswordValidator(accountDto.getConfirmPassword(), accountDto);
            Account account = libAccountService.findAccountByIdServer(id);
            account = libAccountService.authenticate(account.getEmail(), accountDto.getPassword());
            account.setPassword(StringHelper.generateSecurePassword(accountDto.getConfirmPassword(), account.getEmail()));
            account = libAccountService.updateAccount(account);
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);

        }
    }

    @Override
    public ResponseEntity<GenericResultDto> resetPassword(@PathVariable String email) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {

            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteAccount(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);

            if (!validateAccountParametersBeforeDeleteOrDeactivate(accountDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Account account = libAccountService.findAccountByIdServer(id);
            account = libAccountService.authenticate(account.getEmail(), accountDto.getPassword());
            libAccountService.deleteAccount(account.getIdServer());

            List<KeyValueDto> values = new ArrayList<>();
            values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_USERNAME.name(), account.getFirstname() + " " + account.getLastname()));
            values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(),  hostname + "/login"));
            values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_TITLE.name(),  "Création de compte"));
            values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_MESSAGE.name(),  "Bienvenue "  + account.getFirstname()));
            values.add(new KeyValueDto(TemplateKeysEnum.CTA_END_TEXT.name(),  "pour afficher le message"));


            sendEmail(account, values, "Création de compte", "account.ftl");

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deactivateAccount(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody AccountDto accountDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (!validateAccountParametersBeforeDeleteOrDeactivate(accountDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Account account = libAccountService.findAccountByIdServer(id);
            account = libAccountService.authenticate(account.getEmail(), accountDto.getPassword());
            libAccountService.deactivateAccount(account.getIdServer());

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private void sendEmail(Account account, List<KeyValueDto> values, String subject, String template) {
        EmailDto emailDto = new EmailDto();

        emailDto.setTemplate(template);

        MessageDto messageDto = new MessageDto();
        messageDto.setFrom(account.getEmail());
        messageDto.setTo(Collections.singletonList(account.getEmail()));
        messageDto.setBody(subject);
        messageDto.setObject(subject);
        emailDto.setMessage(messageDto);
        emailDto.setValues(values);
        emailDto.setAttachments(new ArrayList<>());
        emailService.sendEmail(emailDto);
    }

    private boolean validateUpdateAccountParametersByNameSetting(AccountDto accountDto){
        return accountDto != null &&
                StringUtils.isNotBlank(accountDto.getLastname()) &&
                StringUtils.isNotBlank(accountDto.getFirstname()) &&
                StringUtils.isNotBlank(accountDto.getPassword()) &&
                accountDto.getPassword().length() >= 5;
    }

    private boolean validateUpdateAccountParametersByEmailSetting(AccountDto accountDto){
        return accountDto != null &&
                MailUtil.isEmailValid(accountDto.getEmail()) &&
                StringUtils.isNotBlank(accountDto.getPassword()) &&
                accountDto.getPassword().length() >= 5;
    }

    private boolean validateUpdateAccountParametersByPhoneNumberSetting(AccountDto accountDto){
        return accountDto != null &&
                StringUtils.isNotBlank(accountDto.getPhoneNumber()) &&
                StringUtils.isNotBlank(accountDto.getPassword()) &&
                accountDto.getPassword().length() >= 5;
    }

    private boolean validateUpdateAccountParametersByPasswordSetting(AccountDto accountDto){
        return accountDto != null &&
                StringUtils.isNotBlank(accountDto.getPassword()) &&
                StringUtils.isNotBlank(accountDto.getConfirmPassword());
    }

    private boolean validateAccountParametersBeforeDeleteOrDeactivate(AccountDto accountDto){
        return accountDto != null &&
                StringUtils.isNotBlank(accountDto.getPassword()) &&
                accountDto.getPassword().length() >= 5 ;
    }
}
