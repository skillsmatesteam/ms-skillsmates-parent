package com.yamo.skillsmates.messages.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.communication.Conversation;

import java.util.Optional;

public interface ConversationService {
    Conversation saveConversation(String idServer) throws GenericException;
    Optional<Conversation> findConversationByIdServer(String idServer) throws GenericException;

}
