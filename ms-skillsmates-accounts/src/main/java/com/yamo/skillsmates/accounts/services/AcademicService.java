package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Certification;
import com.yamo.skillsmates.models.account.DegreeObtained;
import com.yamo.skillsmates.models.account.HigherEducation;
import com.yamo.skillsmates.models.account.SecondaryEducation;
import com.yamo.skillsmates.models.account.config.*;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AcademicService {
    List<DegreeObtained> findDegreesObtained(String accountId, Pageable pageable) throws GenericException;
    List<Certification> findCertificates(String accountId, Pageable pageable) throws GenericException;
    DegreeObtained saveDegreeObtained(DegreeObtained degreeObtained) throws GenericException;
    Certification saveCertification(Certification certification) throws GenericException;
    boolean deleteDegreeObtained(String idServer, String accountId) throws GenericException;
    boolean deleteCertification(String idServer, String accountId) throws GenericException;
    List<ActivityArea> findActivityAreas();
    List<SecondaryEducation> findSecondaryEducations(String accountId, Pageable pageable) throws GenericException;
    SecondaryEducation saveSecondaryEducation(SecondaryEducation secondaryEducation) throws GenericException;
    boolean deleteSecondaryEducation(String idServer, String accountId) throws GenericException;

    List<HigherEducation> findHigherEducations(String accountId, Pageable pageable) throws GenericException;
    HigherEducation saveHigherEducation(HigherEducation higherEducation) throws GenericException;
    boolean deleteHigherEducation(String idServer, String accountId) throws GenericException;

    List<Education> findAllEducations();
    List<EstablishmentType> findAllEstablishmentTypes();
    List<EstablishmentCertificationType> findAllEstablishmentCertificationTypes();
    List<Diploma> findAllDiplomas();
    List<StudyLevel> findAllStudyLevels();
    List<TeachingArea> findAllTeachingAreas();

    long countAcademics(String accountId) throws GenericException;
}
