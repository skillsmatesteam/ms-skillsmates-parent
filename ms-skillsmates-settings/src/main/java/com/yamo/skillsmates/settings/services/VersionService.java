package com.yamo.skillsmates.settings.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.version.Version;

import java.util.List;

public interface VersionService {
    List<Version> findVersions() throws GenericException;
    Boolean dismissVersion() throws GenericException;
}
