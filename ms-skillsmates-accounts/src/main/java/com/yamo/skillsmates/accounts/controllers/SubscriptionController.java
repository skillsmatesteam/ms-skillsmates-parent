package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.SubscriptionApi;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountNetworkDto;
import com.yamo.skillsmates.mapper.account.AccountMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.AccountSocialInteractions;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@CrossOrigin
@RestController
public class SubscriptionController extends AbstractController implements SubscriptionApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionController.class);
    private static final String defaultValueSizePage = "32";
    private static final String defaultValuePage = "0";

    @Override
    public ResponseEntity<GenericResultDto> findNetwork(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId,
                                                        @RequestParam(name = PAGE, defaultValue = defaultValuePage) int page,
                                                        @RequestParam(name = LIMIT, defaultValue = defaultValueSizePage) int limit) throws GenericException {
        LOGGER.info("{}", LoggingUtil.START);
        try {
            processToken(token, id);
            result.setResultObject(accountService.findNetwork(accountId));
            genericResultDtoSuccess(id);
            LOGGER.info("{}", LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        } catch (Exception e) {
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findNetworkOffline(@PathVariable String accountId,
                                                        @RequestParam(name = PAGE, defaultValue = defaultValuePage) int page,
                                                        @RequestParam(name = LIMIT, defaultValue = defaultValueSizePage) int limit) throws GenericException {
        LOGGER.info("{}", LoggingUtil.START);

        try {

            Account account = libAccountService.findAccountByIdServer(accountId);
            AccountNetworkDto accountNetworkDto = new AccountNetworkDto();

            /*  Followers  */
            accountNetworkDto.setFollowers(AccountMapper.getInstance().asAccountSocialInteractionsDtos(libAccountService.findFollowers(accountId, page, limit)));
            accountNetworkDto.setNumberFollowers(libAccountService.countFollowers(account));
            accountNetworkDto.setPageFollowers(new int[libAccountService.findPageFollowers(accountId, page, limit).getTotalPages()]);

            /*  Followees  */
            accountNetworkDto.setFollowees(AccountMapper.getInstance().asAccountSocialInteractionsDtos(libAccountService.findFollowees(accountId, page, limit)));
            accountNetworkDto.setNumberFollowees(libAccountService.countFollowees(account));
            accountNetworkDto.setPageFollowees(new int[libAccountService.findPageFollowees(accountId, page, limit).getTotalPages()]);

            /*  Suggestions  */
            List<AccountSocialInteractions> accountSuggestions = libAccountService.findSuggestions(accountId);
            accountNetworkDto.setNumberSuggestions((long) accountSuggestions.size());
            accountNetworkDto.setPageSuggestions(new int[getTotalPages(accountSuggestions, limit)]);
            accountNetworkDto.setSuggestions(getPage(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSuggestions), page, limit));

            /*  Around You  */
            List<AccountSocialInteractions> accountAroundYou = libAccountService.findAroundYou(accountId);
            accountNetworkDto.setAroundYou(getPage(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountAroundYou), page, limit));
            accountNetworkDto.setNumberAroundYou((long) accountAroundYou.size());
            accountNetworkDto.setPageAroundYou(new int[getTotalPages(accountAroundYou, limit)]);

            /*  Online  */
            List<AccountSocialInteractions> accountsOnline = libAccountService.findAccountsOnline(accountId);
            accountNetworkDto.setAccountsOnline(getPage(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountsOnline), page, limit));
            accountNetworkDto.setNumberAccountsOnline((long) accountsOnline.size());
            accountNetworkDto.setPageAccountsOnline(new int[getTotalPages(accountsOnline, limit)]);

            /*  Favorites  */
            List<AccountSocialInteractions> accountFavorites = accountService.findAccountsByFavorite(accountId);
            accountNetworkDto.setFavorites(AccountMapper.getInstance().asAccountSocialInteractionsDtos(getPage(accountFavorites, page, limit)));
            accountNetworkDto.setNumberFavorites((long) accountFavorites.size());
            accountNetworkDto.setPageFavorites(new int[getTotalPages(accountFavorites, limit)]);

            accountNetworkDto.setCurrentPage(page); // mémorise la page courante
            result.setResultObject(accountNetworkDto);

            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}", LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        } catch (Exception e) {
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findNetworkByType(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId,
                                                        @RequestParam(name = PAGE, defaultValue = defaultValuePage) int page,
                                                        @RequestParam(name = LIMIT, defaultValue = defaultValueSizePage) int limit,
                                                        @RequestParam(name = "networkType", defaultValue = "1") int networkType) throws GenericException {
        LOGGER.info("{}", LoggingUtil.START);

        try {
            processToken(token, id);
            Account account = libAccountService.findAccountByIdServer(accountId);
            AccountNetworkDto accountNetworkDto = new AccountNetworkDto();

            switch (networkType) {
                case 1:
                    findSuggestions(accountNetworkDto, account, id, page, limit);
                    break;
                case 2:
                    findFollowers(accountNetworkDto, account, accountId, page, limit);
                    break;
                case 3:
                    findFollowees(accountNetworkDto, account, accountId, page, limit);
                    break;
                case 4:
                    findAround(accountNetworkDto, account, id, page, limit);
                    break;
                case 5:
                    findFavorites(accountNetworkDto, account, accountId, page, limit);
                    break;
                case 6:
                    findOnline(accountNetworkDto, account, id, page, limit);
                    break;
            }

            accountNetworkDto.setCurrentPage(page); // mémorise la page courante
            result.setResultObject(accountNetworkDto);

            genericResultDtoSuccess(id);
            LOGGER.info("{}", LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        } catch (Exception e) {
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private void findSuggestions(AccountNetworkDto accountNetworkDto, Account account, String id, int page, int limit) throws GenericException {
        Page<AccountSocialInteractions> accountSocialInteractions = libAccountService.findSuggestionsAsAccountSocialInteraction(id, page, limit);
        accountNetworkDto.setNumberSuggestions(accountSocialInteractions.getTotalElements());
        accountNetworkDto.setPageSuggestions(new int[accountSocialInteractions.getTotalPages()]);
        accountNetworkDto.setSuggestions(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSocialInteractions.getContent()));

        accountNetworkDto.setNumberFollowers(libAccountService.countFollowers(account));
        accountNetworkDto.setNumberFollowees(libAccountService.countFollowees(account));
        accountNetworkDto.setNumberAroundYou(libAccountService.countAroundYou(id));
        accountNetworkDto.setNumberFavorites(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name()));
        accountNetworkDto.setNumberAccountsOnline(libAccountService.countAccountsOnline(id));
    }

    private void findFollowers(AccountNetworkDto accountNetworkDto, Account account, String accountId, int page, int limit) throws GenericException {
        Page<AccountSocialInteractions> accountSocialInteractions = libAccountService.findFollowersAsAccountSocialInteraction(accountId, page, limit);
        accountNetworkDto.setNumberFollowers(accountSocialInteractions.getTotalElements());
        accountNetworkDto.setPageFollowers(new int[accountSocialInteractions.getTotalPages()]);
        accountNetworkDto.setFollowers(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSocialInteractions.getContent()));

        accountNetworkDto.setNumberSuggestions(libAccountService.countSuggestion(accountId));
        accountNetworkDto.setNumberFollowees(libAccountService.countFollowees(account));
        accountNetworkDto.setNumberAroundYou(libAccountService.countAroundYou(accountId));
        accountNetworkDto.setNumberFavorites(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name()));
        accountNetworkDto.setNumberAccountsOnline(libAccountService.countAccountsOnline(accountId));
    }

    private void findFollowees(AccountNetworkDto accountNetworkDto, Account account, String accountId, int page, int limit) throws GenericException {
        Page<AccountSocialInteractions> accountSocialInteractions = libAccountService.findFolloweesAsAccountSocialInteraction(accountId, page, limit);
        accountNetworkDto.setNumberFollowees(accountSocialInteractions.getTotalElements());
        accountNetworkDto.setPageFollowees(new int[accountSocialInteractions.getTotalPages()]);
        accountNetworkDto.setFollowees(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSocialInteractions.getContent()));

        accountNetworkDto.setNumberSuggestions(libAccountService.countSuggestion(accountId));
        accountNetworkDto.setNumberFollowers(libAccountService.countFollowers(account));
        accountNetworkDto.setNumberAroundYou(libAccountService.countAroundYou(accountId));
        accountNetworkDto.setNumberFavorites(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name()));
        accountNetworkDto.setNumberAccountsOnline(libAccountService.countAccountsOnline(accountId));
    }

    private void findAround(AccountNetworkDto accountNetworkDto, Account account, String id, int page, int limit) throws GenericException {
        Page<AccountSocialInteractions> accountSocialInteractions = libAccountService.findAroundYouAsAccountSocialInteraction(id, page, limit);
        accountNetworkDto.setNumberAroundYou(accountSocialInteractions.getTotalElements());
        accountNetworkDto.setPageAroundYou(new int[accountSocialInteractions.getTotalPages()]);
        accountNetworkDto.setAroundYou(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSocialInteractions.getContent()));

        accountNetworkDto.setNumberSuggestions(libAccountService.countSuggestion(id));
        accountNetworkDto.setNumberFollowers(libAccountService.countFollowers(account));
        accountNetworkDto.setNumberFollowees(libAccountService.countFollowees(account));
        accountNetworkDto.setNumberFavorites(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name()));
        accountNetworkDto.setNumberAccountsOnline(libAccountService.countAccountsOnline(id));
    }

    private void findFavorites(AccountNetworkDto accountNetworkDto, Account account, String accountId, int page, int limit) throws GenericException {
        Page<AccountSocialInteractions> accountSocialInteractions = accountService.findFavoriteAsAccountSocialInteraction(accountId, page, limit);
        accountNetworkDto.setNumberFavorites(accountSocialInteractions.getTotalElements());
        accountNetworkDto.setPageFavorites(new int[accountSocialInteractions.getTotalPages()]);
        accountNetworkDto.setFavorites(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSocialInteractions.getContent()));

        accountNetworkDto.setNumberSuggestions(libAccountService.countSuggestion(accountId));
        accountNetworkDto.setNumberFollowers(libAccountService.countFollowers(account));
        accountNetworkDto.setNumberFollowees(libAccountService.countFollowees(account));
        accountNetworkDto.setNumberAroundYou(libAccountService.countAroundYou(accountId));
        accountNetworkDto.setNumberAccountsOnline(libAccountService.countAccountsOnline(accountId));
    }

    private void findOnline(AccountNetworkDto accountNetworkDto, Account account, String id, int page, int limit) throws GenericException {
        Page<AccountSocialInteractions> accountSocialInteractions = libAccountService.findAccountsOnlineAsAccountSocialInteraction(id, page, limit);
        accountNetworkDto.setNumberAccountsOnline(accountSocialInteractions.getTotalElements());
        accountNetworkDto.setPageAccountsOnline(new int[accountSocialInteractions.getTotalPages()]);
        accountNetworkDto.setAccountsOnline(AccountMapper.getInstance().asAccountSocialInteractionsDtos(accountSocialInteractions.getContent()));

        accountNetworkDto.setNumberSuggestions(libAccountService.countSuggestion(id));
        accountNetworkDto.setNumberFollowers(libAccountService.countFollowers(account));
        accountNetworkDto.setNumberFollowees(libAccountService.countFollowees(account));
        accountNetworkDto.setNumberAroundYou(libAccountService.countAroundYou(id));
        accountNetworkDto.setNumberFavorites(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name()));
    }

    /**
     * returns a view (not a new list) of the sourceList for the
     * range based on page and pageSize
     *
     * @param sourceList
     * @param page,      page number should start from 0
     * @param pageSize
     * @return custom error can be given instead of returning emptyList
     */
    public static <T> List<T> getPage(List<T> sourceList, int page, int pageSize) {
        if (pageSize <= 0 || page <= -1) {
            throw new IllegalArgumentException("invalid page size: " + pageSize);
        }
        //int fromIndex = (page - 1) * pageSize;
        int fromIndex = page * pageSize;
        if (sourceList == null || sourceList.size() <= fromIndex) {
            return Collections.emptyList();
        }
        // toIndex exclusive
        List<T> list;
        list = sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
        Collections.shuffle(list);
        return list;
    }

    public static int getTotalPages(List<AccountSocialInteractions> sourceList, int pageSize) {
        if (pageSize <= 0) {
            throw new IllegalArgumentException("invalid page size: " + pageSize);
        }
        int totalPages;
        int resultat = sourceList.size() / pageSize;
        int reste = sourceList.size() % pageSize;

        if (reste == 0)
            totalPages = resultat;
        else if (resultat <= 0) totalPages = 1;
        else totalPages = resultat + 1;

        return totalPages;
    }
}
