package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.AcademicApi;
import com.yamo.skillsmates.accounts.services.AcademicService;
import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.*;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class AcademicController extends AbstractController implements AcademicApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(AcademicController.class);

    protected static final String DEGREE_OBTAINED_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Degree-Obtained-Id";
    protected static final String SECONDARY_EDUCATION_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Secondary-Education-Id";
    protected static final String HIGHER_EDUCATION_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Higher-Education-Id";
    protected static final String CERTIFICATION_OBTAINED_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Certification-Id";

    @Autowired
    private AcademicService academicService;

    @Override
    public ResponseEntity<GenericResultDto> createDegreeObtained(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody DegreeObtainedDto degreeObtainedDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateDegreeObtained(degreeObtainedDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            degreeObtainedDto.setEstablishmentName(StringHelper.capitalizeFirstCharacter(degreeObtainedDto.getEstablishmentName()));
            degreeObtainedDto.setCity(StringHelper.capitalizeFirstCharacter(degreeObtainedDto.getCity()));
            academicService.saveDegreeObtained(GenericMapper.INSTANCE.asEntity(degreeObtainedDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> createCertification(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody CertificationDto certificationDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateCertification(certificationDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            certificationDto.setEntitledCertification(StringHelper.capitalizeFirstCharacter(certificationDto.getEntitledCertification()));
            certificationDto.setIssuingInstitutionName(StringHelper.capitalizeFirstCharacter(certificationDto.getIssuingInstitutionName()));

            academicService.saveCertification(GenericMapper.INSTANCE.asEntity(certificationDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findAcademicsById(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 100, Sort.by(Sort.Order.desc("id")));
            AccountAcademicsDto accountAcademicsDto = new AccountAcademicsDto();
            accountAcademicsDto.setAccount(GenericMapper.INSTANCE.asDto(accountService.findAccountByIdServer(accountId)));
            accountAcademicsDto.setDegreesObtained(DegreeObtainedMapper.getInstance().asDtos(academicService.findDegreesObtained(accountId, pageable)));
            accountAcademicsDto.setEducations(EducationMapper.getInstance().asDtos(academicService.findAllEducations()));
            accountAcademicsDto.setEstablishmentTypes(EstablishmentTypeMapper.getInstance().asDtos(academicService.findAllEstablishmentTypes()));
            accountAcademicsDto.setHigherEducations(HigherEducationMapper.getInstance().asDtos(academicService.findHigherEducations(accountId, pageable)));
            accountAcademicsDto.setDiplomas(DiplomaMapper.getInstance().asDtos(academicService.findAllDiplomas()));
            accountAcademicsDto.setSecondaryEducations(SecondaryEducationMapper.getInstance().asDtos(academicService.findSecondaryEducations(accountId, pageable)));
            accountAcademicsDto.setStudyLevels(StudyLevelMapper.getInstance().asDtos(academicService.findAllStudyLevels()));
            accountAcademicsDto.setTeachingAreas(TeachingAreaMapper.getInstance().asDtos(academicService.findAllTeachingAreas()));
            accountAcademicsDto.setCertificates(CertificationMapper.getInstance().asDtos(academicService.findCertificates(accountId, pageable)));
            accountAcademicsDto.setActivityAreas(ActivityAreaMapper.getInstance().asDtos(academicService.findActivityAreas()));
            accountAcademicsDto.setEstablishmentCertificationTypes(EstablishmentCertificationTypeMapper.getInstance().asDtos(academicService.findAllEstablishmentCertificationTypes()));


            result.setResultObject(accountAcademicsDto);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findAcademicsOfflineById(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 100, Sort.by(Sort.Order.desc("id")));
            AccountAcademicsDto accountAcademicsDto = new AccountAcademicsDto();
            accountAcademicsDto.setAccount(GenericMapper.INSTANCE.asDto(accountService.findAccountByIdServer(accountId)));
            accountAcademicsDto.setDegreesObtained(DegreeObtainedMapper.getInstance().asDtos(academicService.findDegreesObtained(accountId, pageable)));
            accountAcademicsDto.setEducations(EducationMapper.getInstance().asDtos(academicService.findAllEducations()));
            accountAcademicsDto.setEstablishmentTypes(EstablishmentTypeMapper.getInstance().asDtos(academicService.findAllEstablishmentTypes()));
            accountAcademicsDto.setHigherEducations(HigherEducationMapper.getInstance().asDtos(academicService.findHigherEducations(accountId, pageable)));
            accountAcademicsDto.setDiplomas(DiplomaMapper.getInstance().asDtos(academicService.findAllDiplomas()));
            accountAcademicsDto.setSecondaryEducations(SecondaryEducationMapper.getInstance().asDtos(academicService.findSecondaryEducations(accountId, pageable)));
            accountAcademicsDto.setStudyLevels(StudyLevelMapper.getInstance().asDtos(academicService.findAllStudyLevels()));
            accountAcademicsDto.setTeachingAreas(TeachingAreaMapper.getInstance().asDtos(academicService.findAllTeachingAreas()));
            accountAcademicsDto.setCertificates(CertificationMapper.getInstance().asDtos(academicService.findCertificates(accountId, pageable)));
            accountAcademicsDto.setActivityAreas(ActivityAreaMapper.getInstance().asDtos(academicService.findActivityAreas()));
            accountAcademicsDto.setEstablishmentCertificationTypes(EstablishmentCertificationTypeMapper.getInstance().asDtos(academicService.findAllEstablishmentCertificationTypes()));


            result.setResultObject(accountAcademicsDto);
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteDegreeObtained(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestHeader(value = DEGREE_OBTAINED_HEADER) String degreeObtainedId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(degreeObtainedId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            academicService.deleteDegreeObtained(degreeObtainedId, id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteCertification(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestHeader(value = CERTIFICATION_OBTAINED_HEADER) String certificationId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(certificationId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            academicService.deleteCertification(certificationId, id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> createSecondaryEducation(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody SecondaryEducationDto secondaryEducationDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateSecondaryEducation(secondaryEducationDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            secondaryEducationDto.setEstablishmentName(StringHelper.capitalizeFirstCharacter(secondaryEducationDto.getEstablishmentName()));
            secondaryEducationDto.setCity(StringHelper.capitalizeFirstCharacter(secondaryEducationDto.getCity()));
            academicService.saveSecondaryEducation(GenericMapper.INSTANCE.asEntity(secondaryEducationDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteSecondaryEducation(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestHeader(value = SECONDARY_EDUCATION_HEADER) String secondaryEducationId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(secondaryEducationId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            academicService.deleteSecondaryEducation(secondaryEducationId, id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> createHigherEducation(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody HigherEducationDto higherEducationDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateHigherEducation(higherEducationDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            higherEducationDto.setEstablishmentName(StringHelper.capitalizeFirstCharacter(higherEducationDto.getEstablishmentName()));
            higherEducationDto.setCity(StringHelper.capitalizeFirstCharacter(higherEducationDto.getCity()));
            academicService.saveHigherEducation(GenericMapper.INSTANCE.asEntity(higherEducationDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteHigherEducation(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestHeader(value = HIGHER_EDUCATION_HEADER) String higherEducationId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(higherEducationId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            academicService.deleteHigherEducation(higherEducationId, id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validateDegreeObtained(DegreeObtainedDto degreeObtainedDto){
        return degreeObtainedDto != null &&
                degreeObtainedDto.getEndDate() != null &&
                degreeObtainedDto.getStartDate() != null &&
                degreeObtainedDto.getEstablishmentType() != null &&
                degreeObtainedDto.getSpecialty() != null &&
                degreeObtainedDto.getDiploma() != null &&
                validateTeachingArea(degreeObtainedDto) &&
                degreeObtainedDto.getAccount() != null &&
                StringUtils.isNotBlank(degreeObtainedDto.getEstablishmentName()) &&
                StringUtils.isNotBlank(degreeObtainedDto.getCity());
    }

    private boolean validateCertification(CertificationDto certificationDto){
        return certificationDto != null &&
                certificationDto.getObtainedDate() != null &&
                certificationDto.getActivityArea() != null &&
                certificationDto.getEstablishmentCertificationType() != null &&
                certificationDto.getAccount() != null &&
                StringUtils.isNotBlank(certificationDto.getEntitledCertification()) &&
                StringUtils.isNotBlank(certificationDto.getIssuingInstitutionName());
    }

    private boolean validateSecondaryEducation(SecondaryEducationDto secondaryEducationDto){
        return secondaryEducationDto != null &&
                secondaryEducationDto.getStudyLevel() != null &&
                secondaryEducationDto.getEstablishmentType() != null &&
                secondaryEducationDto.getSpecialty() != null &&
                secondaryEducationDto.getPreparedDiploma() != null &&
                secondaryEducationDto.getAccount() != null &&
                StringUtils.isNotBlank(secondaryEducationDto.getEstablishmentName()) &&
                StringUtils.isNotBlank(secondaryEducationDto.getCity());
    }

    private boolean validateHigherEducation(HigherEducationDto higherEducationDto){
        return higherEducationDto != null &&
                higherEducationDto.getStudyLevel() != null &&
                higherEducationDto.getTeachingArea() != null &&
                higherEducationDto.getEstablishmentType() != null &&
                higherEducationDto.getSpecialty() != null &&
                higherEducationDto.getAccount() != null &&
                higherEducationDto.getTargetedDiploma() != null &&
                StringUtils.isNotBlank(higherEducationDto.getEstablishmentName()) &&
                StringUtils.isNotBlank(higherEducationDto.getCity());
    }

    private boolean validateTeachingArea(DegreeObtainedDto degreeObtainedDto){
        if (degreeObtainedDto.getDiploma().getEducation().getCode().equalsIgnoreCase("higher")){
            return degreeObtainedDto.getTeachingArea() != null;
        }
        return true;
    }
}
