package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.services.ProfessionalService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.account.AccountFilterDto;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Professional;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.account.config.ActivitySector;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.account.ActivityAreaRepository;
import com.yamo.skillsmates.repositories.account.ActivitySectorRepository;
import com.yamo.skillsmates.repositories.account.ProfessionalRepository;
import com.yamo.skillsmates.services.LibAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProfessionalServiceImpl implements ProfessionalService {

    private final ActivitySectorRepository activitySectorRepository;
    private final ActivityAreaRepository activityAreaRepository;
    private final AccountRepository accountRepository;
    private final ProfessionalRepository professionalRepository;
    private final LibAccountService libAccountService;

    @Autowired
    public ProfessionalServiceImpl(ActivitySectorRepository activitySectorRepository, ActivityAreaRepository activityAreaRepository, AccountRepository accountRepository, ProfessionalRepository professionalRepository, LibAccountService libAccountService) {
        this.activitySectorRepository = activitySectorRepository;
        this.activityAreaRepository = activityAreaRepository;
        this.accountRepository = accountRepository;
        this.professionalRepository = professionalRepository;
        this.libAccountService = libAccountService;
    }

    @Override
    public Professional saveProfessional(Professional professional) throws GenericException {
        Optional<ActivityArea> activityArea = activityAreaRepository.findByIdServer(professional.getActivityArea().getIdServer());
        if (!activityArea.isPresent()){
            throw new GenericException("Activity Area not found");
        }
        professional.setActivityArea(activityArea.get());

        Optional<ActivitySector> activitySector = activitySectorRepository.findByIdServer(professional.getActivitySector().getIdServer());
        if (!activitySector.isPresent()){
            throw new GenericException("Activity Sector not found");
        }
        professional.setActivitySector(activitySector.get());

        Optional<Account> account = accountRepository.findByIdServer(professional.getAccount().getIdServer());
        if (!account.isPresent()){
            throw new GenericException("Account not found");
        }
        professional.setAccount(account.get());
        professional.setIdServer(String.valueOf(System.nanoTime()));
        professional = professionalRepository.save(professional);
        libAccountService.updateProfessionalTitleToAccount(professional.getAccount());
        return professional;
    }

    @Override
    public List<Professional> findProfessionals(String accountId, Pageable pageable) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return professionalRepository.findAllByDeletedFalseAndAccount(optionalAccount.get(), pageable).orElseGet(ArrayList::new);
    }

    @Override
    public boolean deleteProfessional(String idServer, String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        Optional<Professional> professionalOptional = professionalRepository.findByIdServerAndAccount(idServer, optionalAccount.get());
        if (!professionalOptional.isPresent()){
            throw new GenericException("Professional does not exists");
        }
        professionalOptional.get().setDeleted(true);
        professionalRepository.save(professionalOptional.get());
        libAccountService.updateProfessionalTitleToAccount(professionalOptional.get().getAccount());
        return true;
    }

    @Override
    public List<ActivityArea> findActivityAreas() {
        return activityAreaRepository.findAll();
    }

    @Override
    public List<ActivitySector> findActivitySectors() {
        return activitySectorRepository.findAll();
    }

    @Override
    public long countProfessionals(String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return professionalRepository.countAllByActiveTrueAndDeletedFalseAndAccount(optionalAccount.get());
    }

    @Override
    public List<AccountDto> filterAccounts(AccountFilterDto accountFilterDto) {
        List<AccountDto> listAccountDto = new ArrayList<>();
        accountFilterDto.getAccountsToFilter().forEach(accountDto -> {
            Optional<List<Professional>> listProfessional = professionalRepository.findByAccount_IdServer(accountDto.getIdServer());

            if(listProfessional.isPresent())
                listProfessional.get().forEach(professional -> {
                    if(professional.getActivityArea().getLabel().toLowerCase().equals(accountFilterDto.getSearchContent().toLowerCase()))
                        if (!listAccountDto.contains(accountDto))
                              listAccountDto.add(accountDto);
                });
        });

        return listAccountDto;
    }
}
