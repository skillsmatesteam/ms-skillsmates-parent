package com.yamo.skillsmates.admin.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.admin.Partner;

import java.util.List;

public interface PartnerService {
    List<Partner> findAllPartners();
    Partner savePartner(Partner partner) throws GenericException;
}
