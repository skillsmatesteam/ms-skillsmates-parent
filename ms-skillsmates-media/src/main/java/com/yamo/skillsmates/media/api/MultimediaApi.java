package com.yamo.skillsmates.media.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-media/multimedia", produces = MediaType.APPLICATION_JSON_VALUE)
public interface MultimediaApi {
    @GetMapping
    ResponseEntity<GenericResultDto> getMultimedias(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/{multimediaId}")
    ResponseEntity<GenericResultDto> getMultimediaById(String token, String id, String accountId, String multimediaId) throws GenericException;

    @GetMapping(value = "/type/{typeMultimedia}")
    ResponseEntity<GenericResultDto> getMultimediaByType(String token, String id, String accountId, String typeMultimedia) throws GenericException;

    @GetMapping(value = "/download/{multimediaId}")
    ResponseEntity<GenericResultDto> downloadMultimedia(String token, String id, String accountId, String multimediaId) throws GenericException;

    @GetMapping(value = "/mediatype/{accountId}")
    ResponseEntity<GenericResultDto> getMultimediaInfos(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/mediatype/offline/{accountId}")
    ResponseEntity<GenericResultDto> getMultimediaInfosOffline(String accountId) throws GenericException;

    @GetMapping(value = "/countmedia/{accountId}")
    ResponseEntity<GenericResultDto> getCountMultimediaInfos(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/mediasubtype")
    ResponseEntity<GenericResultDto> getMediaSubtype(String token, String id) throws GenericException;
}
