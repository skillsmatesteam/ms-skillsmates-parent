package com.yamo.skillsmates.notifications.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.notifications.NotificationDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-notifications/notifications", produces = MediaType.APPLICATION_JSON_VALUE)
public interface NotificationApi {
    @GetMapping
    ResponseEntity<GenericResultDto> getNotifications(String token, String id) throws GenericException;

    @PutMapping
    ResponseEntity<GenericResultDto> deactivateNotification(String token, String id, NotificationDto notification) throws GenericException;
}
