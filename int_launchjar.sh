#!/bin/bash
#
# Cyrille MOFFO
#
# Script d'execution des jars skillsmates
#

# kill processes (ports starting with 9 are for prod)
sudo kill -9 $(sudo lsof -t -i:8001)
sudo kill -9 $(sudo lsof -t -i:8002)
sudo kill -9 $(sudo lsof -t -i:8003)
sudo kill -9 $(sudo lsof -t -i:8004)
sudo kill -9 $(sudo lsof -t -i:8005)
sudo kill -9 $(sudo lsof -t -i:8006)
sudo kill -9 $(sudo lsof -t -i:8007)
sudo kill -9 $(sudo lsof -t -i:8008)

# launch processes
nohup java -jar ms-skillsmates-accounts.jar > ms-skillsmates-accounts.log 2>&1 &
nohup java -jar ms-skillsmates-communications.jar > ms-skillsmates-communications.log 2>&1 &
nohup java -jar ms-skillsmates-media.jar > ms-skillsmates-media.log 2>&1 &
nohup java -jar ms-skillsmates-notifications.jar > ms-skillsmates-notifications.log 2>&1 &
nohup java -jar ms-skillsmates-pages.jar > ms-skillsmates-pages.log 2>&1 &
nohup java -jar ms-skillsmates-settings.jar > ms-skillsmates-settings.log 2>&1 &