package com.yamo.skillsmates.messages.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping(value = "/skillsmates-messages/messagesfile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
public interface MessageFileApi {

    @PostMapping
    ResponseEntity<GenericResultDto> createMessage(String token, String id,String idServerReceiver, MultipartFile file) throws GenericException;
}
