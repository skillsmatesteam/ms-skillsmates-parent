package com.yamo.skillsmates.admin.services;

import com.yamo.skillsmates.models.admin.config.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAllCategories();
}
