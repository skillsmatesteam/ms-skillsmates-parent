package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.enums.TemplateKeysEnum;
import com.yamo.skillsmates.accounts.http.EmailService;
import com.yamo.skillsmates.accounts.http.dto.EmailDto;
import com.yamo.skillsmates.accounts.http.dto.KeyValueDto;
import com.yamo.skillsmates.accounts.http.dto.MessageDto;
import com.yamo.skillsmates.accounts.services.AccountService;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.dto.account.AccountNetworkDto;
import com.yamo.skillsmates.dto.account.AccountSocialInteractionsDto;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.mail.exception.SkillsmatesMailException;
import com.yamo.skillsmates.mail.service.MailService;
import com.yamo.skillsmates.mapper.account.AccountMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.AccountSocialInteractions;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.models.post.SocialInteractionMap;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.account.ActivityAreaRepository;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionMapRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionTypeRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.CryptUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.internet.AddressException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final MailService mailService;
    private final CryptUtil cryptUtil;
    private final LibAccountService libAccountService;
    private final ActivityAreaRepository activityAreaRepository;
    private final LibSocialInteractionService libSocialInteractionService;
    private final SocialInteractionRepository socialInteractionRepository;
    private final PostRepository postRepository;
    private final EmailService emailService;
    private final SocialInteractionTypeRepository socialInteractionTypeRepository;
    private final SocialInteractionMapRepository socialInteractionMapRepository;

    @Value("${ms.skillsmates.accounts.url}")
    private String accountUrl;
    @Value("${ms.skillsmates.accounts.link}")
    private String accountLink;

    @Value("${ms.skillsmates.accounts.active.link}")
    private String accountActiveLink;

    @Value("${ms.skillsmates.activate.link}")
    private String accountActivateLink;

    @Value("${ms.skillsmates.hostname}")
    private String hostname;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, MailService mailService, CryptUtil cryptUtil, LibAccountService libAccountService, ActivityAreaRepository activityAreaRepository, LibSocialInteractionService libSocialInteractionService, SocialInteractionRepository socialInteractionRepository, PostRepository postRepository, EmailService emailService, SocialInteractionTypeRepository socialInteractionTypeRepository, SocialInteractionMapRepository socialInteractionMapRepository) {
        this.accountRepository = accountRepository;
        this.mailService = mailService;
        this.cryptUtil = cryptUtil;
        this.libAccountService = libAccountService;
        this.activityAreaRepository = activityAreaRepository;
        this.libSocialInteractionService = libSocialInteractionService;
        this.socialInteractionRepository = socialInteractionRepository;
        this.postRepository = postRepository;
        this.emailService = emailService;
        this.socialInteractionTypeRepository = socialInteractionTypeRepository;
        this.socialInteractionMapRepository = socialInteractionMapRepository;
    }

    @Override
    public Account saveAccount(Account account) throws GenericException {
        try {
            if (accountRepository.findByDeletedFalseAndEmail(account.getEmail()).isPresent()) {
                throw new GenericException(ResponseMessage.ACCOUNT_ALREADY_EXISTS.getMessage());
            }
            account.setPassword(StringHelper.generateSecurePassword(account.getPassword(), account.getEmail()));
            account.setIdServer(String.valueOf(System.nanoTime()));
            account.setActive(false); //to be active after email confirmation

            account = accountRepository.save(account);
            try {
                sendConfirmationEmail(account);
            } catch (SkillsmatesMailException | AddressException | InvalidKeyException | NoSuchPaddingException |
                     BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
                accountRepository.delete(account);
                throw new GenericException(e.getMessage());
            }
            return account;
        } catch (NoSuchAlgorithmException e) {
            throw new GenericException(ResponseMessage.ERROR_SAVING_ACCOUNT.getMessage());
        }
    }

    @Override
    public Account updateAccount(Account account) throws GenericException {
        return libAccountService.updateAccount(account);
    }

    @Override
    public List<Account> findAllAccounts() {
        return accountRepository.findAllByActiveTrueAndDeletedFalse();
    }

    @Override
    public List<Account> findAllAccounts(String idServer, int page, int size) {
        return accountRepository.findByActiveTrueAndDeletedFalseAndIdServerNot(idServer, PageRequest.of(page, size)).orElse(new ArrayList<>());
    }

    @Override
    public Page<Account> findAllAccountsByPage(int page, int size) throws GenericException {
        return null;
    }

    @Override
    public Account findAccountByIdServer(String idServer) {
        Optional<Account> account = accountRepository.findByIdServer(idServer);
        return account.orElse(null);
    }

    @Override
    public Account findAccountByEmail(String email) {
        return accountRepository.findByActiveTrueAndDeletedFalseAndEmail(email).orElse(null);
    }

    @Override
    public boolean deleteAccount(String idServer) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(idServer);
        if (!account.isPresent()) {
            throw new GenericException(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage());
        }
        account.get().setDeleted(true);
        accountRepository.save(account.get());
        return true;
    }

    @Override
    public boolean deactivateAccount(String idServer) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(idServer);
        if (!account.isPresent()) {
            throw new GenericException(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage());
        }
        account.get().setActive(false);
        accountRepository.save(account.get());
        return true;
    }

    @Override
    public Account activateAccount(String idServer) throws GenericException {
        try {
            String accountId = cryptUtil.decrypt(idServer);
            Optional<Account> accountOptional = accountRepository.findByIdServer(accountId);
            if (!accountOptional.isPresent()) {
                throw new GenericException(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage());
            }

            if (accountOptional.get().isActive()) {
                throw new GenericException(ResponseMessage.ACCOUNT_ALREADY_ACTIVE.getMessage());
            }

            Account account = accountOptional.get();
            account.setActive(true);
            accountRepository.save(account);

            //make this account to follow skills-mates account
            makeThisAccountTofollowSkillsmate(account);

            sendValidationEmail(account);
            return account;
        } catch (SkillsmatesMailException | AddressException | NoSuchPaddingException | NoSuchAlgorithmException |
                 InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            throw new GenericException(e.getMessage());
        }
    }

    @Override
    public List<ActivityArea> findAllActivitiesAreas() {
        return activityAreaRepository.findAll();
    }

    @Override
    public boolean setConnectedStatus(String accountId, boolean connected) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()) {
            throw new GenericException(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage());
        }
        account.get().setConnected(connected);
        accountRepository.save(account.get());
        return true;
    }

    @Override
    public List<Account> findAllConnectedAccounts() {
        return accountRepository.findByActiveTrueAndDeletedFalseAndConnectedTrue().orElse(new ArrayList<>());
    }

    @Override
    public List<Account> findAllConnectedAccounts(String idServer, int page, int size) {
        return accountRepository.findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(idServer, PageRequest.of(page, size)).getContent();
    }

    @Override
    public List<AccountSocialInteractions> findAccountsByFavorite(String accountId) throws GenericException {
        List<Account> favorites = libSocialInteractionService
                .findSocialInteractionsByReceiverAndType(libAccountService.findAccountByIdServer(accountId), SocialInteractionTypeEnum.FAVORITE.name())
                .parallelStream()
                .map(SocialInteraction::getEmitterAccount)
                .collect(Collectors.toList());
        return libAccountService.convertToAccountSocialInteractions(favorites);
    }

    @Override
    public Page<AccountSocialInteractions> findFavoriteAsAccountSocialInteraction(String accountId, int page, int size) throws GenericException {
        Account account = libAccountService.findAccountByIdServer(accountId);
        return libSocialInteractionService.findSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.FAVORITE.name(), page, size)
                .map(SocialInteraction::getEmitterAccount).map(e -> libAccountService.convertToAccountSocialInteractions(e));
    }

    @Override
    public AccountSocialInteractions findAccountSocialInteractions(String idServer) throws GenericException {
        AccountSocialInteractions accountSocialInteractions = new AccountSocialInteractions();
        Account account = libAccountService.findAccountByIdServer(idServer);
        accountSocialInteractions.setAccount(account);
        accountSocialInteractions.setSocialInteractions(socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(account.getIdServer()));
        accountSocialInteractions.setNumberPosts(postRepository.countByActiveTrueAndDeletedFalseAndAccount(account));
        return accountSocialInteractions;
    }

    @Override
    public List<AccountSocialInteractions> searchAccounts(SearchParam searchParam) throws GenericException {
        return null;
    }

    @Override
    public AccountNetworkDto findNetwork(String accountId) {
        List<Account> accounts = new ArrayList<>();
        AccountNetworkDto accountNetworkDto = new AccountNetworkDto();

        List<Account> followers = accountRepository.findFollowers(accountId);
        this.mergeWithoutDuplicate(accounts, followers);
        accountNetworkDto.setNumberFollowers((long) followers.size());

        List<Account> followees = accountRepository.findFollowees(accountId);
        this.mergeWithoutDuplicate(accounts, followees);
        accountNetworkDto.setNumberFollowees((long) followees.size());

        List<String> knownAccountIds = Stream.of(followers, followees)
                .flatMap(Collection::parallelStream)
                .map(Account::getIdServer)
                .collect(Collectors.toList());
        List<Account> suggestions = accountRepository.findByActiveTrueAndDeletedFalseAndIdServerNotIn(knownAccountIds);
        this.mergeWithoutDuplicate(accounts, suggestions);
        accountNetworkDto.setNumberSuggestions((long) suggestions.size());

        List<Account> online = accountRepository.findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(accountId);
        this.mergeWithoutDuplicate(accounts, online);
        accountNetworkDto.setNumberAccountsOnline((long) online.size());

        List<Account> favorites = accountRepository.findFavorites(accountId);
        this.mergeWithoutDuplicate(accounts, favorites);
        accountNetworkDto.setNumberFavorites((long) favorites.size());

        List<Account> around = accountRepository.findAllByActiveTrueAndDeletedFalse();
        this.mergeWithoutDuplicate(accounts, around);
        accountNetworkDto.setNumberAroundYou((long) around.size());

        Map<String, AccountSocialInteractions> map = new HashMap<>();
        accounts.stream().forEach(e -> map.put(e.getIdServer(), convertToAccountSocialInteractions(e)));

        accountNetworkDto.setFollowers(getCorrespondingData(map, followers));
        accountNetworkDto.setFollowees(getCorrespondingData(map, followees));
        accountNetworkDto.setSuggestions(getCorrespondingData(map, suggestions));
        accountNetworkDto.setAccountsOnline(getCorrespondingData(map, online));
        accountNetworkDto.setFavorites(getCorrespondingData(map, favorites));
        accountNetworkDto.setAroundYou(getCorrespondingData(map, around));
        return accountNetworkDto;
    }

//    private String buildSearchName(String search) {
//        return "LOWER(firstname) LIKE LOWER(%" + search + "%) OR LOWER(lastname) LIKE LOWER(%" + search + "%)";
//    }
//
//    private String buildSearchPlace(String search) {
//        return "LOWER(country) LIKE LOWER(%" + search + "%) OR LOWER(city) LIKE LOWER(%" + search + "%)";
//    }

//    public void searchAccount(String accountId, int accountType, int page, int limit) {
//        int status = 0;
//        String name = "";
//        String place = "";
//
//        List<Integer> statusParams = status > 2 ? Arrays.asList(0, 1, 2) : Arrays.asList(status);
//        String searchParam =
//                Stream.of(name.split(" "))
//                        .map(String::trim).filter(e -> !e.isEmpty())
//                        .map(e -> buildSearchName(e)).collect(Collectors.joining(" OR "));
//        searchParam = searchParam.length() > 5 ? "( " + searchParam + " ) AND " : "";
//
//        String placeParam =
//                Stream.of(name.split(" "))
//                        .map(String::trim).filter(e -> !e.isEmpty())
//                        .map(e -> buildSearchPlace(e)).collect(Collectors.joining(" OR "));
//        placeParam = placeParam.length() > 5 ? "( " + placeParam + " ) AND " : "";
//
//        Pageable pageable = PageRequest.of(page, limit);
//
//
//        switch (accountType) {
//            case 1:
//                searchSuggestions(accountNetworkDto, account, id, page, limit);
//                break;
//            case 2:
//                searchFollowers(accountNetworkDto, account, accountId, page, limit);
//                break;
//            case 3:
//                accountRepository.searchFollowees(accountId, statusParams, searchParam, placeParam, pageable);
//                break;
//            case 4:
//                searchAround(accountNetworkDto, account, id, page, limit);
//                break;
//            case 5:
//                searchFavorites(accountNetworkDto, account, accountId, page, limit);
//                break;
//            case 6:
//                searchOnline(accountNetworkDto, account, id, page, limit);
//                break;
//        }
//    }

    private List<AccountSocialInteractionsDto> getCorrespondingData(Map<String, AccountSocialInteractions> map, List<Account> accounts) {
        return AccountMapper.getInstance().asAccountSocialInteractionsDtos(map.keySet().parallelStream().
                filter(e -> accounts.parallelStream().anyMatch(i -> i.getIdServer().equals(e)))
                .map(e -> map.get(e)).collect(Collectors.toList()));
    }

    private void mergeWithoutDuplicate(List<Account> finalList, List<Account> data) {
        data.stream().
                filter(e -> !finalList.stream().anyMatch(i -> i.getId().equals(e.getId())))
                .forEach(e -> finalList.add(e));
    }

    private AccountSocialInteractions convertToAccountSocialInteractions(Account account) {
        AccountSocialInteractions accountSocialInteractions = new AccountSocialInteractions();
        accountSocialInteractions.setId(account.getId());
        accountSocialInteractions.setIdServer(account.getIdServer());
        accountSocialInteractions.setAccount(account);
        accountSocialInteractions.setSocialInteractions(socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(account.getIdServer()));
        return accountSocialInteractions;
    }


    private void sendConfirmationEmail(Account account) throws SkillsmatesMailException, AddressException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, GenericException {
        String cryptedId = cryptUtil.secureCrypt(account.getIdServer());

        List<KeyValueDto> values = new ArrayList<>();
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_USERNAME.name(), account.getFirstname() + " " + account.getLastname()));
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(), accountUrl + accountActiveLink + accountActivateLink + cryptedId));

        values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_TITLE.name(), "Création de compte"));
        values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_MESSAGE.name(), "Bienvenue " + account.getFirstname()));
        values.add(new KeyValueDto(TemplateKeysEnum.CTA_END_TEXT.name(), "pour afficher le message"));

        sendEmail(account, values, "Création de compte", "account.ftl");
    }

    private void sendValidationEmail(Account account) throws SkillsmatesMailException, AddressException {

        List<KeyValueDto> values = new ArrayList<>();
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_USERNAME.name(), account.getFirstname() + " " + account.getLastname()));
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(), hostname));
        values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_TITLE.name(), "Activation compte"));
        values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_MESSAGE.name(), "Bienvenue " + account.getFirstname()));
        values.add(new KeyValueDto(TemplateKeysEnum.CTA_END_TEXT.name(), "pour afficher le message"));

        sendEmail(account, values, "Activation compte", "account.ftl");
    }

    private void sendEmail(Account account, List<KeyValueDto> values, String subject, String template) {
        EmailDto emailDto = new EmailDto();

        emailDto.setTemplate(template);

        MessageDto messageDto = new MessageDto();
        messageDto.setFrom(account.getEmail());
        messageDto.setTo(Collections.singletonList(account.getEmail()));
        messageDto.setBody(subject);
        messageDto.setSubject(subject);
        emailDto.setMessage(messageDto);
        emailDto.setValues(values);
        emailDto.setAttachments(new ArrayList<>());
        emailService.sendEmail(emailDto);
    }

    private void makeThisAccountTofollowSkillsmate(Account account) {
        SocialInteraction socialInteraction = new SocialInteraction();
        String receiverIdServer = "4349844837027990";
        socialInteraction.setEmitterAccount(account);
        socialInteraction.setReceiverAccount(accountRepository.findByIdServer(receiverIdServer).get());
        socialInteraction.setSocialInteractionType(socialInteractionTypeRepository.findByLabel(SocialInteractionTypeEnum.FOLLOWER.name()).get());
        socialInteraction.setIdServer(String.valueOf(System.nanoTime()));

        SocialInteractionMap socialInteractionMap = new SocialInteractionMap();
        socialInteractionMap.setElement(receiverIdServer);
        socialInteractionMap.setIdServer(String.valueOf(System.nanoTime()));
        socialInteractionMap.setSocialInteraction(socialInteraction);
        socialInteractionMap = socialInteractionMapRepository.save(socialInteractionMap);

        socialInteraction.setSocialInteractionMap(socialInteractionMap);
        socialInteractionRepository.save(socialInteraction);
    }
}
