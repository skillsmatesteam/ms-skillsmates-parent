package com.yamo.skillsmates.settings.controllers;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.mapper.version.VersionMapper;
import com.yamo.skillsmates.settings.api.VersionApi;
import com.yamo.skillsmates.settings.services.VersionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class VersionController extends AbstractController implements VersionApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(VersionController.class);

    @Autowired
    private VersionService versionService;

    @Override
    public ResponseEntity<GenericResultDto> getVersions(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            result.setResultObjects(VersionMapper.getInstance().asDtos(versionService.findVersions()));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> dismissVersion(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            versionService.dismissVersion();
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
