package com.yamo.skillsmates.pages.services.impl;

import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.mail.service.LibMessageService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Subscription;
import com.yamo.skillsmates.models.post.*;
import com.yamo.skillsmates.notifications.services.LibNotificationService;
import com.yamo.skillsmates.pages.enums.TemplateKeysEnum;
import com.yamo.skillsmates.pages.enums.TypeEnum;
import com.yamo.skillsmates.pages.http.EmailService;
import com.yamo.skillsmates.pages.http.dto.EmailDto;
import com.yamo.skillsmates.pages.http.dto.KeyValueDto;
import com.yamo.skillsmates.pages.http.dto.MessageDto;
import com.yamo.skillsmates.pages.services.SocialInteractionService;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.account.SubscriptionRepository;
import com.yamo.skillsmates.repositories.post.*;
import com.yamo.skillsmates.services.LibAccountService;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SocialInteractionServiceImpl implements SocialInteractionService {

    private final AccountRepository accountRepository;
    private final LibAccountService libAccountService;
    private final SocialInteractionTypeRepository socialInteractionTypeRepository;
    private final SocialInteractionRepository socialInteractionRepository;
    private final SocialInteractionMapRepository socialInteractionMapRepository;
    private final LibNotificationService libNotificationService;
    private final LibMessageService libMessageService;
    private final SubscriptionRepository subscriptionRepository;
    private final EmailService emailService;
    private final PostRepository postRepository;
    private final PublicationRepository publicationRepository;

    @Value("${ms.skillsmates.hostname}")
    private String hostname;

    private static final String RESOURCE_URL = "https://skillsmatesmedia.s3.us-east-2.amazonaws.com/multimedia/";

    @Autowired
    public SocialInteractionServiceImpl(AccountRepository accountRepository, LibAccountService libAccountService,
                                        SocialInteractionTypeRepository socialInteractionTypeRepository,
                                        SocialInteractionRepository socialInteractionRepository,
                                        SocialInteractionMapRepository socialInteractionMapRepository,
                                        LibNotificationService libNotificationService, LibMessageService libMessageService,
                                        SubscriptionRepository subscriptionRepository, EmailService emailService,
                                        PostRepository postRepository, PublicationRepository publicationRepository) {
        this.accountRepository = accountRepository;
        this.libAccountService = libAccountService;
        this.socialInteractionTypeRepository = socialInteractionTypeRepository;
        this.socialInteractionRepository = socialInteractionRepository;
        this.socialInteractionMapRepository = socialInteractionMapRepository;
        this.libNotificationService = libNotificationService;
        this.libMessageService = libMessageService;
        this.subscriptionRepository = subscriptionRepository;
        this.emailService = emailService;
        this.postRepository = postRepository;
        this.publicationRepository = publicationRepository;
    }

    @Override
    public SocialInteraction saveSocialInteration(SocialInteraction socialInteraction, String type, String element) throws GenericException {
        try{
            Account emitterAccount = libAccountService.findAccountByIdServer(socialInteraction.getEmitterAccount().getIdServer());

            Account receiverAccount = libAccountService.findAccountByIdServer(socialInteraction.getReceiverAccount().getIdServer());

            SocialInteractionType socialInteractionType = getSocialInteractionType(type);

            if (SocialInteractionTypeEnum.COMMENT.equals(getSocialInteractionTypeEnum(type)) && StringUtils.isBlank(socialInteraction.getContent())){
                throw new GenericException("content of social Interaction must exists");
            }

            boolean shouldContinue = true;
            if (SocialInteractionTypeEnum.FAVORITE.equals(getSocialInteractionTypeEnum(type))){
                Optional<List<SocialInteraction>> socialInteractions = socialInteractionRepository.findByActiveTrueAndDeletedFalseAndEmitterAccountAndReceiverAccount(emitterAccount, receiverAccount);
                if (socialInteractions.isPresent() && !CollectionUtils.isEmpty(socialInteractions.get())){
                    socialInteractions.get().forEach(favorite -> favorite.setActive(false));
                    socialInteractionRepository.saveAll(socialInteractions.get());
                    socialInteraction.setActive(false);
                    shouldContinue = false;
                }
            }

            if (shouldContinue){
                socialInteraction.setSocialInteractionType(socialInteractionType);
                socialInteraction.setIdServer(String.valueOf(System.nanoTime()));
                socialInteraction = socialInteractionRepository.save(socialInteraction);

                SocialInteractionMap socialInteractionMap = new SocialInteractionMap();
                socialInteractionMap.setIdServer(String.valueOf(System.nanoTime()));
                socialInteractionMap.setElement(element);
                socialInteractionMap.setSocialInteraction(socialInteraction);
                socialInteractionMap = socialInteractionMapRepository.save(socialInteractionMap);

                socialInteraction.setSocialInteractionMap(socialInteractionMap);
                socialInteraction = socialInteractionRepository.save(socialInteraction);

                //create publication when is SHARE
                if (socialInteractionType.getLabel().equalsIgnoreCase(SocialInteractionTypeEnum.SHARE.name())){
                    publicationRepository.save(Publication.builder().authorId(emitterAccount.getId())
                            .contentId(socialInteraction.getId()).type(PublicationType.SHARE).build());
                }

                // Creation de la notification
                libNotificationService.createNotification(socialInteraction);
                sendEmail(socialInteraction);
            }
            return socialInteraction;
        }catch (GenericException e){
            throw new GenericException("Error saving shared post");
        }
    }

    @Override
    public List<SocialInteraction> findSocialInteractionsByType(String accountId, String type) throws GenericException {
        return socialInteractionRepository.findByActiveTrueAndDeletedFalseAndEmitterAccountIdServerAndSocialInteractionTypeLabel(accountId, getSocialInteractionType(type).getLabel());
    }

    @Override
    public Long countSocialInteractionOnElementByType(String element, String type) {
        try {
            SocialInteractionTypeEnum typeEnum = getSocialInteractionTypeEnum(type);
            return socialInteractionMapRepository.countAllByElementAndSocialInteraction_SocialInteractionType_Label(element, typeEnum.name());
        }catch (GenericException e){
            return 0L;
        }
    }

    @Override
    public boolean isSocialInteractionOnElementByType(String accountId, String element, String type) throws GenericException {
        try {
            return socialInteractionRepository.existsByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionTypeAndSocialInteractionMap_Element(getAccount(accountId), getSocialInteractionType(type), element);
        }catch (GenericException e){
            return false;
        }
    }

    @Override
    public List<SocialInteraction> findSocialInteractionOnElementByType(String element, String type) throws GenericException {
        try {
            return socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionTypeAndSocialInteractionMap_Element(getSocialInteractionType(type), element).orElse(new ArrayList<>());
        }catch (GenericException e){
            return new ArrayList<>();
        }
    }

    @Override
    public boolean deleteSocialInteration(String accountId, String socialInteractionId) throws GenericException {
        try {
            Account account = getAccount(accountId);
            Optional<SocialInteraction> socialInteraction  = socialInteractionRepository.findByIdServer(socialInteractionId);
            if (!socialInteraction.isPresent()){
                throw new GenericException("Social interaction unidentified");
            }
            if (!account.getIdServer().equals(socialInteraction.get().getEmitterAccount().getIdServer())){
                throw new GenericException("Social interaction doesn't belong to this account");
            }
            socialInteraction.get().setDeleted(true);
            socialInteractionRepository.save(socialInteraction.get());
            return true;
        }catch (GenericException e){
            return false;
        }
    }

    @Override
    public SocialInteraction findById(String id) throws GenericException {
        return socialInteractionRepository.findByIdServer(id).orElse(null);
    }

    @Override
    public Long countUnreadMessagesByReceiver(Account receiver) {
        return libMessageService.countUnreadMessagesByReceiver(receiver);
    }

    @Override
    public Boolean migrate() throws GenericException {
        List<Subscription> subscriptions = subscriptionRepository.findAll();
        if (CollectionUtils.isEmpty(subscriptions)){
            return true;
        }
        SocialInteractionType socialInteractionType = getSocialInteractionType(SocialInteractionTypeEnum.FOLLOWER.name());
        subscriptions.forEach(subscription -> {
            SocialInteraction socialInteraction = new SocialInteraction();
            socialInteraction.setSocialInteractionType(socialInteractionType);
            socialInteraction.setIdServer(subscription.getIdServer());
            socialInteraction.setEmitterAccount(subscription.getFollower());
            socialInteraction.setReceiverAccount(subscription.getFollowee());

            socialInteraction = socialInteractionRepository.save(socialInteraction);

            SocialInteractionMap socialInteractionMap = new SocialInteractionMap();
            socialInteractionMap.setIdServer(String.valueOf(System.nanoTime()));
            socialInteractionMap.setElement(subscription.getFollowee().getIdServer());
            socialInteractionMap.setSocialInteraction(socialInteraction);
            socialInteractionMap = socialInteractionMapRepository.save(socialInteractionMap);

            socialInteraction.setSocialInteractionMap(socialInteractionMap);
            socialInteractionRepository.save(socialInteraction);
        });
        return true;
    }

    private Account getAccount(String accountId) throws GenericException{
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account unidentified");
        }
        return account.get();
    }

    private SocialInteractionTypeEnum getSocialInteractionTypeEnum(String type) throws GenericException{
        SocialInteractionTypeEnum typeEnum = SocialInteractionTypeEnum.getEnumFromName(type);
        if (typeEnum == null){
            throw new GenericException("Social Interaction type unidentified");
        }
        return typeEnum;
    }

    private SocialInteractionType getSocialInteractionType(String type) throws GenericException{
        Optional<SocialInteractionType> socialInteractionType = socialInteractionTypeRepository.findByLabel(getSocialInteractionTypeEnum(type).name());
        if (!socialInteractionType.isPresent()){
            throw new GenericException("Social Interaction type unidentified");
        }
        return socialInteractionType.get();
    }

    /**
     * send email via email service
     * @param socialInteraction social interaction
     */
    private void sendEmail(SocialInteraction socialInteraction){
        EmailDto emailDto = new EmailDto();
        MessageDto messageDto = new MessageDto();
        messageDto.setFrom(socialInteraction.getEmitterAccount().getEmail());
        List<String> receivers = new ArrayList<>();
        receivers.add(socialInteraction.getReceiverAccount().getEmail());
        messageDto.setTo(receivers);
        messageDto.setSubject(socialInteraction.getSocialInteractionType().getLabel());
        messageDto.setBody(socialInteraction.getSocialInteractionType().getLabel());
        emailDto.setMessage(messageDto);

        emailDto.setType(TypeEnum.fromSocialInteractionType(socialInteraction.getSocialInteractionType()));
        emailDto.setTemplate("notification.ftl");

        String postId = socialInteraction.getSocialInteractionMap().getElement();

        Optional<Post> post = postRepository.findByIdServer(postId);

        Long nbPosts = postRepository.countByActiveTrueAndDeletedFalseAndAccount(socialInteraction.getEmitterAccount());
        Long nbFollowers = libAccountService.countFollowers(socialInteraction.getEmitterAccount());
        Long nbFollowees = libAccountService.countFollowees(socialInteraction.getEmitterAccount());

        List<KeyValueDto> values = new ArrayList<>();
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_USERNAME.name(), socialInteraction.getReceiverAccount().getFirstname() + " " + socialInteraction.getReceiverAccount().getLastname()));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_USERNAME.name(), socialInteraction.getEmitterAccount().getFirstname() + " " + socialInteraction.getEmitterAccount().getLastname()));
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_PICTURE.name(), profilePicture(socialInteraction.getReceiverAccount())));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_PICTURE.name(), profilePicture(socialInteraction.getEmitterAccount())));
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_TITLE.name(), post.isPresent() ? post.get().getTitle() : "Notification"));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_STATUS.name(), socialInteraction.getEmitterAccount().getStatus() != null ? socialInteraction.getEmitterAccount().getStatus().getCode() : "Non renseigné"));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_JOB.name(), socialInteraction.getEmitterAccount().getCurrentJobTitle() != null ? socialInteraction.getEmitterAccount().getCurrentJobTitle() : "Non renseigné"));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_COMPANY.name(), socialInteraction.getEmitterAccount().getCurrentEstablishmentName() != null ? socialInteraction.getEmitterAccount().getCurrentEstablishmentName() : "Non renseigné"));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_LOCATION.name(), location(socialInteraction.getEmitterAccount())));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_NB_POSTS.name(), String.valueOf(nbPosts)));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_NB_FOLLOWERS.name(), String.valueOf(nbFollowers)));
        values.add(new KeyValueDto(TemplateKeysEnum.EMITTER_NB_FOLLOWEES.name(), String.valueOf(nbFollowees)));
        values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(),  hostname + "/view/post/" + postId));
        values.add(new KeyValueDto(TemplateKeysEnum.TYPE.name(),  "Notification"));

        values.addAll(updateKeyValues(emailDto.getType()));

        emailDto.setValues(values);
        emailDto.setAttachments(new ArrayList<>());

        emailService.sendEmail(emailDto);
    }

    private String profilePicture(Account account){
        if (account != null && StringUtils.isNotBlank(account.getIdServer())){
            return RESOURCE_URL + account.getIdServer() + "/" + account.getProfilePicture();
        }else {
            return "https://skillsmatesresources.s3.us-east-2.amazonaws.com/images/user.png";
        }
    }

    private String location(Account account){
        String location = "";
        if (StringUtils.isNotBlank(account.getCity())){
            location += location + account.getCity();
        }

        if (StringUtils.isNotBlank(account.getCountry())){
            location += location + " - " + account.getCountry();
        }
        return StringUtils.isNotBlank(location) ? location : "Non renseigné";
    }

    private List<KeyValueDto> updateKeyValues(TypeEnum typeEnum) {
        List<KeyValueDto> keyValues = new ArrayList<>();
        switch (typeEnum) {
            case FAVORITE:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouveau favoris"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "favoris"));
                break;
            case FOLLOWER:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouvel abonné"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "follower"));
                break;
            case POST:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouvelle publication"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "post"));
                break;
            case COMMENT:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouveau commentaire"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "comment"));
                break;
            case SHARE:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouveau partage"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "share"));
                break;
            case LIKE:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouveau j'aime"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "like"));
                break;
            case CHAT:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouveau message instantané"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "chat"));
                break;
            case REQUEST_RESET:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Réinitialisation du mot de passe"));
                keyValues.add(new KeyValueDto(KeyWordEnum.CTA_END_TEXT.name(), " si vous en êtes l'auteur."));
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_MESSAGE.name(),
                        "une demande de réinitialisation du mot de passe de votre compte Skills Mates a été initiée."));
                break;
            case CONFIRM_RESET:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Mot de passe réinitialisé"));
                keyValues.add(new KeyValueDto(KeyWordEnum.CTA_END_TEXT.name(), " pour accéder à la plateforme."));
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_MESSAGE.name(),
                        "le mot de passe de votre compte Skills Mates a été réinitialisé avec succès."));
                break;
            case ACCOUNT_CREATED:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Compte crée"));
                keyValues.add(new KeyValueDto(KeyWordEnum.CTA_END_TEXT.name(), " pour activer le compte."));
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_MESSAGE.name(),
                        "votre compte Skills Mates a été enregistré avec succès."));
                break;
            case ACCOUNT_ACTIVATED:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Compte Activé"));
                keyValues.add(new KeyValueDto(KeyWordEnum.CTA_END_TEXT.name(), " pour accéder à la plateforme."));
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_MESSAGE.name(),
                        "votre compte Skills Mates a été activé avec succès."));
                break;
            case ACCOUNT_DELETED_NOTIFICATION:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Suppression de compte"));
                keyValues.add(new KeyValueDto(KeyWordEnum.CTA_END_TEXT.name(), " pour accéder à la plateforme."));
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_MESSAGE.name(),
                        "une demande de suppression de votre compte Skills Mates a été initiée. <p>Votre compte a été désactivé et restera inactif pendant 14 jours avant d'être définitivement supprimé.</p> <p>Si vous n'êtes pas à l'origine de cette demande veillez l'annuler dans le delais susmentioné.</p>"));
                break;
            case ACCOUNT_DELETED_CONFIRMATION:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Compte supprimé"));
                keyValues.add(new KeyValueDto(KeyWordEnum.CTA_END_TEXT.name(), " pour réintégrer la communauté."));
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_MESSAGE.name(),
                        "votre compte Skills Mates a été définitivement supprimé au terme de la période de désactivation de 14 jours. <p>Nous regrettons votre départ et nous espérons vous revoir bientôt.</p>"));
                break;
            default:
                keyValues.add(new KeyValueDto(KeyWordEnum.EMAIL_TITLE.name(), "Nouvelle notification"));
                keyValues.add(new KeyValueDto(KeyWordEnum.NOTIFICATION_PICTURE.name(), "notification"));
                break;
        }
        return keyValues;
    }
}

@Getter
enum KeyWordEnum {
    EMAIL_TITLE, NOTIFICATION_PICTURE, CTA_END_TEXT, EMAIL_MESSAGE
}

