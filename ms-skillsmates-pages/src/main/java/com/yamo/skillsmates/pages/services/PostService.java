package com.yamo.skillsmates.pages.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.PostSocialInteractions;
import com.yamo.skillsmates.models.post.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Transactional
@Service
public interface PostService {
    /**
     * save a post
     * @param post
     * @param multipartFile
     * @return
     * @throws GenericException
     */
    Post savePost(Post post, MultipartFile multipartFile) throws GenericException;

    Post saveNewPost(PostDto postDto, MultipartFile multipartFile) throws GenericException;

    /**
     * save a post
     * @param post
     * @return
     * @throws GenericException
     */
    Post savePost(Post post) throws GenericException;

    /**
     * find all posts
     * @param accountId
     * @param pageable
     * @return
     * @throws GenericException
     */
    List<Post> findPosts(String accountId, Pageable pageable) throws GenericException;

    List<Post> findPostsByContentAndMediaSubtype(String content, String mediaSubtype, String mediaType, Pageable pageable) throws GenericException;

    /**
     * delete a post
     * @param accountId
     * @param postId
     * @return
     * @throws GenericException
     */
    boolean deletePost(String accountId, String postId) throws GenericException;

    /**
     * find a post knowing its idServer
     * @param idServer
     * @return
     */
    Post findPostByIdServer(String idServer);

    /**
     * update an existing post
     * @param post
     * @param multipartFile
     * @return
     * @throws GenericException
     */
    Post updatePost(Post post, MultipartFile multipartFile) throws GenericException;

    Post updateNewPost(PostDto postDto, MultipartFile multipartFile) throws GenericException;


    /**
     * update an existing post
     * @param post
     * @return
     * @throws GenericException
     */
    Post updatePost(Post post) throws GenericException;

    List<PostSocialInteractions> searchPosts(Specification<Post> specification, int page, int limit) throws GenericException;
    Long countSearchPosts(Specification<Post> specification) throws GenericException;
    Long countPosts(Account account) throws GenericException;
    Page<Post> findPageSearchPosts(Specification<Post> specification, int page, int limit) throws GenericException;
    Specification<Post> generatePostSpecification(SearchParam searchParam);
    Specification<Post> generatePostSpecification(SearchParam searchParam, MediaSubtypeDto mediaSubtypeDto);

    List<PostSocialInteractions> findPostsSocialInteractions(String accountId, Pageable pageable) throws GenericException;

    List<PostSocialInteractions> findDashboardPostsSocialInteractions(String accountId, int page, int limit) throws GenericException;

    PostSocialInteractions findSinglePostSocialInteractions(String postId) throws GenericException;
}
