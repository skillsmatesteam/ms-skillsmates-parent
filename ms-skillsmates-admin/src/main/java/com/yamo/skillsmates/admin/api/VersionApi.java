package com.yamo.skillsmates.admin.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.version.VersionDescriptionDto;
import com.yamo.skillsmates.dto.version.VersionDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-admin/versions", produces = MediaType.APPLICATION_JSON_VALUE)
public interface VersionApi {
    @PostMapping
    ResponseEntity<GenericResultDto> saveVersion(String token, String id, VersionDescriptionDto versionDescriptionDto) throws GenericException;

    @DeleteMapping(value = "/{versionId}")
    ResponseEntity<GenericResultDto> deleteVersion(String token, String id, String versionId) throws GenericException;

    @GetMapping
    ResponseEntity<GenericResultDto> findAllVersions(String token, String id) throws GenericException;

}
