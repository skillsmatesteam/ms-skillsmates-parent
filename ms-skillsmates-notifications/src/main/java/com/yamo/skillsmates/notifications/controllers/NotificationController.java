package com.yamo.skillsmates.notifications.controllers;

import com.yamo.skillsmates.common.enumeration.NotificationTypeEnum;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.notifications.NotificationDto;
import com.yamo.skillsmates.dto.notifications.NotificationsDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.notification.NotificationMapper;
import com.yamo.skillsmates.models.notifications.Notification;
import com.yamo.skillsmates.notifications.api.NotificationApi;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.notifications.services.NotificationService;
import com.yamo.skillsmates.services.LibAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class NotificationController extends AbstractController implements NotificationApi {

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private LibSocialInteractionService libSocialInteractionService;
    @Autowired
    private LibAccountService libAccountService;

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);

    @Override
    public ResponseEntity<GenericResultDto> getNotifications(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);

            NotificationsDto notifications = new NotificationsDto();
            notifications.setLikes(NotificationMapper.getInstance().asDtos(notificationService.findNotificationsByReceiverAccountAndType(id, NotificationTypeEnum.LIKE.name())));
            notifications.setComments(NotificationMapper.getInstance().asDtos(notificationService.findNotificationsByReceiverAccountAndType(id, NotificationTypeEnum.COMMENT.name())));
            notifications.setFollowers(NotificationMapper.getInstance().asDtos(notificationService.findNotificationsByReceiverAccountAndType(id, NotificationTypeEnum.FOLLOWER.name())));
            notifications.setShares(NotificationMapper.getInstance().asDtos(notificationService.findNotificationsByReceiverAccountAndType(id, NotificationTypeEnum.SHARE.name())));
            notifications.setFavorites(NotificationMapper.getInstance().asDtos(notificationService.findNotificationsByReceiverAccountAndType(id, NotificationTypeEnum.FAVORITE.name())));
            notifications.setMessages(NotificationMapper.getInstance().asNotifications(libSocialInteractionService.findUnreadMessagesByReceiver(libAccountService.findAccountByIdServer(id))));
            notifications.setPosts(NotificationMapper.getInstance().asDtos(notificationService.findNotificationsByReceiverAccountAndType(id, NotificationTypeEnum.POST.name())));
            result.setResultObject(notifications);
            genericResultDtoSuccess(id);

            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deactivateNotification(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody NotificationDto notificationDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);

            if (notificationDto == null){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Notification notification = notificationService.deactivateNotification(id, GenericMapper.INSTANCE.asEntity(notificationDto));
            notificationDto.setActive(notification.isActive());
            result.setResultObject(notificationDto);
            genericResultDtoSuccess(id);

            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
