package com.yamo.skillsmates.admin.services.impl;

import com.yamo.skillsmates.admin.services.VersionService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.version.Version;
import com.yamo.skillsmates.repositories.version.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VersionServiceImpl implements VersionService {

    private final VersionRepository versionRepository;

    @Autowired
    public VersionServiceImpl(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    @Override
    public List<Version> findAllVersions() {
        return versionRepository.findAll();
    }

    @Override
    public List<Version> saveVersion(List<Version> versions) throws GenericException {
        Optional<List<Version>> oldVersions = versionRepository.findByActiveTrueAndDeletedFalse();
        if (oldVersions.isPresent()){
            oldVersions.get().forEach(version -> version.setActive(false));
            versionRepository.saveAll(oldVersions.get());
        }
        versions.forEach(version -> version.setIdServer(String.valueOf(System.nanoTime())));
        return versionRepository.saveAll(versions);
    }
}
