package com.yamo.skillsmates.admin.controllers;

import com.yamo.skillsmates.admin.api.PartnerApi;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.admin.PartnerDto;
import com.yamo.skillsmates.dto.admin.PartnersGeneralInfosDto;
import com.yamo.skillsmates.mail.util.MailUtil;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.ActivityAreaMapper;
import com.yamo.skillsmates.mapper.admin.CategoryMapper;
import com.yamo.skillsmates.mapper.admin.PartnerMapper;
import com.yamo.skillsmates.models.admin.Partner;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class PartnerController extends AbstractController implements PartnerApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerController.class);


    @Override
    public ResponseEntity<GenericResultDto> createPartner(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody PartnerDto partnerDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        processToken(token, id);

        if (!validatePartnerParameters(partnerDto)){
            return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
        }

        try {
            partnerDto.setName(StringHelper.capitalizeFirstCharacter(partnerDto.getName()));
            partnerDto.setEmail(partnerDto.getEmail().toLowerCase());

            Partner partner = GenericMapper.INSTANCE.asEntity(partnerDto);
            partner = partnerService.savePartner(partner);

            result.setResultObject(GenericMapper.INSTANCE.asDto(partner));
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        LOGGER.info("{}" , LoggingUtil.END);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    private boolean validatePartnerParameters(PartnerDto partnerDto){
        return partnerDto != null &&
                StringUtils.isNotBlank(partnerDto.getName()) &&
                //StringUtils.isNotBlank(accountDto.getFirstname()) &&
                MailUtil.isEmailValid(partnerDto.getEmail()) ;
    }
    @Override
    public ResponseEntity<GenericResultDto> findPartnerById(String token, String id, String partnerId) throws GenericException {
        return null;
    }

    @Override
    public ResponseEntity<GenericResultDto> deletePartner(String token, String id, String partnerId) throws GenericException {
        return null;
    }

    @Override
    public ResponseEntity<GenericResultDto> findPartnersGeneralInfos(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);

            PartnersGeneralInfosDto partnersGeneralInfosDto = new PartnersGeneralInfosDto();
            partnersGeneralInfosDto.setPartners(PartnerMapper.getInstance().asDtos(partnerService.findAllPartners()));
            partnersGeneralInfosDto.setCategories(CategoryMapper.getInstance().asDtos(categoryService.findAllCategories()));
            partnersGeneralInfosDto.setActivitiesAreas(ActivityAreaMapper.getInstance().asDtos(libAccountService.findAllActivitiesAreas()));

            genericResultDtoSuccess(id);
            result.setResultObject(partnersGeneralInfosDto);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}
