package com.yamo.skillsmates.accounts.controlers;

import com.yamo.skillsmates.accounts.services.*;
import com.yamo.skillsmates.mail.service.LibConversationService;
import com.yamo.skillsmates.mail.service.LibMessageService;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.communication.ConversationRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.HeaderValidator;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

public abstract class AbstractControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    protected AccountService accountService;
    @MockBean
    protected HeaderValidator headerValidator;
    @MockBean
    protected AuthenticateService authenticateService;
    @MockBean
    protected JwtTokenUtil jwtTokenUtil;
    @MockBean
    protected TokenService tokenService;
    @MockBean
    protected LibAccountService libAccountService;
    @MockBean
    protected AcademicService academicService;
    @MockBean
    protected ProfessionalService professionalService;
    @MockBean
    protected SkillService skillService;
    @MockBean
    protected PasswordService passwordService;
    @MockBean
    protected LibSocialInteractionService libSocialInteractionService;

    @MockBean
    protected LibConversationService libConversationService;
    @MockBean
    protected ConversationRepository conversationRepository;
    @MockBean
    protected LibMessageService libMessageService;
    @MockBean
    protected AccountRepository accountRepository;
}
