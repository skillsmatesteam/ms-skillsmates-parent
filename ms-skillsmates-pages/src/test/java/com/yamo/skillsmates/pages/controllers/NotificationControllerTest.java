package com.yamo.skillsmates.pages.controllers;

import com.yamo.skillsmates.common.enumeration.NotificationTypeEnum;
import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.models.notifications.NotificationType;
import com.yamo.skillsmates.models.post.SocialInteractionType;
import com.yamo.skillsmates.pages.ApplicationTest;
import com.yamo.skillsmates.repositories.notifications.NotificationTypeRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionTypeRepository;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationTest.class })
public class NotificationControllerTest {

    @Autowired
    private NotificationTypeRepository notificationTypeRepository;
    @Autowired
    private SocialInteractionTypeRepository socialInteractionTypeRepository;

    public void generateNotificationType(){
        List<NotificationType> notificationTypes = new ArrayList<>();
        for (NotificationTypeEnum notificationTypeEnum : NotificationTypeEnum.values()){
            NotificationType notificationType = new NotificationType();
            notificationType.setIdServer(String.valueOf(System.nanoTime()));
            notificationType.setLabel(notificationTypeEnum.name());
            notificationTypes.add(notificationType);
        }

        notificationTypeRepository.saveAll(notificationTypes);
    }

    public void generateSocialInterationType(){
        List<SocialInteractionType> socialInteractionTypes = new ArrayList<>();
        for (SocialInteractionTypeEnum socialInteractionTypeEnum : SocialInteractionTypeEnum.values()){
            SocialInteractionType socialInteractionType = new SocialInteractionType();
            socialInteractionType.setIdServer(String.valueOf(System.nanoTime()));
            socialInteractionType.setLabel(socialInteractionTypeEnum.name());
            socialInteractionTypes.add(socialInteractionType);
        }
        socialInteractionTypeRepository.saveAll(socialInteractionTypes);
    }
}
