package com.yamo.skillsmates.media.controllers;


import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.exception.InvalidParameterException;
import com.yamo.skillsmates.common.logging.LoggingHelper;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.media.helpers.MultimediaHelper;
import com.yamo.skillsmates.media.services.MultimediaService;
import com.yamo.skillsmates.validators.HeaderValidator;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

    protected static final String EMAIL_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Email";
    protected static final String PASSWORD_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Password";
    protected static final String TOKEN = SkillsmatesHeader.TOKEN;
    protected static final String ID = SkillsmatesHeader.ID;

    protected static final String MEDIA_CHECKSUM_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Media-Checksum";
    protected static final String ACCOUNT_ID_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Account-Id";
    protected static final String MULTIMEDIA_ID_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Multimedia-Id";
    protected static final String TYPE_MULTIMEDIA_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Type-Multimedia";

    @Autowired
    protected HeaderValidator headerValidator;
    @Autowired
    protected JwtTokenUtil jwtTokenUtil;
    @Autowired
    protected MultimediaService multimediaService;
    @Autowired
    protected MultimediaHelper multimediaHelper;

    @Value("${media.directory.root}")
    protected String rootDirectory;

    protected GenericResultDto result = null;

    protected ResponseEntity<GenericResultDto> genericResultDtoError(String message, HttpStatus httpStatus){
        InvalidParameterException e = new InvalidParameterException(message);
        result = new GenericResultDto();
        result.setHttpStatus(httpStatus);
        result.setMessage(e.getMessage());
        result.setGenericMessageDetailsList(LoggingHelper.buildGenericMessageDetails(e));
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    protected GenericResultDto processToken(String token, String id) throws GenericException {
        if (StringUtils.isBlank(token) || StringUtils.isBlank(id)){
            throw new GenericException("Invalid input data");
        }

        // validation du token
        result = headerValidator.processHeader(token, id);
        if (result != null)
            throw new GenericException("Invalid input data");

        result = new GenericResultDto<>();
        //generate new token
        result.setHttpHeaders(TOKEN,jwtTokenUtil.generateToken(id));

        return result;
    }

    protected void genericResultDtoSuccess(String id){
        result.setHttpStatus(HttpStatus.OK);
        result.setMessage("Success");

        //generate token
        result.setHttpHeaders(ID, id);
        result.setHttpHeaders(TOKEN, jwtTokenUtil.generateToken(id));
    }
}
