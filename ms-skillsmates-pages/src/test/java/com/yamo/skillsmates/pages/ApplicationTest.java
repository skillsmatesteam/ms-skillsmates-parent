package com.yamo.skillsmates.pages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The Class ApplicationTest.
 */
@EnableJpaRepositories(basePackages = { "com.yamo.skillsmates.repositories" })
@ComponentScan(basePackages = { "com.yamo" })
@EntityScan(basePackages = { "com.yamo.skillsmates.models" })
@Configuration
@EnableAutoConfiguration
public class ApplicationTest {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ApplicationTest.class, args);
	}
}
