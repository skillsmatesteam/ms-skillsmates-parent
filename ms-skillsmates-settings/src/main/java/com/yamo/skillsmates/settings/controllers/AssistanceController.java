package com.yamo.skillsmates.settings.controllers;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.assistance.AssistanceDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.assistance.AssistanceMapper;
import com.yamo.skillsmates.settings.api.AssistanceApi;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class AssistanceController extends AbstractController implements AssistanceApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(AssistanceController.class);

    @Override
    public ResponseEntity<GenericResultDto> saveAssistance(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody AssistanceDto assistanceDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateAssistance(assistanceDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            result.setResultObject(GenericMapper.INSTANCE.asDto(assistanceService.saveAssistance(GenericMapper.INSTANCE.asEntity(assistanceDto))));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getAssistances(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            result.setResultObjects(AssistanceMapper.getInstance().asDtos(assistanceService.findAllAssistances()));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * check if assistance is correct
     * @param assistanceDto to be check
     * @return true if assistance is ok , false otherwise
     */
    private boolean validateAssistance(AssistanceDto assistanceDto){
        return assistanceDto != null &&
                assistanceDto.getAccount() != null &&
                StringUtils.isNotBlank(assistanceDto.getTopic()) &&
                StringUtils.isNotBlank(assistanceDto.getContent());
    }
}
