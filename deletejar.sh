#!/bin/bash
#
# Cyrille MOFFO
#
# Script de suppression des jars skillsmates
#
find . -name "*.jar" -type f -delete
find . -name "*.log" -type f -delete