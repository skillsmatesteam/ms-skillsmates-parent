package com.yamo.skillsmates.settings.controllers;

import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.exception.InvalidParameterException;
import com.yamo.skillsmates.common.logging.LoggingHelper;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.settings.http.EmailService;
import com.yamo.skillsmates.settings.services.AssistanceService;
import com.yamo.skillsmates.validators.HeaderValidator;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController {

    protected static final String TOKEN = SkillsmatesHeader.TOKEN;
    protected static final String ID = SkillsmatesHeader.ID;

    @Autowired
    protected HeaderValidator headerValidator;
    @Autowired
    protected JwtTokenUtil jwtTokenUtil;
    @Autowired
    protected LibAccountService libAccountService;
    @Autowired
    protected AssistanceService assistanceService;
    @Autowired
    protected EmailService emailService;

    protected GenericResultDto result = null;
    protected String newToken = null ;

    protected ResponseEntity<GenericResultDto> genericResultDtoError(String message, HttpStatus httpStatus){
        InvalidParameterException e = new InvalidParameterException(message);
        result = new GenericResultDto();
        result.setHttpStatus(httpStatus);
        result.setMessage(e.getMessage());
        result.setGenericMessageDetailsList(LoggingHelper.buildGenericMessageDetails(e));
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    protected GenericResultDto processToken(String token, String id) throws GenericException {
        if (StringUtils.isBlank(token) || StringUtils.isBlank(id)){
            throw new GenericException("Invalid input data");
        }

        // validation du token
        result = headerValidator.processHeader(token, id);
        if (result != null)
            throw new GenericException("Invalid input data");

        result = new GenericResultDto<>();
        return result;
    }

    protected void genericResultDtoSuccess(String id){
        result.setHttpStatus(HttpStatus.OK);
        result.setMessage("Success");

        //generate token
        result.setHttpHeaders(ID, id);
        result.setHttpHeaders(TOKEN, jwtTokenUtil.generateToken(id));
    }
}
