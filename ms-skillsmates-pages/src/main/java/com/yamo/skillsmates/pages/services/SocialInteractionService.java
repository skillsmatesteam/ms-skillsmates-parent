package com.yamo.skillsmates.pages.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.SocialInteraction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public interface SocialInteractionService {
    /**
     *
     * @param socialInteraction: social interaction to be saved
     * @param type : type df the social interaction
     * @param element: element on which the social interaction has been made
     * @return saved social interaction
     * @throws GenericException
     */
    SocialInteraction saveSocialInteration(SocialInteraction socialInteraction, String type, String element) throws GenericException;
    List<SocialInteraction> findSocialInteractionsByType(String accountId, String type) throws GenericException;
    Long countSocialInteractionOnElementByType(String element, String type);
    boolean isSocialInteractionOnElementByType(String accountId, String element, String type) throws GenericException;
    List<SocialInteraction> findSocialInteractionOnElementByType(String element, String type) throws GenericException;
    boolean deleteSocialInteration(String accountId, String socialInteractionId) throws GenericException;
    SocialInteraction findById(String id) throws GenericException;
    Long countUnreadMessagesByReceiver(Account receiver);
    Boolean migrate() throws GenericException;
}
