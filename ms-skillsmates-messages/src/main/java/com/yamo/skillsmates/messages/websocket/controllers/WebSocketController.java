package com.yamo.skillsmates.messages.websocket.controllers;

import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.dto.post.SocialInteractionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.security.Principal;

@CrossOrigin
@Controller
public class WebSocketController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketController.class);
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/user")
    public void sendPushNotification(Principal principal, SocialInteractionDto socialInteractionDto) throws  Exception {
        LOGGER.info("{} -- {}" , LoggingUtil.START, socialInteractionDto);
        messagingTemplate.convertAndSendToUser(socialInteractionDto.getReceiverAccount().getIdServer(), "/queue/chat", socialInteractionDto);
    }
}
