package com.yamo.skillsmates.notifications.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.notifications.Notification;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NotificationService {
    boolean addNotification(Account account, String idElement, Account notifiedAccount, String type) throws GenericException;
    List<Notification> findNotifications(String accountId, Pageable pageable) throws GenericException;
    Long countActiveNotificationsByAccountAndType(String accountId, String labelTypeNotification) throws GenericException;
    List<Notification> findNotificationsByReceiverAccountAndType(String accountId, String labelTypeNotification) throws GenericException;
    Notification deactivateNotification(String accountId, Notification notification) throws GenericException;
}
