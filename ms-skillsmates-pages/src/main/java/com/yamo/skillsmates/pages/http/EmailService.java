package com.yamo.skillsmates.pages.http;

import com.yamo.skillsmates.pages.http.dto.EmailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmailService {

    @Value("${service.email.url}")
    private String emailUrl;

    private final RestTemplate restTemplate;

    @Autowired
    public EmailService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * get email by id
     * @param emailId email id
     * @return email dto
     */
    public EmailDto getEmail(String emailId){
        return this.restTemplate.getForObject(emailUrl + "/" + emailId, EmailDto.class);
    }

    /**
     * send email
     * @param emailDto email dto
     * @return string
     */
    public String sendEmail(EmailDto emailDto){
        return this.restTemplate.postForObject(emailUrl, emailDto, String.class);
    }
}
