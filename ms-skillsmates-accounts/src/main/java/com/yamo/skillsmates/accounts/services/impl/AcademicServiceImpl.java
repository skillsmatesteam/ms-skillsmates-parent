package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.services.AcademicService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.*;
import com.yamo.skillsmates.models.account.config.*;
import com.yamo.skillsmates.repositories.account.*;
import com.yamo.skillsmates.services.LibAccountService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AcademicServiceImpl implements AcademicService {

    private final StudyLevelRepository studyLevelRepository;
    private final TeachingAreaRepository teachingAreaRepository;
    private final EstablishmentTypeRepository establishmentTypeRepository;
    private final EstablishmentCertificationTypeRepository establishmentCertificationTypeRepository;
    private final EducationRepository educationRepository;
    private final DegreeObtainedRepository degreeObtainedRepository;
    private final CertificationRepository certificationRepository;
    private final HigherEducationRepository higherEducationRepository;
    private final DiplomaRepository diplomaRepository;
    private final SecondaryEducationRepository secondaryEducationRepository;
    private final ActivityAreaRepository activityAreaRepository;
    private final LibAccountService libAccountService;
    private final AccountRepository accountRepository;

    @Autowired
    public AcademicServiceImpl(StudyLevelRepository studyLevelRepository, TeachingAreaRepository teachingAreaRepository, EstablishmentTypeRepository establishmentTypeRepository, EstablishmentCertificationTypeRepository establishmentCertificationTypeRepository, EducationRepository educationRepository, DegreeObtainedRepository degreeObtainedRepository, CertificationRepository certificationRepository, HigherEducationRepository higherEducationRepository, DiplomaRepository diplomaRepository, SecondaryEducationRepository secondaryEducationRepository, ActivityAreaRepository activityAreaRepository, LibAccountService libAccountService,  AccountRepository accountRepository) {
        this.studyLevelRepository = studyLevelRepository;
        this.teachingAreaRepository = teachingAreaRepository;
        this.establishmentTypeRepository = establishmentTypeRepository;
        this.establishmentCertificationTypeRepository = establishmentCertificationTypeRepository;
        this.educationRepository = educationRepository;
        this.degreeObtainedRepository = degreeObtainedRepository;
        this.certificationRepository = certificationRepository;
        this.higherEducationRepository = higherEducationRepository;
        this.diplomaRepository = diplomaRepository;
        this.secondaryEducationRepository = secondaryEducationRepository;
        this.activityAreaRepository = activityAreaRepository;
        this.libAccountService = libAccountService;
        this.accountRepository = accountRepository;
    }

    @Override
    public List<ActivityArea> findActivityAreas() {
        return activityAreaRepository.findAll();
    }

    @Override
    public List<DegreeObtained> findDegreesObtained(String accountId, Pageable pageable) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return degreeObtainedRepository.findAllByDeletedFalseAndAccount(optionalAccount.get(), pageable).orElseGet(ArrayList::new);
    }

    @Override
    public List<Certification> findCertificates(String accountId, Pageable pageable) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return certificationRepository.findAllByDeletedFalseAndAccount(optionalAccount.get(), pageable).orElseGet(ArrayList::new);
    }

    @Override
    public DegreeObtained saveDegreeObtained(DegreeObtained degreeObtained) throws GenericException {
        Optional<Diploma> diploma = diplomaRepository.findByIdServer(degreeObtained.getDiploma().getIdServer());
        if (!diploma.isPresent()){
            throw new GenericException("Study level not found");
        }
        degreeObtained.setDiploma(diploma.get());
        if (StringUtils.isBlank(degreeObtained.getSpecialty())){
            throw new GenericException("speciality not found");
        }
        if (validateTeachingArea(degreeObtained)) {
            degreeObtained.setTeachingArea(null);
        }else {
            Optional<TeachingArea> teachingArea = teachingAreaRepository.findByIdServer(degreeObtained.getTeachingArea().getIdServer());
            if (!teachingArea.isPresent()) {
                throw new GenericException("Teaching area not found");
            }
            degreeObtained.setTeachingArea(teachingArea.get());
        }
        Optional<EstablishmentType> establishmentType = establishmentTypeRepository.findByIdServer(degreeObtained.getEstablishmentType().getIdServer());
        if (!establishmentType.isPresent()){
            throw new GenericException("Establishment type not found");
        }
        degreeObtained.setEstablishmentType(establishmentType.get());
        Optional<Account> account = accountRepository.findByIdServer(degreeObtained.getAccount().getIdServer());
        if (!account.isPresent()){
            throw new GenericException("account not found");
        }
        degreeObtained.setAccount(account.get());
        degreeObtained.setIdServer(String.valueOf(System.nanoTime()));
        return degreeObtainedRepository.save(degreeObtained);
    }

    @Override
    public Certification saveCertification(Certification certification) throws GenericException {
        Optional<ActivityArea> activityArea = activityAreaRepository.findByIdServer(certification.getActivityArea().getIdServer());
        if (!activityArea.isPresent()){
            throw new GenericException("Activity Area not found");
        }
        certification.setActivityArea(activityArea.get());

        Optional<EstablishmentCertificationType> establishmentCertificationType = establishmentCertificationTypeRepository.findByIdServer(certification.getEstablishmentCertificationType().getIdServer());
        if (!establishmentCertificationType.isPresent()){
            throw new GenericException("Establishment certification type not found");
        }
        certification.setEstablishmentCertificationType(establishmentCertificationType.get());
        Optional<Account> account = accountRepository.findByIdServer(certification.getAccount().getIdServer());
        if (!account.isPresent()){
            throw new GenericException("account not found");
        }
        certification.setAccount(account.get());
        certification.setIdServer(String.valueOf(System.nanoTime()));
        return certificationRepository.save(certification);
    }

    @Override
    public boolean deleteDegreeObtained(String idServer, String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        Optional<DegreeObtained> degreeObtainedOptional = degreeObtainedRepository.findByIdServerAndAccount(idServer, optionalAccount.get());
        if (!degreeObtainedOptional.isPresent()){
            throw new GenericException("Degree obtained does not exists");
        }
        degreeObtainedOptional.get().setDeleted(true);
        degreeObtainedRepository.save(degreeObtainedOptional.get());
        return true;
    }

    @Override
    public boolean deleteCertification(String idServer, String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        Optional<Certification> certificationOptional = certificationRepository.findByIdServerAndAccount(idServer, optionalAccount.get());
        if (!certificationOptional.isPresent()){
            throw new GenericException("Certification does not exists");
        }
        certificationOptional.get().setDeleted(true);
        certificationRepository.save(certificationOptional.get());
        return true;
    }

    @Override
    public List<SecondaryEducation> findSecondaryEducations(String accountId, Pageable pageable) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return secondaryEducationRepository.findAllByDeletedFalseAndAccount(optionalAccount.get(), pageable).orElseGet(ArrayList::new);
    }

    @Override
    public SecondaryEducation saveSecondaryEducation(SecondaryEducation secondaryEducation) throws GenericException {
        Optional<StudyLevel> studyLevel = studyLevelRepository.findByIdServer(secondaryEducation.getStudyLevel().getIdServer());
        if (!studyLevel.isPresent()){
            throw new GenericException("Frequented class not found");
        }
        secondaryEducation.setStudyLevel(studyLevel.get());
        if (StringUtils.isBlank(secondaryEducation.getSpecialty())){
            throw new GenericException("speciality not found");
        }
        Optional<EstablishmentType> establishmentType = establishmentTypeRepository.findByIdServer(secondaryEducation.getEstablishmentType().getIdServer());
        if (!establishmentType.isPresent()){
            throw new GenericException("Establishment type not found");
        }
        secondaryEducation.setEstablishmentType(establishmentType.get());
        Optional<Diploma> preparedDiploma = diplomaRepository.findByIdServer(secondaryEducation.getPreparedDiploma().getIdServer());
        if (!preparedDiploma.isPresent()){
            throw new GenericException("Prepared diploma not found");
        }
        secondaryEducation.setPreparedDiploma(preparedDiploma.get());
        Optional<Account> account = accountRepository.findByIdServer(secondaryEducation.getAccount().getIdServer());
        if (!account.isPresent()){
            throw new GenericException("account not found");
        }
        secondaryEducation.setAccount(account.get());
        secondaryEducation.setIdServer(String.valueOf(System.nanoTime()));
        secondaryEducation = secondaryEducationRepository.save(secondaryEducation);
        libAccountService.updateProfessionalTitleToAccount(secondaryEducation.getAccount());
        return secondaryEducation;
    }

    @Override
    public boolean deleteSecondaryEducation(String idServer, String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        Optional<SecondaryEducation> secondaryEducation = secondaryEducationRepository.findByIdServerAndAccount(idServer, optionalAccount.get());
        if (!secondaryEducation.isPresent()){
            throw new GenericException("Secondary education does not exists");
        }
        secondaryEducation.get().setDeleted(true);
        secondaryEducationRepository.save(secondaryEducation.get());
        libAccountService.updateProfessionalTitleToAccount(secondaryEducation.get().getAccount());
        return true;
    }

    @Override
    public List<HigherEducation> findHigherEducations(String accountId, Pageable pageable) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return higherEducationRepository.findAllByDeletedFalseAndAccount(optionalAccount.get(), pageable).orElseGet(ArrayList::new);
    }

    @Override
    public HigherEducation saveHigherEducation(HigherEducation higherEducation) throws GenericException {
        Optional<StudyLevel> studyLevel = studyLevelRepository.findByIdServer(higherEducation.getStudyLevel().getIdServer());
        if (!studyLevel.isPresent()){
            throw new GenericException("Study level not found");
        }
        higherEducation.setStudyLevel(studyLevel.get());
        Optional<TeachingArea> teachingArea = teachingAreaRepository.findByIdServer(higherEducation.getTeachingArea().getIdServer());
        if (!teachingArea.isPresent()){
            throw new GenericException("Teaching area not found");
        }
        higherEducation.setTeachingArea(teachingArea.get());
        Optional<Diploma> targetedDiploma = diplomaRepository.findByIdServer(higherEducation.getTargetedDiploma().getIdServer());
        if (!targetedDiploma.isPresent()){
            throw new GenericException("Targeted diploma not found");
        }
        higherEducation.setTargetedDiploma(targetedDiploma.get());

        Optional<EstablishmentType> establishmentType = establishmentTypeRepository.findByIdServer(higherEducation.getEstablishmentType().getIdServer());
        if (!establishmentType.isPresent()){
            throw new GenericException("Establishment type not found");
        }
        higherEducation.setEstablishmentType(establishmentType.get());
        Optional<Account> account = accountRepository.findByIdServer(higherEducation.getAccount().getIdServer());
        if (!account.isPresent()){
            throw new GenericException("account not found");
        }
        higherEducation.setAccount(account.get());
        higherEducation.setIdServer(String.valueOf(System.nanoTime()));
        higherEducation = higherEducationRepository.save(higherEducation);
        libAccountService.updateProfessionalTitleToAccount(higherEducation.getAccount());
        return higherEducation;
    }

    @Override
    public boolean deleteHigherEducation(String idServer, String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        Optional<HigherEducation> higherEducation = higherEducationRepository.findByIdServerAndAccount(idServer, optionalAccount.get());
        if (!higherEducation.isPresent()){
            throw new GenericException("Higher education does not exists");
        }
        higherEducation.get().setDeleted(true);
        higherEducationRepository.save(higherEducation.get());
        libAccountService.updateProfessionalTitleToAccount(higherEducation.get().getAccount());
        return true;
    }

    @Override
    public List<Education> findAllEducations() {
        return educationRepository.findAll();
    }

    @Override
    public List<EstablishmentType> findAllEstablishmentTypes() {
        return establishmentTypeRepository.findAll();
    }

    @Override
    public List<EstablishmentCertificationType> findAllEstablishmentCertificationTypes() {
        return establishmentCertificationTypeRepository.findAll();
    }


    @Override
    public List<Diploma> findAllDiplomas() {
        return diplomaRepository.findAll();
    }

    @Override
    public List<StudyLevel> findAllStudyLevels() {
        return studyLevelRepository.findAll();
    }

    @Override
    public List<TeachingArea> findAllTeachingAreas() {
        return teachingAreaRepository.findAll();
    }

    @Override
    public long countAcademics(String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return secondaryEducationRepository.countAllByActiveTrueAndDeletedFalseAndAccount(optionalAccount.get()) +
                higherEducationRepository.countAllByActiveTrueAndDeletedFalseAndAccount(optionalAccount.get()) +
                degreeObtainedRepository.countAllByActiveTrueAndDeletedFalseAndAccount(optionalAccount.get())+
                certificationRepository.countAllByActiveTrueAndDeletedFalseAndAccount(optionalAccount.get());
    }

    private boolean validateTeachingArea(DegreeObtained degreeObtained){
        if (degreeObtained.getDiploma().getEducation().getCode().equalsIgnoreCase("higher")){
            return degreeObtained.getTeachingArea() != null;
        }
        return true;
    }
}
