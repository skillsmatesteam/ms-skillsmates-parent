package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.AuthenticateApi;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.Role;
import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.mail.util.MailUtil;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Account;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class AuthenticateController extends AbstractController implements AuthenticateApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticateController.class);
    private static final String EMAIL_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Email";
    private static final String PASSWORD_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Password";
    private static final String TOKEN = SkillsmatesHeader.TOKEN;

    @Override
    public ResponseEntity<GenericResultDto> authenticateAccount(@RequestHeader(value = EMAIL_HEADER) String email,
                                                                @RequestHeader(value = PASSWORD_HEADER) String password) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        result = new GenericResultDto<>();
        if (!MailUtil.isEmailValid(email) || StringUtils.isBlank(password)){
            return genericResultDtoError(ResponseMessage.CREDENTIALS_INCORRECT.getMessage(), HttpStatus.BAD_REQUEST);
        }

        try {
            Account account = authenticateService.authenticate(email, password);
            accountService.setConnectedStatus(account.getIdServer(), true);
            result.setResultObject(GenericMapper.INSTANCE.asDto(account));
            genericResultDtoSuccess(account.getIdServer());
            LOGGER.info("{}" , LoggingUtil.END);

            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> renewPasswordAccount(String identifier) throws GenericException {
        return null;
    }

    @Override
    public ResponseEntity<GenericResultDto> verificateAccount(@RequestHeader(value = EMAIL_HEADER) String email) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        result = new GenericResultDto<>();
        try {
            if (!validateEmail(email)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Account account = authenticateService.verificate(email);
            genericResultDtoSuccess(account.getIdServer());
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> authenticateAdminAccount(@RequestHeader(value = EMAIL_HEADER) String email,
                                                                     @RequestHeader(value = PASSWORD_HEADER) String password) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        result = new GenericResultDto<>();
        if (!MailUtil.isEmailValid(email) || StringUtils.isBlank(password)){
            return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
        }

        try {
            Account account = authenticateService.authenticate(email, password);
            if (Role.ADMIN.name().equals(account.getRole())){
                accountService.setConnectedStatus(account.getIdServer(), true);
                result.setResultObject(GenericMapper.INSTANCE.asDto(account));
                genericResultDtoSuccess(account.getIdServer());
            }else {
                genericResultDtoError(ResponseMessage.ACCOUNT_NOT_ALLOWED.getMessage(), HttpStatus.FORBIDDEN);
            }

            LOGGER.info("{}" , LoggingUtil.END);

            return new ResponseEntity<>(result, result.getHttpHeaders(), result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private boolean validateEmail(String email){
        return email != null && MailUtil.isEmailValid(email);
    }
}
