package com.yamo.skillsmates.media.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.multimedia.config.InfosMediaType;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface MultimediaService {
    Multimedia saveMultimedia(Multimedia multimedia, MultipartFile multipartFile, String accountId) throws GenericException;
    Map<String, Multimedia> saveAvatarMultimedia(Map<String, MultipartFile> files, String accountId) throws GenericException;
    List<Multimedia> findMultimediaByAccount(String accountId) throws GenericException;
    Multimedia findMultimediaByIdServer(String idServer, String accountId) throws GenericException;
    boolean deleteMultimedia(String idServer, String accountId) throws GenericException;
    List<InfosMediaType> findAllInfosMediaType();
    List<Multimedia> findMultimediaByType(String typeMultimedia, String accountId, Pageable pageable) throws GenericException;
    int countMultimediaByType(String typeMultimedia, String accountId) throws GenericException;
    List<MediaSubtype> findMediaSubtypes();
}
