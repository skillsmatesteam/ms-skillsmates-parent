package com.yamo.skillsmates.settings.services.impl;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.version.Version;
import com.yamo.skillsmates.repositories.version.VersionRepository;
import com.yamo.skillsmates.settings.services.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VersionServiceImpl implements VersionService {

    private final VersionRepository versionRepository;

    @Autowired
    public VersionServiceImpl(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    @Override
    public List<Version> findVersions() throws GenericException {
        return versionRepository.findByActiveTrueAndDeletedFalse().orElse(new ArrayList<>());
    }

    @Override
    public Boolean dismissVersion() throws GenericException {
        Optional<List<Version>> updates = versionRepository.findByActiveTrueAndDeletedFalse();
        if (updates.isPresent()){
            updates.get().forEach(update -> update.setActive(false));
            versionRepository.saveAll(updates.get());
        }
        return Boolean.TRUE;
    }
}
