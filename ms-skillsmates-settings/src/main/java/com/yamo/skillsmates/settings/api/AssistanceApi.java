package com.yamo.skillsmates.settings.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.assistance.AssistanceDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-settings/assistance", produces = MediaType.APPLICATION_JSON_VALUE)
public interface AssistanceApi {
    @PostMapping
    ResponseEntity<GenericResultDto> saveAssistance(String token, String id, AssistanceDto assistanceDto) throws GenericException;

    @GetMapping
    ResponseEntity<GenericResultDto> getAssistances(String token, String id) throws GenericException;
}
