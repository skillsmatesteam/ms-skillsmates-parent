package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.account.AccountNetworkDto;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.AccountSocialInteractions;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AccountService {
    Account saveAccount(Account account) throws GenericException;
    Account updateAccount(Account account) throws GenericException;
    List<Account> findAllAccounts();
    List<Account> findAllAccounts(String accountId, int page, int size) throws GenericException;
    Page<Account> findAllAccountsByPage(int page, int size) throws GenericException;
    Account findAccountByIdServer(String idServer);
    Account findAccountByEmail(String email);
    boolean deleteAccount(String idServer) throws GenericException;
    boolean deactivateAccount(String idServer) throws GenericException;
    Account activateAccount(String idServer) throws GenericException;
    List<ActivityArea> findAllActivitiesAreas();
    boolean setConnectedStatus(String accountId, boolean connected) throws GenericException;
    List<Account> findAllConnectedAccounts() throws GenericException;
    List<Account> findAllConnectedAccounts(String idServer, int page, int size) throws GenericException;
    List<AccountSocialInteractions> findAccountsByFavorite(String accountId) throws GenericException;
    Page<AccountSocialInteractions> findFavoriteAsAccountSocialInteraction(String accountId, int page, int size) throws GenericException;
    AccountSocialInteractions findAccountSocialInteractions(String idServer) throws GenericException;
    List<AccountSocialInteractions> searchAccounts(SearchParam searchParam) throws GenericException;

    AccountNetworkDto findNetwork(String accountId);
}
