package com.yamo.skillsmates.accounts.controllers;

import com.yamo.skillsmates.accounts.api.ProfessionalApi;
import com.yamo.skillsmates.accounts.services.ProfessionalService;
import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.DateHelper;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.account.AccountFilterDto;
import com.yamo.skillsmates.dto.account.AccountProfessionalDto;
import com.yamo.skillsmates.dto.account.ProfessionalDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.ActivityAreaMapper;
import com.yamo.skillsmates.mapper.account.ActivitySectorMapper;
import com.yamo.skillsmates.mapper.account.ProfessionalMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ProfessionalController extends AbstractController implements ProfessionalApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfessionalController.class);

    protected static final String PROFESSIONAL_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Professional-Id";

    @Autowired
    private ProfessionalService professionalService;

    @Override
    public ResponseEntity<GenericResultDto> createProfessional(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody ProfessionalDto professionalDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (!validateAccountProfessional(professionalDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            professionalDto.setJobTitle(StringHelper.capitalizeFirstCharacter(professionalDto.getJobTitle()));
            professionalDto.setEstablishmentName(StringHelper.capitalizeFirstCharacter(professionalDto.getEstablishmentName()));
            professionalDto.setCity(StringHelper.capitalizeFirstCharacter(professionalDto.getCity()));
            professionalService.saveProfessional(GenericMapper.INSTANCE.asEntity(professionalDto));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findProfessionalsById(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 100, Sort.by(Sort.Order.desc("id")));
            AccountProfessionalDto accountProfessionalDto = new AccountProfessionalDto();
            accountProfessionalDto.setActivityAreas(ActivityAreaMapper.getInstance().asDtos(professionalService.findActivityAreas()));
            accountProfessionalDto.setActivitySectors(ActivitySectorMapper.getInstance().asDtos(professionalService.findActivitySectors()));
            accountProfessionalDto.setProfessionals(ProfessionalMapper.getInstance().asDtos(professionalService.findProfessionals(accountId, pageable)));

            result.setResultObject(accountProfessionalDto);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findProfessionalsOfflineById(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 100, Sort.by(Sort.Order.desc("id")));
            AccountProfessionalDto accountProfessionalDto = new AccountProfessionalDto();
            accountProfessionalDto.setActivityAreas(ActivityAreaMapper.getInstance().asDtos(professionalService.findActivityAreas()));
            accountProfessionalDto.setActivitySectors(ActivitySectorMapper.getInstance().asDtos(professionalService.findActivitySectors()));
            accountProfessionalDto.setProfessionals(ProfessionalMapper.getInstance().asDtos(professionalService.findProfessionals(accountId, pageable)));

            result.setResultObject(accountProfessionalDto);
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteProfessional(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestHeader(value = PROFESSIONAL_HEADER) String professionalId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(professionalId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            professionalService.deleteProfessional(professionalId, id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> filterProfessionals(@RequestHeader(value = TOKEN)String token, @RequestHeader(value = ID) String id, @RequestBody AccountFilterDto accountFilterDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try{
            processToken(token, id);
            if (!validateAccountFilter(accountFilterDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            List<AccountDto> list = professionalService.filterAccounts(accountFilterDto);

            result.setResultObjects(list);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private boolean validateAccountProfessional(ProfessionalDto professionalDto){
        return professionalDto != null &&
                professionalDto.getActivityArea() != null &&
                professionalDto.getActivitySector() != null &&
                professionalDto.getStartDate() != null &&
                StringUtils.isNotBlank(professionalDto.getEstablishmentName()) &&
                StringUtils.isNotBlank(professionalDto.getCity()) &&
                StringUtils.isNotBlank(professionalDto.getJobTitle()) &&
                DateHelper.isDateCorrect(professionalDto.getStartDate(), professionalDto.getEndDate());
    }

    private boolean validateAccountFilter(AccountFilterDto accountFilterDto){
        return accountFilterDto != null &&
                accountFilterDto.getSearchContent() != null &&
                accountFilterDto.getAccountsToFilter() != null &&
                StringUtils.isNotBlank(accountFilterDto.getSearchContent()) ;
    }
}
