package com.yamo.skillsmates.settings.enums;

import lombok.Getter;

@Getter
public enum TypeEnum {

    ACCOUNT_DELETED_NOTIFICATION,
    ACCOUNT_DELETED_CONFIRMATION

}
