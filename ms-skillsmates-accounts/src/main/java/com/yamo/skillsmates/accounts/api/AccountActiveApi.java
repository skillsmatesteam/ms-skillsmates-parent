package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-accounts/accounts/my")
public interface AccountActiveApi {

    @GetMapping(value = "/activate/{accountId}")
    String activateAccount(String accountId) throws GenericException;

}
