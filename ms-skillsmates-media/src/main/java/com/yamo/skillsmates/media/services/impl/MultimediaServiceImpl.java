package com.yamo.skillsmates.media.services.impl;

import com.yamo.skillsmates.common.enumeration.OriginMultimedia;
import com.yamo.skillsmates.common.enumeration.TypeMultimedia;
import com.yamo.skillsmates.common.enumeration.TypePost;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.media.helpers.AwsAmazonHelper;
import com.yamo.skillsmates.media.helpers.MultimediaHelper;
import com.yamo.skillsmates.media.services.MultimediaService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.models.multimedia.config.InfosMediaType;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import com.yamo.skillsmates.models.multimedia.config.MediaType;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.multimedia.InfosMediaTypeRepository;
import com.yamo.skillsmates.repositories.multimedia.MediaSubTypeRepository;
import com.yamo.skillsmates.repositories.multimedia.MediaTypeRepository;
import com.yamo.skillsmates.repositories.multimedia.MultimediaRepository;
import com.yamo.skillsmates.repositories.post.PostRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class MultimediaServiceImpl implements MultimediaService {

    private final AccountRepository accountRepository;
    private final MultimediaRepository multimediaRepository;
    private final MultimediaHelper multimediaHelper;
    private final InfosMediaTypeRepository infosMediaTypeRepository;
    private final MediaSubTypeRepository mediaSubTypeRepository;
    private final MediaTypeRepository mediaTypeRepository;
    private final PostRepository postRepository;

    @Value("${media.directory.root}")
    protected String rootDirectory;
    @Value("${ms.skillsmates.media.aws.accessKey}")
    protected String awsAccessKey;
    @Value("${ms.skillsmates.media.aws.secretKey}")
    protected String secretKey;
    @Value("${ms.skillsmates.media.aws.s3bucket.name}")
    protected String awsS3Bucket;

    public MultimediaServiceImpl(AccountRepository accountRepository, MultimediaRepository multimediaRepository, MultimediaHelper multimediaHelper, InfosMediaTypeRepository infosMediaTypeRepository, MediaSubTypeRepository mediaSubTypeRepository, MediaTypeRepository mediaTypeRepository, PostRepository postRepository) {
        this.accountRepository = accountRepository;
        this.multimediaRepository = multimediaRepository;
        this.multimediaHelper = multimediaHelper;
        this.infosMediaTypeRepository = infosMediaTypeRepository;
        this.mediaSubTypeRepository = mediaSubTypeRepository;
        this.mediaTypeRepository = mediaTypeRepository;
        this.postRepository = postRepository;
    }

    @Override
    public Multimedia saveMultimedia(Multimedia multimedia, MultipartFile multipartFile, String accountId) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account unidentified");
        }
        if (!multimediaHelper.validateFileMimetype(multipartFile)){
            throw new GenericException("This kind of document is not allowed");
        }

        return uploadMultimedia(account.get(), multimedia, multipartFile);
    }

    @Override
    public Map<String, Multimedia> saveAvatarMultimedia(Map<String, MultipartFile> files, String accountId) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account unidentified");
        }
        if (!multimediaHelper.validateFileMimetype(files.get(TypeMultimedia.IMAGE.name()))){
            throw new GenericException("This kind of document is not allowed");
        }

        Map<String, Multimedia> multimediaMap = new HashMap<>();
        for (Map.Entry<String, MultipartFile> entry : files.entrySet()) {
            Multimedia multimedia = new Multimedia();
            multimedia.setIdServer(String.valueOf(System.nanoTime()));
            multimedia.setType(entry.getKey());
            multimediaMap.put(entry.getKey(), uploadMultimedia(account.get(), multimedia, entry.getValue()));
        }

        // if multimedia is avatar, the previous one need to be deleted
        updateAvatar(account.get(), multimediaMap.get(TypeMultimedia.AVATAR.name()));
        List<Multimedia> multimediaList = new ArrayList<>();
        multimediaList.add(multimediaMap.get(TypeMultimedia.IMAGE.name()));
        String description = account.get().getFirstname() + " " + account.get().getLastname() + " a changé sa photo de profil";
        createStatusPost(account.get(), multimediaList.get(0), description);

        return multimediaMap;
    }

    @Override
    public List<Multimedia> findMultimediaByAccount(String accountId) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account not found");
        }
        return multimediaRepository.findByAccount(account.get()).orElseGet(ArrayList::new);
    }

    @Override
    public Multimedia findMultimediaByIdServer(String idServer, String accountId)  throws GenericException{
        Optional<Multimedia> multimedia = multimediaRepository.findByIdServer(idServer);
        if (!multimedia.isPresent()){
            throw new GenericException("Multimedia not found");
        }
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account not found");
        }
        if (!multimedia.get().getAccount().getIdServer().equals(account.get().getIdServer())){
            throw new GenericException("Multimedia not found");
        }
        return multimedia.get();
    }

    @Override
    public boolean deleteMultimedia(String idServer, String accountId) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        Optional<Multimedia> document = multimediaRepository.findByIdServer(idServer);
        if (!account.isPresent() || !document.isPresent()){
            throw new GenericException("Account or Media can not be identified");
        }
        if (multimediaHelper.deleteMultimedia(rootDirectory, document.get().getChecksum())){
            multimediaRepository.delete(document.get());
        }else {
            throw new GenericException("Error while deleting media");
        }
        return true;
    }

    @Override
    public List<InfosMediaType> findAllInfosMediaType() {
        return infosMediaTypeRepository.findAll();
    }

    @Override
    public List<Multimedia> findMultimediaByType(String typeMultimedia, String accountId, Pageable pageable) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account not found");
        }
        if (pageable != null){
            return multimediaRepository.findByActiveTrueAndDeletedFalseAndAccountAndType(account.get(), getTypeMultimedia(typeMultimedia).name(), pageable).orElseGet(ArrayList::new);
        }else {
            return multimediaRepository.findByActiveTrueAndDeletedFalseAndAccountAndType(account.get(), getTypeMultimedia(typeMultimedia).name()).orElseGet(ArrayList::new);
        }
    }

    @Override
    public int countMultimediaByType(String typeMultimedia, String accountId) throws GenericException {
        Optional<Account> account = accountRepository.findByIdServer(accountId);
        if (!account.isPresent()){
            throw new GenericException("Account not found");
        }
        return multimediaRepository.countByActiveTrueAndDeletedFalseAndAccountAndTypeAndOrigin(account.get(), getTypeMultimedia(typeMultimedia).name(), OriginMultimedia.POST.name());
    }

    @Override
    public List<MediaSubtype> findMediaSubtypes() {
        return mediaSubTypeRepository.findAll();
    }

    /**
     * get typeMultimedia by the label
     * @param label of the type
     * @return typeMultimedia enum
     * @throws GenericException if something goes wrong
     */
    private TypeMultimedia getTypeMultimedia(String label) throws GenericException{
        TypeMultimedia typeMultimediaEnum = TypeMultimedia.fromString(label);
        if (typeMultimediaEnum == null){
            throw new GenericException("type multimedia undefined");
        }
        return typeMultimediaEnum;
    }

    private void updateAvatar(Account account, Multimedia multimedia){
        // find all profile picture for the account
        Optional<List<Multimedia>> avatars = multimediaRepository.findByActiveTrueAndDeletedFalseAndAccountAndType(account, TypeMultimedia.AVATAR.name());
        if (avatars.isPresent() && !CollectionUtils.isEmpty(avatars.get())){
            avatars.get().forEach(media -> media.setActive(false));
            multimediaRepository.saveAll(avatars.get());
        }
        account.setProfilePicture(MultimediaHelper.computeFilename(multimedia));
        accountRepository.save(account);
    }

    /**
     * create a status post
     * @param account: owner of the post
     * @param multimedia of the post
     */
    private void createStatusPost(Account account, Multimedia multimedia, String description){
        Post post = new Post();
        Optional<MediaType> mediaType = mediaTypeRepository.findByLabel("IMAGE");
        if (mediaType.isPresent()){
            Optional<MediaSubtype> mediaSubtype = mediaSubTypeRepository.findByMediaTypeAndLabelIgnoreCase(mediaType.get(), "Mes photos");
            mediaSubtype.ifPresent(post::setMediaSubtype);
        }
        post.setTitle(description);
        post.setDescription(description);
        post.setIdServer(String.valueOf(System.nanoTime()));
        post.setType(TypePost.STATUS.name());
        post.setMultimedia(multimedia);
        post.setAccount(account);
        postRepository.save(post);
    }

    /**
     * upload file into AWS S3 et persiste in DB
     * @param account of the file
     * @param multimedia file
     * @param multipartFile file
     * @return multimedia file
     * @throws GenericException if something goes wrong
     */
    private Multimedia uploadMultimedia(Account account, Multimedia multimedia, MultipartFile multipartFile) throws GenericException{
        try {
            AwsAmazonHelper awsAmazonHelper = new AwsAmazonHelper(awsAccessKey, secretKey, awsS3Bucket, rootDirectory);
            if (StringUtils.isBlank(multimedia.getIdServer())) {
                multimedia.setIdServer(String.valueOf(System.nanoTime()));
            }

            // if the file already exists for the same account
            boolean isSameFile = StringUtils.equals(multimediaHelper.computeChecksum(multipartFile.getInputStream()), multimedia.getChecksum());

            if (!isSameFile) {
                multimedia.setAccount(account);
                multimedia.setName(StringUtils.stripAccents(multipartFile.getOriginalFilename()));
                multimedia.setExtension(multimediaHelper.getExtension(multipartFile));
                multimedia.setMimeType(multipartFile.getContentType());
                multimedia.setChecksum(multimediaHelper.computeChecksum(multipartFile.getInputStream()));
                multimedia.setDirectory(account.getIdServer());

                awsAmazonHelper.upload(multimedia, multipartFile.getInputStream());
                multimediaRepository.save(multimedia);
            }
        }catch (IOException e){
            throw new GenericException("Error saving document");
        }
        return multimedia;
    }
}
