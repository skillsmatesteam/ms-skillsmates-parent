package com.yamo.skillsmates.media.controllers;

import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.enumeration.TypeMultimedia;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.multimedia.DownloadMultimediaDto;
import com.yamo.skillsmates.dto.multimedia.MediaCountDto;
import com.yamo.skillsmates.dto.multimedia.MultimediaInfosDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.multimedia.MediaSubtypeMapper;
import com.yamo.skillsmates.mapper.multimedia.MultimediaMapper;
import com.yamo.skillsmates.media.api.MultimediaApi;
import com.yamo.skillsmates.media.helpers.MultimediaHelper;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@CrossOrigin
@RestController
public class MultimediaController extends AbstractController implements MultimediaApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultimediaController.class);

    @Override
    public ResponseEntity<GenericResultDto> getMultimedias(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                           @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                           @RequestHeader(value = ACCOUNT_ID_HEADER) String accountId) throws GenericException {

        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            List<Multimedia> multimedia = multimediaService.findMultimediaByAccount(accountId);
            result.setResultObjects(MultimediaMapper.getInstance().asDtos(multimedia));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getMultimediaById(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                              @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                              @RequestHeader(value = ACCOUNT_ID_HEADER) String accountId,
                                                              @PathVariable String multimediaId) throws GenericException {

        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(multimediaId) || StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Multimedia multimedia = multimediaService.findMultimediaByIdServer(multimediaId, accountId);
            result.setResultObject(GenericMapper.INSTANCE.asDto(multimedia));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getMultimediaByType(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                                @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                                @RequestHeader(value = ACCOUNT_ID_HEADER) String accountId,
                                                                @PathVariable String typeMultimedia) throws GenericException {

        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(typeMultimedia) || StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            List<Multimedia> multimedia = multimediaService.findMultimediaByType(typeMultimedia, accountId, null);
            result.setResultObjects(MultimediaMapper.getInstance().asDtos(multimedia));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (GenericException e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> downloadMultimedia(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                               @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                               @RequestHeader(value = ACCOUNT_ID_HEADER) String accountId,
                                                               @PathVariable String multimediaId) throws GenericException {

        LOGGER.info("{}" , LoggingUtil.START );
        try{
            processToken(token, id);
            if (StringUtils.isBlank(multimediaId) || StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Multimedia multimedia = multimediaService.findMultimediaByIdServer(multimediaId, accountId);
            Path path = Paths.get(rootDirectory + File.separator + multimedia.getDirectory() + File.separator + MultimediaHelper.computeFilename(multimedia));
            DownloadMultimediaDto downloadMultimediaDto = new DownloadMultimediaDto();
            downloadMultimediaDto.setMultimedia(GenericMapper.INSTANCE.asDto(multimedia));
            downloadMultimediaDto.setContent(Files.readAllBytes(path));
            result.setResultObject(downloadMultimediaDto);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (GenericException | IOException e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getMultimediaInfos(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                               @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                               @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 3, Sort.by(Sort.Order.desc("id")));
            MultimediaInfosDto multimediaInfosDto = new MultimediaInfosDto();
            multimediaInfosDto.setDocuments(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.DOCUMENT.name(), accountId, pageable)));
            multimediaInfosDto.setNbDocuments(multimediaService.countMultimediaByType(TypeMultimedia.DOCUMENT.name(), accountId));

            multimediaInfosDto.setVideos(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.VIDEO.name(), accountId, pageable)));
            multimediaInfosDto.setNbVideos(multimediaService.countMultimediaByType(TypeMultimedia.VIDEO.name(), accountId));

            multimediaInfosDto.setAudios(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.AUDIO.name(), accountId, pageable)));
            multimediaInfosDto.setNbAudios(multimediaService.countMultimediaByType(TypeMultimedia.AUDIO.name(), accountId));

            multimediaInfosDto.setImages(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.IMAGE.name(), accountId, pageable)));
            multimediaInfosDto.setNbImages(multimediaService.countMultimediaByType(TypeMultimedia.IMAGE.name(), accountId));

            multimediaInfosDto.setLinks(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.LINK.name(), accountId, pageable)));
            multimediaInfosDto.setNbLinks(multimediaService.countMultimediaByType(TypeMultimedia.LINK.name(), accountId));

            result.setResultObject(multimediaInfosDto);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getMultimediaInfosOffline(@PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Pageable pageable = PageRequest.of(0, 3, Sort.by(Sort.Order.desc("id")));
            MultimediaInfosDto multimediaInfosDto = new MultimediaInfosDto();
            multimediaInfosDto.setDocuments(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.DOCUMENT.name(), accountId, pageable)));
            multimediaInfosDto.setNbDocuments(multimediaService.countMultimediaByType(TypeMultimedia.DOCUMENT.name(), accountId));

            multimediaInfosDto.setVideos(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.VIDEO.name(), accountId, pageable)));
            multimediaInfosDto.setNbVideos(multimediaService.countMultimediaByType(TypeMultimedia.VIDEO.name(), accountId));

            multimediaInfosDto.setAudios(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.AUDIO.name(), accountId, pageable)));
            multimediaInfosDto.setNbAudios(multimediaService.countMultimediaByType(TypeMultimedia.AUDIO.name(), accountId));

            multimediaInfosDto.setImages(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.IMAGE.name(), accountId, pageable)));
            multimediaInfosDto.setNbImages(multimediaService.countMultimediaByType(TypeMultimedia.IMAGE.name(), accountId));

            multimediaInfosDto.setLinks(MultimediaMapper.getInstance().asDtos(multimediaService.findMultimediaByType(TypeMultimedia.LINK.name(), accountId, pageable)));
            multimediaInfosDto.setNbLinks(multimediaService.countMultimediaByType(TypeMultimedia.LINK.name(), accountId));

            result.setResultObject(multimediaInfosDto);

            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getCountMultimediaInfos(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                                    @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                                    @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(accountId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            MediaCountDto mediaCountDto = new MediaCountDto();
            mediaCountDto.setDocuments(multimediaService.countMultimediaByType(TypeMultimedia.DOCUMENT.name(), accountId));
            mediaCountDto.setVideos(multimediaService.countMultimediaByType(TypeMultimedia.VIDEO.name(), accountId));
            mediaCountDto.setAudios(multimediaService.countMultimediaByType(TypeMultimedia.AUDIO.name(), accountId));
            mediaCountDto.setImages(multimediaService.countMultimediaByType(TypeMultimedia.IMAGE.name(), accountId));
            mediaCountDto.setLinks(multimediaService.countMultimediaByType(TypeMultimedia.LINK.name(), accountId));

            result.setResultObject(mediaCountDto);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getMediaSubtype(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                            @RequestHeader(value = SkillsmatesHeader.ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);

            result.setResultObjects(MediaSubtypeMapper.getInstance().asDtos(multimediaService.findMediaSubtypes()));

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private boolean validateDocument(Multimedia multimedia){
        return multimedia != null &&
                StringUtils.isNotBlank(multimedia.getChecksum()) &&
                StringUtils.isNotBlank(multimedia.getName()) &&
                StringUtils.isNotBlank(multimedia.getExtension()) &&
                StringUtils.isNotBlank(multimedia.getType()) &&
                StringUtils.isNotBlank(multimedia.getDirectory()) &&
                StringUtils.isNotBlank(multimedia.getMimeType());
    }
}
