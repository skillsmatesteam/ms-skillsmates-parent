package com.yamo.skillsmates.media.controllers;

import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.enumeration.TypeMultimedia;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.media.api.MultimediaFileApi;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
public class MultimediaFileController extends AbstractController implements MultimediaFileApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultimediaFileController.class);

    @Override
    public ResponseEntity<GenericResultDto> uploadMultimedia(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                             @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                             @RequestParam(value = TYPE_MULTIMEDIA_HEADER) String typeMultimedia,
                                                             @RequestParam(value = MULTIMEDIA_ID_HEADER) String multimediaId,
                                                             @RequestParam(value = "file") MultipartFile file) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Multimedia multimedia = null;
            if(StringUtils.isNotBlank(multimediaId) && StringUtils.isNotBlank(id)){
                try{
                    multimedia = multimediaService.findMultimediaByIdServer(multimediaId, id);
                }catch (GenericException e){
                    LOGGER.info(e.getMessage());
                }
            }

            if (multimedia == null){
                multimedia = new Multimedia();
            }else if (!validateDocument(multimedia)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            TypeMultimedia typeMultimediaEnum = TypeMultimedia.fromString(typeMultimedia);
            if (typeMultimediaEnum == null){
                return genericResultDtoError("Type multimedia unknown", HttpStatus.NOT_FOUND);
            }
            multimedia.setType(typeMultimediaEnum.name());
            multimedia = multimediaService.saveMultimedia(multimedia, file, id);

            result.setResultObject(GenericMapper.INSTANCE.asDto(multimedia));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> uploadAvatarMultimedia(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                                   @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                                   @RequestParam(value = "avatar") MultipartFile avatarFile,
                                                                   @RequestParam(value = "image") MultipartFile originalFile) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Map<String, MultipartFile> files = new HashMap<>();
            files.put(TypeMultimedia.AVATAR.name(), avatarFile);
            files.put(TypeMultimedia.IMAGE.name(), originalFile);
            Map<String, Multimedia> avatarMultimedia = multimediaService.saveAvatarMultimedia(files, id);

            result.setResultObject(GenericMapper.INSTANCE.asDto(avatarMultimedia.get(TypeMultimedia.AVATAR.name())));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteMultimedia(@RequestHeader(value = SkillsmatesHeader.TOKEN) String token,
                                                             @RequestHeader(value = SkillsmatesHeader.ID) String id,
                                                             @RequestHeader(value = MEDIA_CHECKSUM_HEADER) String checksum,
                                                             @RequestHeader(value = MULTIMEDIA_ID_HEADER) String documentId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);

            Multimedia multimedia = multimediaService.findMultimediaByIdServer(documentId, id);
            if (multimedia == null || !multimedia.getChecksum().equals(checksum)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            multimediaService.deleteMultimedia(multimedia.getIdServer(), id);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }



    private boolean validateDocument(Multimedia multimedia){
        return multimedia != null &&
                StringUtils.isNotBlank(multimedia.getChecksum()) &&
                StringUtils.isNotBlank(multimedia.getName()) &&
                StringUtils.isNotBlank(multimedia.getExtension()) &&
                StringUtils.isNotBlank(multimedia.getType()) &&
                StringUtils.isNotBlank(multimedia.getDirectory()) &&
                StringUtils.isNotBlank(multimedia.getMimeType());
    }
}
