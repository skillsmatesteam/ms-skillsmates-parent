package com.yamo.skillsmates.pages.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.post.SocialInteractionDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-pages/social-interaction", produces = MediaType.APPLICATION_JSON_VALUE)
public interface SocialInteractionApi {
    @PostMapping
    ResponseEntity<GenericResultDto> createSocialInteraction(String token, String id, SocialInteractionDto socialInteractionDto) throws GenericException;

    @GetMapping(value = "/comments/{postId}")
    ResponseEntity<GenericResultDto> findCommentsByPost(String token, String id, String postId) throws GenericException;

    @GetMapping(value = "/{socialInteractionId}")
    ResponseEntity<GenericResultDto> findById(String token, String id, String socialInteractionId) throws GenericException;

    @DeleteMapping(value = "/{socialInteractionId}")
    ResponseEntity<GenericResultDto> deleteSocialInteraction(String token, String id, String socialInteractionId) throws GenericException;

    @GetMapping(value = "/unread")
    ResponseEntity<GenericResultDto> countUnreadMessages(String token, String id) throws GenericException;

    @GetMapping(value = "/migrate")
    ResponseEntity<GenericResultDto> migrate(String token, String id) throws GenericException;

    @GetMapping(value = "/email/{emailId}")
    ResponseEntity<GenericResultDto> getEmail(String emailId) throws GenericException;
}
