package com.yamo.skillsmates.pages.helpers;

import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.dto.post.PostDto;
import org.apache.commons.lang3.StringUtils;

public class PostHelper {

    /**
     * validate mandatory data of a post
     * @param postDto
     * @return
     */
    public static boolean validatePost(PostDto postDto){
        boolean valid = postDto != null &&
                (StringUtils.isNotBlank(postDto.getTitle()) || StringUtils.isNotBlank(postDto.getDescription()));
        if (valid){
            if (StringUtils.isBlank(postDto.getTitle())) {
                postDto.setTitle(postDto.getDescription().length() > StringHelper.MAX_TITLE_SIZE ? postDto.getDescription().substring(0, StringHelper.MAX_TITLE_SIZE) : postDto.getDescription());
            }else {
                postDto.setTitle(postDto.getTitle().length() > StringHelper.MAX_TITLE_SIZE ? postDto.getTitle().substring(0, StringHelper.MAX_TITLE_SIZE) : postDto.getTitle());
            }

            if (StringUtils.isNotBlank(postDto.getDescription())){
                postDto.setDescription(postDto.getDescription().length() > StringHelper.MAX_DESCRIPTION_SIZE? postDto.getDescription().substring(0, StringHelper.MAX_DESCRIPTION_SIZE) : postDto.getDescription());
            }
        }
        return valid;
    }
}
