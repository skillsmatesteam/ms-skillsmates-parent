package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-accounts/subscriptions", produces = MediaType.APPLICATION_JSON_VALUE)
public interface SubscriptionApi {
    @GetMapping(value = "/networkByType/{accountId}")
    ResponseEntity<GenericResultDto> findNetworkByType(String token, String id, String accountId, int page, int size, int networkType) throws GenericException;

    @GetMapping(value = "/network/{accountId}")
    ResponseEntity<GenericResultDto> findNetwork(String token, String id, String accountId, int page, int size) throws GenericException;

    @GetMapping(value = "/network/offline/{accountId}")
    ResponseEntity<GenericResultDto> findNetworkOffline(String accountId, int page, int size) throws GenericException;
}
