package com.yamo.skillsmates.admin.services.impl;

import com.yamo.skillsmates.admin.services.CategoryService;
import com.yamo.skillsmates.models.admin.config.Category;
import com.yamo.skillsmates.repositories.admin.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAllCategories() {
        return categoryRepository.findAll();
    }
}
