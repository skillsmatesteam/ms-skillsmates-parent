package com.yamo.skillsmates.admin.controllers;

import com.yamo.skillsmates.admin.api.VersionApi;
import com.yamo.skillsmates.admin.services.VersionService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.version.VersionDescriptionDto;
import com.yamo.skillsmates.mapper.version.VersionMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class VersionController extends AbstractController implements VersionApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(VersionController.class);

    private final VersionService versionService;

    @Autowired
    public VersionController(VersionService versionService) {
        this.versionService = versionService;
    }

    @Override
    public ResponseEntity<GenericResultDto> saveVersion(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody VersionDescriptionDto versionDescriptionDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (!validateVersionDescription(versionDescriptionDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            genericResultDtoSuccess(id);
            result.setResultObjects(versionService.saveVersion(VersionMapper.getInstance().asEntities(versionDescriptionDto)));
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteVersion(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String versionId) throws GenericException {
        return null;
    }

    @Override
    public ResponseEntity<GenericResultDto> findAllVersions(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            genericResultDtoSuccess(id);
            result.setResultObjects(VersionMapper.getInstance().asDtos(versionService.findAllVersions()));
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    private boolean validateVersionDescription(VersionDescriptionDto versionDescriptionDto){
        return versionDescriptionDto != null && StringUtils.isNotBlank(versionDescriptionDto.getVersion()) && !CollectionUtils.isEmpty(versionDescriptionDto.getDescriptions());
    }
}
