package com.yamo.skillsmates.accounts.services;

public interface TokenService {
    void saveToken(String id, String myToken);
}
