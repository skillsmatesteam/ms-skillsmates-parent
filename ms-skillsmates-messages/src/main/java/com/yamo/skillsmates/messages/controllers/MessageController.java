package com.yamo.skillsmates.messages.controllers;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.message.AccountMessageDto;
import com.yamo.skillsmates.dto.message.MessageDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.communication.MessageMapper;
import com.yamo.skillsmates.messages.api.MessageApi;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class MessageController extends AbstractController implements MessageApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);
    private static final String defaultValueSizePage = "5";
    private static final String defaultValuePage = "0";

    @Override
    public ResponseEntity<GenericResultDto> getAccountsMessages(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            List<AccountMessageDto> accountsMessages = new ArrayList<>();
            List<AccountMessageDto> accountsWithoutMessages = new ArrayList<>();
            List<Account> accounts = libAccountService.findAccountsMessages(id);
            Account emitter = libAccountService.findAccountByIdServer(id);
            accounts.remove(emitter);

            if (!CollectionUtils.isEmpty(accounts)){
                for (Account receiver: accounts){
                    List<Message> messages = findMessagesByEmitterAndReceiver(emitter.getIdServer(), receiver.getIdServer(), false, pageable);
                    if (CollectionUtils.isEmpty(messages)) {
                        AccountMessageDto accountWithoutMessage = new AccountMessageDto();
                        accountWithoutMessage.setAccount(GenericMapper.INSTANCE.asDto(receiver));
                        accountWithoutMessage.setMessages(new ArrayList<>());
                        accountsWithoutMessages.add(accountWithoutMessage);
                    } else {
                        AccountMessageDto accountMessage = new AccountMessageDto();
                        accountMessage.setAccount(GenericMapper.INSTANCE.asDto(receiver));
                        accountMessage.setMessages(MessageMapper.getInstance().asDtos(messages));
                        accountsMessages.add(accountMessage);
                    }
                }
            }
            accountsMessages.sort(new AccountMessageComparator());
            accountsMessages.addAll(accountsWithoutMessages);
            result.setResultObjects(accountsMessages);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findMessagesByAccount(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Account accountOwner = libAccountService.findAccountByIdServer(id);
            if (accountOwner == null){
                return genericResultDtoError("Account unidentified", HttpStatus.BAD_REQUEST);
            }

            Account accountReceiver = libAccountService.findAccountByIdServer(accountId);
            if (accountReceiver == null){
                return genericResultDtoError("Account unidentified", HttpStatus.BAD_REQUEST);
            }

            result.setResultObjects(MessageMapper.getInstance().asDtos(findMessagesByEmitterAndReceiver(accountOwner.getIdServer(), accountReceiver.getIdServer(), true, pageable)));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity saveMessage(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody MessageDto messageDto) throws GenericException{

        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!validateContentMessage(messageDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            Message message  = GenericMapper.INSTANCE.asEntity(messageDto);

            /* Creation du Conversation */
            String idServerConversation = libConversationService.getConversationId(message);
            Conversation conversation = libConversationService.saveConversation(idServerConversation);

            /* Enregistrement du message en db  */
            message.setConversation(conversation);
            message = messageService.saveMessage(message);
            result.setResultObject(GenericMapper.INSTANCE.asDto(message));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @Override
    public ResponseEntity<GenericResultDto> getMyMessages(@RequestHeader(value = TOKEN) String token,
                                                          @RequestHeader(value = ID) String id,
                                                          @RequestParam(value = PAGE, defaultValue = defaultValuePage) String page,
                                                          @RequestParam(value = LIMIT, defaultValue = defaultValueSizePage) String limit) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {

            processToken(token, id);
            List<AccountMessageDto> accountsMessages = new ArrayList<>();
            List<AccountMessageDto> accountsWithoutMessages = new ArrayList<>();
            List<Account> accounts = libAccountService.findAccountsMessages(id);
            Account receiver= libAccountService.findAccountByIdServer(id);
            accounts.remove(receiver);
            Pageable pageable = PageRequest.of(StringHelper.parseInt(page), StringHelper.parseInt(limit), Sort.by(Sort.Order.desc("id")));


            if (!CollectionUtils.isEmpty(accounts)){
                for (Account emitter: accounts){
                    List<Message> messages = findMyMessagesByEmitterAndReceiver(emitter.getIdServer(), receiver.getIdServer(), false, pageable);
                    if (!CollectionUtils.isEmpty(messages)) {
                        AccountMessageDto accountMessage = new AccountMessageDto();
                        accountMessage.setAccount(GenericMapper.INSTANCE.asDto(emitter));
                        accountMessage.setMessages(MessageMapper.getInstance().asDtos(messages));
                        accountsMessages.add(accountMessage);

                    }
                }
            }
            accountsMessages.sort(new AccountMessageComparator());
            accountsMessages.addAll(accountsWithoutMessages);
            result.setResultObjects(accountsMessages);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * validate the content of the message
     * @param messageDto: message sent
     * @return true if message valid, false otherwise
     */
    private boolean validateContentMessage(MessageDto messageDto){
        return messageDto != null &&
                messageDto.getEmitterAccount() != null &&
                messageDto.getReceiverAccount()!= null &&
                StringUtils.isNotBlank(messageDto.getContent());
    }

    /**
     * fetch messages between emitter and receiver
     * @param emitterId: emitter id
     * @param receiverId: receiver id
     * @param active:
     * @param pageable: pageable
     * @return list of messages
     * @throws GenericException if something goes wrong
     */
    private List<Message> findMessagesByEmitterAndReceiver(String emitterId, String receiverId, boolean active, Pageable pageable) throws GenericException {
        Optional<Conversation> conversation  = conversationService.findConversationByIdServer(libConversationService.getConversationId(emitterId, receiverId));
        List<Message> messages = new ArrayList<>();

        if(conversation.isPresent()){
            messages = messageService.findAllMessagesByConversation(conversation.get(), active, pageable);
            Collections.reverse(messages);
        }
        return messages;
    }

    private List<Message> findMyMessagesByEmitterAndReceiver(String emitterId, String receiverId, boolean active, Pageable pageable) throws GenericException {
        Optional<Conversation> conversation  = conversationService.findConversationByIdServer(libConversationService.getConversationId(emitterId, receiverId));
        List<Message> messages = new ArrayList<>();
        List<Message> myMessages = new ArrayList<>();

        if(conversation.isPresent()){
            messages = messageService.findAllMessagesByConversation(conversation.get(), active, pageable);
            for (Message message: messages){
                if(message.getReceiverAccount().getIdServer().equals(receiverId))
                    myMessages.add(message);
            }
            Collections.reverse(myMessages);
        }
        return myMessages;
    }
}
