package com.yamo.skillsmates.messages.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.message.MessageDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/skillsmates-messages/messages", produces = MediaType.APPLICATION_JSON_VALUE)
public interface MessageApi {

    @GetMapping
    ResponseEntity<GenericResultDto> getAccountsMessages(String token, String id) throws GenericException;

    @GetMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> findMessagesByAccount(String token, String id, String accountId) throws GenericException;

    @PostMapping
    ResponseEntity saveMessage(String token, String id, MessageDto messageDto) throws GenericException;

    @GetMapping(value = "/my")
    ResponseEntity<GenericResultDto> getMyMessages(String token, String id, String page, String limit) throws GenericException;
}
