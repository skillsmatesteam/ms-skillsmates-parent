package com.yamo.skillsmates.accounts.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.SkillDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/skillsmates-accounts/accounts/skills", produces = MediaType.APPLICATION_JSON_VALUE)
public interface SkillApi {
    @PostMapping
    ResponseEntity<GenericResultDto> createSkill(String token, String id, SkillDto skillDto) throws GenericException;

    @PutMapping
    ResponseEntity<GenericResultDto> updateSkill(String token, String id, SkillDto skillDto) throws GenericException;

    @GetMapping(value = "/{accountId}")
    ResponseEntity<GenericResultDto> findSkillsById(String token, String id, String accountId) throws GenericException;

    @GetMapping(value = "/offline/{accountId}")
    ResponseEntity<GenericResultDto> findSkillsOfflineById(String accountId) throws GenericException;

    @DeleteMapping
    ResponseEntity<GenericResultDto> deleteSkill(String token, String id, String skillId) throws GenericException;
}
