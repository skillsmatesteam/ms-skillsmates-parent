package com.yamo.skillsmates.pages.controllers;

import com.yamo.skillsmates.common.enumeration.TypePost;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.AccountMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.pages.api.PostFileApi;
import com.yamo.skillsmates.pages.helpers.PostHelper;
import com.yamo.skillsmates.pages.services.PostService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController
public class PostFileController extends AbstractController implements PostFileApi {

    @Autowired
    private PostService postService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PostFileController.class);

    private static final String NO_FILENAME = "no_file.txt";

    @Override
    public ResponseEntity<GenericResultDto> createPost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestParam(value = "mediasubtype") String mediaSubtypeId, @RequestParam(value = "title") String title, @RequestParam(value = "type") String type, @RequestParam(value = "keywords") String keywords, @RequestParam(value = "discipline") String discipline, @RequestParam(value = "description") String description, @RequestParam(value = "file") MultipartFile file) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            PostDto postDto = new PostDto();
            postDto.setTitle(title);
            postDto.setType(TypePost.fromString(type).name());
            postDto.setKeywords(keywords);
            postDto.setDiscipline(discipline);
            postDto.setDescription(description);
            MediaSubtypeDto mediaSubTypeDto = new MediaSubtypeDto();
            mediaSubTypeDto.setIdServer(mediaSubtypeId);
            postDto.setMediaSubtype(mediaSubTypeDto);

            if (!PostHelper.validatePost(postDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Post post = GenericMapper.INSTANCE.asEntity(postDto);
            post.setAccount(libAccountService.findAccountByIdServer(id));
            post = postService.savePost(post, file);
            // return list of follwers so that they can be notified
            List<Account> followers = libAccountService.findFollowers(post.getAccount());
            result.setResultObjects(AccountMapper.getInstance().asDtos(followers));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updatePost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestParam(value = "postId") String postId, @RequestParam(value = "mediasubtype") String mediaSubtypeId, @RequestParam(value = "title") String title, @RequestParam(value = "type") String type, @RequestParam(value = "keywords") String keywords, @RequestParam(value = "discipline") String discipline, @RequestParam(value = "description") String description, @RequestParam(value = "file") MultipartFile file) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            PostDto postDto = new PostDto();
            postDto.setIdServer(postId);
            postDto.setTitle(title);
            postDto.setKeywords(keywords);
            postDto.setDiscipline(discipline);
            postDto.setDescription(description);
            MediaSubtypeDto mediaSubTypeDto = new MediaSubtypeDto();
            mediaSubTypeDto.setIdServer(mediaSubtypeId);
            postDto.setMediaSubtype(mediaSubTypeDto);

            if (!PostHelper.validatePost(postDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Post post = GenericMapper.INSTANCE.asEntity(postDto);
            post.setAccount(libAccountService.findAccountByIdServer(id));
            post = postService.updatePost(post, file);
            // return list of follwers so that they can be notified
            List<Account> followers = libAccountService.findFollowers(post.getAccount());
            result.setResultObjects(AccountMapper.getInstance().asDtos(followers));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> saveNewPost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestPart("post") @Valid PostDto postDto, @RequestPart("file") MultipartFile file) throws GenericException {
        try{
            processToken(token, id);
            if (!PostHelper.validatePost(postDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            if (!StringUtils.equals(id, postDto.getAccount().getIdServer())){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            if (file != null && file.getSize() <= 0 && Objects.equals(file.getOriginalFilename(), NO_FILENAME)){
                file = null;
            }
            Post post = postService.saveNewPost(postDto, file);
            // return list of follwers so that they can be notified
            List<Account> followers = libAccountService.findFollowers(post.getAccount());
            result.setResultObjects(AccountMapper.getInstance().asDtos(followers));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updateNewPost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @NotBlank @PathVariable String postId, @RequestPart("post") @Valid PostDto postDto, @RequestPart("file") MultipartFile file) throws GenericException {
        try{
            processToken(token, id);
            if (!PostHelper.validatePost(postDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            if (!StringUtils.equals(id, postDto.getAccount().getIdServer())){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            if (file != null && file.getSize() <= 0 && Objects.equals(file.getOriginalFilename(), NO_FILENAME)){
                file = null;
            }
            Post post = postService.updateNewPost(postDto, file);
            // return list of follwers so that they can be notified
            List<Account> followers = libAccountService.findFollowers(post.getAccount());
            result.setResultObjects(AccountMapper.getInstance().asDtos(followers));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
