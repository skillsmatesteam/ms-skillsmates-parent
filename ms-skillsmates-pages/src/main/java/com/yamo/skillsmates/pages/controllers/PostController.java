package com.yamo.skillsmates.pages.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.SkillsmatesHeader;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.dto.post.*;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.account.AccountMapper;
import com.yamo.skillsmates.mapper.post.PostMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.pages.api.PostApi;
import com.yamo.skillsmates.pages.helpers.PostHelper;
import com.yamo.skillsmates.pages.services.PostService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin
@RestController
public class PostController extends AbstractController implements PostApi {

    @Autowired
    private PostService postService;
    @Value("${ms.skillsmates.hostname}")
    private String hostname;
    @Value("${ms.skillsmates.url.post}")
    private String baseUrlPost;

    protected static final String POST_HEADER = SkillsmatesHeader.HEADER_PREFIX + "Post-Id";

    private static final Logger LOGGER = LoggerFactory.getLogger(PostController.class);
    private static final String defaultValueSizePage = "32";
    private static final String defaultValueSizePageSearch = "24";
    private static final String defaultValuePage = "0";

    @Override
    public ResponseEntity<GenericResultDto> createPost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody PostDto postDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );

        try {
            processToken(token, id);
            if (!PostHelper.validatePost(postDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            Post post = GenericMapper.INSTANCE.asEntity(postDto);
            post.setAccount(libAccountService.findAccountByIdServer(id));
            post = postService.savePost(post);

            // return list of follwers so that they can be notified
            List<Account> followers = libAccountService.findFollowers(post.getAccount());
            result.setResultObjects(AccountMapper.getInstance().asDtos(followers));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findPosts(@RequestHeader(value = TOKEN) String token,
                                                      @RequestHeader(value = ID) String id,
                                                      @RequestParam(value = PAGE, defaultValue = defaultValuePage) String page,
                                                      @RequestParam(value = LIMIT, defaultValue = defaultValueSizePage) String limit,
                                                      @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Pageable pageable = PageRequest.of(StringHelper.parseInt(page), StringHelper.parseInt(limit), Sort.by(Sort.Order.desc("id")));
            result.setResultObjects(PostMapper.getInstance().asPostSocialInteractionsDtos(postService.findPostsSocialInteractions(accountId, pageable)));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findPostsOffline(@RequestParam(value = PAGE, defaultValue = defaultValuePage) String page,
                                                      @RequestParam(value = LIMIT, defaultValue = defaultValueSizePage) String limit,
                                                      @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            Pageable pageable = PageRequest.of(StringHelper.parseInt(page), StringHelper.parseInt(limit), Sort.by(Sort.Order.desc("id")));
            result.setResultObjects(PostMapper.getInstance().asPostSocialInteractionsDtos(postService.findPostsSocialInteractions(accountId, pageable)));
            result.setHttpStatus(HttpStatus.OK);
            result.setMessage("Success");
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findDashboardPosts(@RequestHeader(value = TOKEN) String token,
                                                               @RequestHeader(value = ID) String id,
                                                               @RequestParam(value = PAGE, defaultValue = defaultValuePage) String page,
                                                               @RequestParam(value = LIMIT, defaultValue = defaultValueSizePage) String limit,
                                                               @PathVariable String accountId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            result.setResultObjects(PostMapper.getInstance().asPostSocialInteractionsDtos(postService.findDashboardPostsSocialInteractions(accountId, StringHelper.parseInt(page), StringHelper.parseInt(limit))));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findSinglePost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String postId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            result.setResultObject(PostMapper.getInstance().asPostSocialInteractionsDto(postService.findSinglePostSocialInteractions(postId)));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deletePost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestHeader(value = POST_HEADER) String postId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(postId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            postService.deletePost(id, postId);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> updatePost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody PostDto postDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (!PostHelper.validatePost(postDto) || postDto.getAccount() == null){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            if (!id.equals(postDto.getAccount().getIdServer())){
                return genericResultDtoError("This post doesn't belong to this account", HttpStatus.BAD_REQUEST);
            }

            Post post = GenericMapper.INSTANCE.asEntity(postDto);
            post = postService.updatePost(post);

            // return list of follwers so that they can be notified
            List<Account> followers = libAccountService.findFollowers(post.getAccount());
            result.setResultObjects(AccountMapper.getInstance().asDtos(followers));
            genericResultDtoSuccess(id);

            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

//    @Override
//    public ResponseEntity<GenericResultDto> searchPosts(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id,
//                                                        @RequestParam(name = CONTENT) String content,
//                                                        @RequestParam(name = MEDIASUBTYPE) String mediaSubType,
//                                                        @RequestParam(name = PAGE, defaultValue = defaultValuePage) int page,
//                                                        @RequestParam(name = LIMIT, defaultValue = defaultValueSizePageSearch) int limit) throws GenericException {
//        LOGGER.info("{}" , LoggingUtil.START );
//        try {
//            processToken(token, id);
//            libAccountService.findAccountByIdServer(id);
//
//            PostSocialInteractionsSearchDto postSocialInteractionsSearchDto = new PostSocialInteractionsSearchDto();
//
//            Specification<Post> specification = postService.generatePostSpecification(content, mediaSubType);
//            List<PostSocialInteractionsDto> postSocialInteractionsDtos = PostMapper.getInstance().asPostSocialInteractionsDtos(postService.searchPosts(specification, page, limit));
//            postSocialInteractionsSearchDto.setPostSocialInteractions(postSocialInteractionsDtos);
//            postSocialInteractionsSearchDto.setNumberPosts(postService.countSearchPosts(specification));
//            postSocialInteractionsSearchDto.setPagePosts(new int[getTotalPages(postSocialInteractionsDtos.size(), limit)]);
//            postSocialInteractionsSearchDto.setPagePosts(new int[postService.findPageSearchPosts(specification, page, limit).getTotalPages()]);
//            postSocialInteractionsSearchDto.setCurrentPage(page); // mémorise la page courante
//
//            result.setResultObject(postSocialInteractionsSearchDto);
//
//            genericResultDtoSuccess(id);
//            LOGGER.info("{}" , LoggingUtil.END);
//            return new ResponseEntity<>(result, result.getHttpStatus());
//
//        }catch (Exception e){
//            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
//        }
//    }

    @Override
    public ResponseEntity<GenericResultDto> searchPosts(@RequestHeader(value = TOKEN) String token,
                                                        @RequestHeader(value = ID) String id,
                                                        @RequestBody SearchParam searchParam,
                                                        @RequestParam(name = PAGE, defaultValue = defaultValuePage) int page,
                                                        @RequestParam(name = LIMIT, defaultValue = defaultValueSizePageSearch) int limit) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            PostSocialInteractionsSearchDto postSocialInteractionsSearchDto = new PostSocialInteractionsSearchDto();
            Specification<Post> specification = null;
            if (CollectionUtils.isEmpty(searchParam.getMediaSubtypes())){
                specification = postService.generatePostSpecification(searchParam);
            }else {
                for (MediaSubtypeDto mediaSubtypeDto: searchParam.getMediaSubtypes()){
                    if (specification != null){
                        specification = specification.or(postService.generatePostSpecification(searchParam, mediaSubtypeDto));
                    }else {
                        specification = postService.generatePostSpecification(searchParam, mediaSubtypeDto);
                    }
                }
            }
            if (specification != null) {
                List<PostSocialInteractionsDto> postSocialInteractionsDtos = PostMapper.getInstance().asPostSocialInteractionsDtos(postService.searchPosts(specification, page, limit));
                postSocialInteractionsDtos = postSocialInteractionsDtos.stream().filter(e-> e.getPost().getMediaSubtype()!=null).collect(Collectors.toList());
                postSocialInteractionsSearchDto.setPostSocialInteractions(postSocialInteractionsDtos);
                postSocialInteractionsSearchDto.setNumberPosts(postService.countSearchPosts(specification));
                postSocialInteractionsSearchDto.setPagePosts(new int[getTotalPages(postSocialInteractionsDtos.size(), limit)]);
                postSocialInteractionsSearchDto.setPagePosts(new int[postService.findPageSearchPosts(specification, page, limit).getTotalPages()]);
                postSocialInteractionsSearchDto.setCurrentPage(page); // mémorise la page courante
                postSocialInteractionsSearchDto.setResultFound(searchAll(searchParam, page,limit));
            }
            result.setResultObject(postSocialInteractionsSearchDto);

            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());

        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> generateUrlPost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String postId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Post post = postService.findPostByIdServer(postId);
            if (post != null){
                PostUrlDto postUrlDto = new PostUrlDto();
                postUrlDto.setUrl(hostname + baseUrlPost + post.getIdServer());
                result.setResultObject(postUrlDto);
                genericResultDtoSuccess(id);
            }else {
                genericResultDtoError("Post not found", HttpStatus.NOT_FOUND);
            }
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> viewPostOffline(@PathVariable String postId, @RequestParam(value = PAGE, defaultValue = defaultValuePage) String page,
                                                            @RequestParam(value = LIMIT, defaultValue = defaultValueSizePage) String limit) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            Post post = postService.findPostByIdServer(postId);
            if (post != null){
                Pageable pageable = PageRequest.of(StringHelper.parseInt(page), StringHelper.parseInt(limit), Sort.by(Sort.Order.desc("id")));

                AccountDto accountDto = GenericMapper.INSTANCE.asDto(post.getAccount());
                accountDto.setCurrentEstablishmentName(libAccountService.getLastEstablishment(post.getAccount()));

                ViewPostOfflineDto viewPostOfflineDto = new ViewPostOfflineDto();
                viewPostOfflineDto.setAccount(accountDto);
                viewPostOfflineDto.setPosts(postService.countPosts(post.getAccount()));
                viewPostOfflineDto.setFollowees(libAccountService.countFollowees(post.getAccount()));
                viewPostOfflineDto.setFollowers(libAccountService.countFollowers(post.getAccount()));
                viewPostOfflineDto.setPostsSocialInteractions(PostMapper.getInstance().asPostSocialInteractionsDtos(postService.findPostsSocialInteractions(post.getAccount().getIdServer(), pageable)));
                viewPostOfflineDto.setPostSocialInteractions(PostMapper.getInstance().asPostSocialInteractionsDto(postService.findSinglePostSocialInteractions(postId)));
                result.setResultObject(viewPostOfflineDto);
                genericResultDtoSuccess(post.getAccount().getIdServer());
            }else {
                genericResultDtoError(ResponseMessage.POST_NOT_FOUND.getMessage(), HttpStatus.NOT_FOUND);
            }
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public static int getTotalPages(int sourceSize, int pageSize) {
        if (pageSize <= 0) {
            throw new IllegalArgumentException("invalid page size: " + pageSize);
        }
        int totalPages;
        int resultat = sourceSize / pageSize;
        int reste = sourceSize % pageSize;

        if (reste == 0)
            totalPages = resultat;
        else if (resultat <= 0) totalPages = 1;
        else totalPages = resultat + 1;

        return totalPages;
    }

    private ResultFound searchAll(SearchParam searchParam, int page, int limit) throws GenericException {
        ResultFound resultFound = new ResultFound();
        searchParam.setMediaSubtypes(new ArrayList<>());
        Specification<Post> specification ;
        specification = postService.generatePostSpecification(searchParam);
        if (specification != null) {
            List<PostSocialInteractionsDto> postSocialInteractionsDtos = PostMapper.getInstance().asPostSocialInteractionsDtos(postService.searchPosts(specification, page, limit));
            postSocialInteractionsDtos.stream().filter(e-> e.getPost().getMediaSubtype()!=null)
                    .map(e-> e.getPost().getMediaSubtype().getMediaType().getLabel().toUpperCase())
                    .forEach( e-> {
                        if (e.equals("DOCUMENT")){resultFound.setDocument(resultFound.getDocument()+1);}
                        if (e.equals("VIDEO")){resultFound.setVideo(resultFound.getVideo()+1);}
                        if (e.equals("IMAGE")){resultFound.setImage(resultFound.getImage()+1);}
                        if (e.equals("AUDIO")){resultFound.setAudio(resultFound.getAudio()+1);}
                        if (e.equals("LINK")){resultFound.setLien(resultFound.getLien()+1);}
                    });
        }
//        searchParam.setStatuses(new ArrayList<>());
//        Specification<Account> accountSpecification = libAccountService.generateAccountSpecification(searchParam);
//        int nberProfile =  libAccountService.countSearchAccounts(accountSpecification).intValue();
//        resultFound.setProfile(nberProfile);
        return resultFound;
    }
}
