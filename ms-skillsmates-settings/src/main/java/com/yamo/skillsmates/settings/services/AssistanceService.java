package com.yamo.skillsmates.settings.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.assistance.Assistance;

import java.util.List;

public interface AssistanceService {
    Assistance saveAssistance(Assistance assistance) throws GenericException;
    List<Assistance> findAllAssistances();
}
