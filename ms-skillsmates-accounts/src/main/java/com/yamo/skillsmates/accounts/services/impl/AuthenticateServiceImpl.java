package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.enums.TemplateKeysEnum;
import com.yamo.skillsmates.accounts.http.EmailService;
import com.yamo.skillsmates.accounts.http.dto.EmailDto;
import com.yamo.skillsmates.accounts.http.dto.KeyValueDto;
import com.yamo.skillsmates.accounts.http.dto.MessageDto;
import com.yamo.skillsmates.accounts.services.AuthenticateService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.validators.CryptUtil;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AuthenticateServiceImpl implements AuthenticateService {


    private final LibAccountService libAccountService;
    private final EmailService emailService;
    protected JwtTokenUtil jwtTokenUtil;
    private final CryptUtil cryptUtil;
    @Value("${ms.skillsmates.accounts.url}")
    private String accountUrl;
    @Value("${ms.skillsmates.accounts.link}")
    private String accountLink;
    @Value("${ms.skillsmates.reset-password.link}")
    private String accountResetPasswordLink;

    @Autowired
    public AuthenticateServiceImpl(LibAccountService libAccountService, EmailService emailService, JwtTokenUtil jwtTokenUtil, CryptUtil cryptUtil) {
        this.libAccountService = libAccountService;
        this.emailService = emailService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.cryptUtil = cryptUtil;
    }

    @Override
    public Account authenticate(String email, String password) throws GenericException {
        return libAccountService.authenticate(email, password);
    }

    @Override
    public Account verificate(String email) throws GenericException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        Account account = libAccountService.verificate(email);
        if (account != null) {
            String cryptedId = cryptUtil.secureCrypt(account.getIdServer());

            List<KeyValueDto> values = new ArrayList<>();
            values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_USERNAME.name(), account.getFirstname() + " " + account.getLastname()));
            values.add(new KeyValueDto(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(),  accountUrl + accountLink + accountResetPasswordLink + cryptedId + "/token/" + jwtTokenUtil.generateToken(account.getIdServer())));
            values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_TITLE.name(),  "Réinitialisation de mot de passe"));
            values.add(new KeyValueDto(TemplateKeysEnum.EMAIL_MESSAGE.name(),  "Bienvenue "  + account.getFirstname()));
            values.add(new KeyValueDto(TemplateKeysEnum.CTA_END_TEXT.name(),  "pour afficher le message"));

            sendEmail(account, values, "Réinitialisation de mot de passe", "account.ftl");
        }
        return account;
    }

    private void sendEmail(Account account, List<KeyValueDto> values, String subject, String template) {
        EmailDto emailDto = new EmailDto();

        emailDto.setTemplate(template);

        MessageDto messageDto = new MessageDto();
        messageDto.setFrom(account.getEmail());
        messageDto.setTo(Collections.singletonList(account.getEmail()));
        messageDto.setBody(subject);
        messageDto.setSubject(subject);
        emailDto.setMessage(messageDto);
        emailDto.setValues(values);
        emailDto.setAttachments(new ArrayList<>());
        emailService.sendEmail(emailDto);
    }
}
