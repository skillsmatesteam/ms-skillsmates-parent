package com.yamo.skillsmates.accounts.services.impl;

import com.yamo.skillsmates.accounts.services.SkillService;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.mail.service.MailService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Skill;
import com.yamo.skillsmates.models.account.config.Discipline;
import com.yamo.skillsmates.models.account.config.Level;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.account.DisciplineRepository;
import com.yamo.skillsmates.repositories.account.LevelRepository;
import com.yamo.skillsmates.repositories.account.SkillRepository;
import com.yamo.skillsmates.validators.CryptUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SkillServiceImpl implements SkillService {

    private AccountRepository accountRepository;
    private SkillRepository skillRepository;
    private DisciplineRepository disciplineRepository;
    private LevelRepository levelRepository;
    private MailService mailService;
    private CryptUtil cryptUtil;

    @Autowired
    public SkillServiceImpl(AccountRepository accountRepository, SkillRepository skillRepository, DisciplineRepository disciplineRepository, LevelRepository levelRepository, MailService mailService, CryptUtil cryptUtil) {
        this.accountRepository = accountRepository;
        this.skillRepository = skillRepository;
        this.disciplineRepository = disciplineRepository;
        this.levelRepository = levelRepository;
        this.mailService = mailService;
        this.cryptUtil = cryptUtil;
    }

    @Override
    public Skill saveSkill(Skill skill) throws GenericException {
        validateSkill(skill);

        skill.setIdServer(String.valueOf(System.nanoTime()));

        return skillRepository.save(skill);
    }

    @Override
    public Skill updateSkill(Skill skill) throws GenericException {
        Optional<Skill> optionalSkill = skillRepository.findByIdServerAndAccount(skill.getIdServer(), skill.getAccount());
        if (!optionalSkill.isPresent()){
            throw new GenericException("Skill not found");
        }
        validateSkill(skill);
        return skillRepository.save(skill);
    }

    @Override
    public List<Skill> findSkills(String accountId, Pageable pageable) throws GenericException{
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return skillRepository.findAllByDeletedFalseAndAccount(optionalAccount.get(), pageable).orElseGet(ArrayList::new);
    }

    @Override
    public boolean deleteSkill(String idServer, String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        Optional<Skill> skillOptional = skillRepository.findByIdServerAndAccount(idServer, optionalAccount.get());
        if (!skillOptional.isPresent()){
            throw new GenericException("Skill does not exists");
        }
        skillOptional.get().setDeleted(true);
        skillRepository.save(skillOptional.get());
        return true;
    }

    @Override
    public List<Discipline> findDisciplines() {
        return disciplineRepository.findAll();
    }

    @Override
    public List<Level> findLevels() {
        return levelRepository.findAll();
    }

    @Override
    public long countSkills(String accountId) throws GenericException {
        Optional<Account> optionalAccount = accountRepository.findByIdServer(accountId);
        if (!optionalAccount.isPresent()){
            throw new GenericException("Account does not exists");
        }
        return skillRepository.countAllByActiveTrueAndDeletedFalseAndAccount(optionalAccount.get());
    }

    private Skill validateSkill(Skill skill) throws GenericException {
        if (StringUtils.isBlank(skill.getDiscipline())){
            throw new GenericException("Discipline not found");
        }

        if (StringUtils.isBlank(skill.getKeywords())){
            throw new GenericException("Keywords not found");
        }

        Optional<Level> level = levelRepository.findByIdServer(skill.getLevel().getIdServer());
        if (!level.isPresent()){
            throw new GenericException("Level not found");
        }
        skill.setLevel(level.get());

        Optional<Account> account = accountRepository.findByIdServer(skill.getAccount().getIdServer());
        if (!account.isPresent()){
            throw new GenericException("Account not found");
        }
        skill.setAccount(account.get());
        return skill;
    }
}
