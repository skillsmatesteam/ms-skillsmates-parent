package com.yamo.skillsmates.pages.controllers;

import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.message.PrimitiveNumberDto;
import com.yamo.skillsmates.dto.post.SocialInteractionDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.post.SocialInteractionMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.pages.api.SocialInteractionApi;
import com.yamo.skillsmates.pages.http.EmailService;
import com.yamo.skillsmates.pages.http.dto.EmailDto;
import com.yamo.skillsmates.pages.services.SocialInteractionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class SocialInteractionController extends AbstractController implements SocialInteractionApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(SocialInteractionController.class);

    @Autowired
    private SocialInteractionService socialInteractionService;

    @Autowired
    private EmailService emailService;

    @Override
    public ResponseEntity<GenericResultDto> createSocialInteraction(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @RequestBody SocialInteractionDto socialInteractionDto) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (!validateSocialInteraction(socialInteractionDto)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }

            SocialInteraction socialInteraction = GenericMapper.INSTANCE.asEntity(socialInteractionDto);
            socialInteraction = socialInteractionService.saveSocialInteration(socialInteraction, socialInteractionDto.getType(), socialInteractionDto.getElement());

            result.setResultObject(SocialInteractionMapper.getInstance().asDto(socialInteraction));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (GenericException e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findCommentsByPost(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String postId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(postId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            result.setResultObjects(SocialInteractionMapper.getInstance().asDtos(socialInteractionService.findSocialInteractionOnElementByType(postId, SocialInteractionTypeEnum.COMMENT.name())));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (GenericException e){
            result.setResultObjects(new ArrayList());
            genericResultDtoSuccess(id);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> findById(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String socialInteractionId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(socialInteractionId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            result.setResultObject(GenericMapper.INSTANCE.asDto(socialInteractionService.findById(socialInteractionId)));
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (GenericException e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> deleteSocialInteraction(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id, @PathVariable String socialInteractionId) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            if (StringUtils.isBlank(socialInteractionId)){
                return genericResultDtoError("Invalid input data", HttpStatus.BAD_REQUEST);
            }
            socialInteractionService.deleteSocialInteration(id, socialInteractionId);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (GenericException e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> countUnreadMessages(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}" , LoggingUtil.START );
        try {
            processToken(token, id);
            Account receiver = libAccountService.findAccountByIdServer(id);
            PrimitiveNumberDto primitiveNumberDto = new PrimitiveNumberDto();
            primitiveNumberDto.setNb(socialInteractionService.countUnreadMessagesByReceiver(receiver));
            result.setResultObject(primitiveNumberDto);
            genericResultDtoSuccess(id);
            LOGGER.info("{}" , LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        }catch (Exception e){
            return genericResultDtoError(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> migrate(@RequestHeader(value = TOKEN) String token, @RequestHeader(value = ID) String id) throws GenericException {
        LOGGER.info("{}", LoggingUtil.START);
        try {
            processToken(token, id);
            socialInteractionService.migrate();
            genericResultDtoSuccess(id);
            LOGGER.info("{}", LoggingUtil.END);
            return new ResponseEntity<>(result, result.getHttpStatus());
        } catch (Exception e) {
            return genericResultDtoError(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<GenericResultDto> getEmail(@PathVariable String emailId) throws GenericException {
        EmailDto emailDto = emailService.getEmail(emailId);

        LOGGER.info("{}", LoggingUtil.END);
        LOGGER.info(emailDto.getUuid());
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    private boolean validateSocialInteraction(SocialInteractionDto socialInteractionDto){
        return socialInteractionDto != null && StringUtils.isNotBlank(socialInteractionDto.getType()) &&
                socialInteractionDto.getEmitterAccount() != null && socialInteractionDto.getReceiverAccount() != null &&
                StringUtils.isNotBlank(socialInteractionDto.getElement());
    }
}
