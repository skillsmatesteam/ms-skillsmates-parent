package com.yamo.skillsmates.messages.controllers;

import com.yamo.skillsmates.dto.message.AccountMessageDto;
import org.apache.commons.collections.CollectionUtils;

import java.util.Comparator;

public class AccountMessageComparator implements Comparator<AccountMessageDto> {
    @Override
    public int compare(AccountMessageDto o1, AccountMessageDto o2) {
        if(!CollectionUtils.isEmpty(o1.getMessages()) && !CollectionUtils.isEmpty(o2.getMessages()) ) {
            return Long.compare(o1.getMessages().get(o1.getMessages().size() - 1).getCreatedAt().getTime(), o2.getMessages().get(o2.getMessages().size() - 1).getCreatedAt().getTime());
        }
        return 0;
    }
}
