package com.yamo.skillsmates.settings.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-settings/settings", produces = MediaType.APPLICATION_JSON_VALUE)
public interface SettingsApi {
    @PutMapping(value = "/setting/name")
    ResponseEntity<GenericResultDto> updateAccountByNameSetting(String token, String id, AccountDto accountDto) throws GenericException;

    @PutMapping(value = "/setting/email")
    ResponseEntity<GenericResultDto> updateAccountByEmailSetting(String token, String id, AccountDto accountDto) throws GenericException;

    @PutMapping(value = "/setting/phone")
    ResponseEntity<GenericResultDto> updateAccountByPhoneSetting(String token, String id, AccountDto accountDto) throws GenericException;

    @PutMapping(value = "/setting/password")
    ResponseEntity<GenericResultDto> updateAccountByPasswordSetting(String token, String id, AccountDto accountDto) throws GenericException;

    @GetMapping(value = "/resetpassword/{email}")
    ResponseEntity<GenericResultDto> resetPassword(String email) throws GenericException;

    @PutMapping(value = "/delete")
    ResponseEntity<GenericResultDto> deleteAccount(String token, String id, AccountDto accountDto) throws GenericException;

    @PutMapping(value = "/deactivate")
    ResponseEntity<GenericResultDto> deactivateAccount(String token, String id, AccountDto accountDto) throws GenericException;


}
