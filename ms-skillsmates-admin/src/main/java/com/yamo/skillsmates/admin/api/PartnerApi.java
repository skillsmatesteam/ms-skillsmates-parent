package com.yamo.skillsmates.admin.api;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.dto.admin.PartnerDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/skillsmates-admin/partners", produces = MediaType.APPLICATION_JSON_VALUE)
public interface PartnerApi {
    @PostMapping
    ResponseEntity<GenericResultDto> createPartner(String token, String id, PartnerDto partnerDto) throws GenericException;

    @GetMapping(value = "/{partnerId}")
    ResponseEntity<GenericResultDto> findPartnerById(String token, String id, String partnerId) throws GenericException;

    @DeleteMapping(value = "/partner")
    ResponseEntity<GenericResultDto> deletePartner(String token, String id, String partnerId) throws GenericException;

    @GetMapping(value = "/infos")
    ResponseEntity<GenericResultDto> findPartnersGeneralInfos(String token, String id) throws GenericException;

}
